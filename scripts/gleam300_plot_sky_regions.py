#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300.plotting import plot_sky_regions, plot_sin_skyregions

def get_args():
    ps = ArgumentParser()
    ps.add_argument("-o", "--outname", 
        default="GLEAM300_sky_region.pdf"
    )
    ps.add_argument("-t", "--table",
        default=None,
        nargs="*",
    )
    ps.add_argument("-r", "--ra",
        default=45.,
        type=float
    )
    ps.add_argument("-d", "--dec",
        default=-26.7,
        type=float
    )
    ps.add_argument("-n", "--npix",
        default=100,
        type=int
    )
    ps.add_argument("--dpi",
        default=72.,
        type=float
    )


    return ps.parse_args()


def cli(args):

    # plot_sky_regions(
    #     outname=args.outname, 
    #     table=args.table,
    #     dpi=args.dpi
    # )

    plot_sin_skyregions(
        outname=args.outname,
        table=args.table,
        ra=args.ra,
        dec=args.dec,
        npix=args.npix
    )


if __name__ == "__main__":
    cli(get_args())