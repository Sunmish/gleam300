#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300.plotting import tile_example


def get_args():
    ps = ArgumentParser()
    ps.add_argument("tile")
    ps.add_argument("-v", "--vscale", nargs=2, default=[-10e-3, 300e-3], type=float)
    ps.add_argument("-c", "--cmap", default="cmr.flamingo_r")
    ps.add_argument("-f", "--fontsize", default=10, type=float)
    ps.add_argument("-o", "--outname", default=None)
    ps.add_argument("-g", "--grid", action="store_true")
    return ps.parse_args()


def cli(args):
    tile_example(
        tile=args.tile,
        cmap=args.cmap,
        vsetting=args.vscale,
        fontsize=args.fontsize,
        outname=args.outname,
        grid=args.grid
    )


if __name__ == "__main__":
    cli(get_args())