#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300.sed_fitting import matched_catalogue_fitting
from gleam300.plotting import make_all_sed_plots


def get_args():

    ps = ArgumentParser()
    ps.add_argument("catalogue")
    ps.add_argument("-f", "--flux_scale_error", default=0.2, type=float)
    ps.add_argument("-p", "--plots", action="store_true")
    ps.add_argument("-c", "--cmap", default="cmr.neon")
    ps.add_argument("-o", "--overwrite", action="store_true")
    ps.add_argument("-O", "--output_directory", default="./")
    ps.add_argument("--min_snr", default=10, type=int)
    return ps.parse_args()


def cli(args):

    matched_catalogue_fitting(
        args.catalogue, 
        args.flux_scale_error,
        overwrite=args.overwrite
    )
    
    if args.plots:
        make_all_sed_plots(args.catalogue, 
            cmap=args.cmap, 
            min_snr=args.min_snr,
            output_directory=args.output_directory
        )



if __name__ == "__main__":
    cli(get_args())