#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300.beam_modelling import make_matched_lists, fit_flux_density_ratio

from glob import glob

def get_args():
    ps = ArgumentParser()
    ps.add_argument("strip", type=str)
    ps.add_argument("--make_matched_lists", action="store_true")
    ps.add_argument("--stub1", default="_deep-MFS-image-pb_matched.fits", type=str)
    ps.add_argument("--stub2", default="_pb", type=str)
    ps.add_argument("--alpha", default=-0.75, type=float)
    ps.add_argument("--selector", default="bic", choices=["aic", "bic"], type=str)
    ps.add_argument("--method", default="poly", choices=["poly", "spline", "mean", "none"], type=str)
    ps.add_argument("--spline_smooth", default=0., type=float)
    ps.add_argument("--plot_stub", default=".png", type=str)
    ps.add_argument("--max_order", default=40, type=int)
    ps.add_argument("--max_jump", default=0.1, type=float)
    ps.add_argument("--single_night", default=None)
    ps.add_argument("--cmap", default="gnuplot2", type=str)
    return ps.parse_args()


def cli(args):


    if args.make_matched_lists and args.single_night is None:
        make_matched_lists(
            strip=f"{args.strip}{args.stub2}",
            stub=args.stub1,
            alpha=args.alpha,
        )


    if args.single_night is not None:
        fit_flux_density_ratio(
            matched_list=args.strip,
            method=args.method,
            selector=args.selector,
            smooth=args.spline_smooth,
            plot_name_stub=args.plot_stub,
            max_jump=args.max_jump,
            polydegrees=[i for i in range(args.max_order)],
            cmap=args.cmap
        )


    else:
        groups = glob(f"{args.strip}{args.stub2}_matched_??????????_??????????.fits")
        
        for group in groups:

            # if args.single_night is not None and group == args.single_night:

            try:
                fit_flux_density_ratio(
                    matched_list=group,
                    method=args.method,
                    selector=args.selector,
                    smooth=args.spline_smooth,
                    plot_name_stub=args.plot_stub,
                    max_jump=args.max_jump,
                    polydegrees=[i for i in range(args.max_order)],
                    cmap=args.cmap
                )

            except RuntimeError:
                raise



if __name__ == "__main__":
    cli(get_args())