#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300.plotting import peak_int_catalogue


def get_args():
    ps = ArgumentParser()
    ps.add_argument("catalogue")
    ps.add_argument("-o", "--outname", default=None)
    ps.add_argument("--cmap", default="gnuplot2")
    ps.add_argument("--snr_range", nargs=2, default=[5., 2000.], type=float)
    ps.add_argument("--ratio_range", nargs=2, default=[0.5, 3.], type=float)
    return ps.parse_args()


def cli(args):
    peak_int_catalogue(
        catalogue=args.catalogue,
        outname=args.outname,
        cmap_hexbin=args.cmap,
        snr_range=args.snr_range,
        ratio_range=args.ratio_range,
    )


if __name__ == "__main__":
    cli(get_args())