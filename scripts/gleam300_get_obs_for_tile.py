#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300.utils import find_obs, get_tile


def get_args():
    ps = ArgumentParser()
    ps.add_argument("-T", "--sky_tiling", default=None)
    ps.add_argument("-t", "--tile", default=None)
    ps.add_argument("--tile_ra", type=float, default=None)
    ps.add_argument("--tile_dec", type=float, default=None)
    ps.add_argument("--size_ra", type=float, default=None)
    ps.add_argument("--size_dec", type=float, default=None)
    # ps.add_argument("obslist", type=str)
    ps.add_argument("database", type=str)
    ps.add_argument("-i", "--indir", default="./")
    ps.add_argument("-s", "--stub", default="_deep-MFS-image-pb_warp_corr.fits")
    ps.add_argument("-S", "--sep", default=7.5, type=float)
    ps.add_argument("-o", "--outbase", default="GLEAM300")
    ps.add_argument("--inner_sep", default=None, type=float)
    ps.add_argument("--scp", action="store_true")
    return ps.parse_args()


def cli(args):

    if args.tile is not None and args.sky_tiling is not None:
        args.tile_ra, args.tile_dec, args.size_ra, args.size_dec = \
            get_tile(args.sky_tiling, args.tile)

    find_obs(
        tile_coord=(args.tile_ra, args.tile_dec),
        tile_size=(args.size_ra/2, args.size_dec/2),
        database=args.database,
        indir=args.indir,
        stub=args.stub,
        sep=args.sep,
        outbase=args.outbase,
        sep_inner=args.inner_sep,
        include_scp=args.scp
    )


if __name__ == "__main__":
    cli(get_args())