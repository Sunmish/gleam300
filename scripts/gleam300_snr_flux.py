#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300.plotting import flux_scale_comparison


def get_args():
    ps = ArgumentParser()
    ps.add_argument("gleam_match")
    ps.add_argument("vcss_match")
    ps.add_argument("gleamx_match")
    ps.add_argument("--overwrite", action="store_true")
    ps.add_argument("-a", "--alpha", default=None, type=float)
    ps.add_argument("-o", "--outname", default="GLEAM300_snr_flux.pdf")
    return ps.parse_args()

def cli(args):
    flux_scale_comparison(
        gleam_match=args.gleam_match,
        vcss_match=args.vcss_match,
        gleamx_match=args.gleamx_match,
        overwrite=args.overwrite,
        outname=args.outname,
        alpha=args.alpha
    )


if __name__ == "__main__":
    cli(get_args())