#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300.post_image_corrections import apply_corrections 

def get_args():
    ps = ArgumentParser()
    ps.add_argument("--strip", type=str, default=None)
    ps.add_argument("database", type=str)
    ps.add_argument("--unapply_flux", action="store_true")
    ps.add_argument("--unapply_psf", action="store_true")
    ps.add_argument("--image_stub", default="_deep-MFS-image-pb_warp.fits", type=str)
    ps.add_argument("--outbase", default="_deep-MFS-image-pb_warp_corr.fits")
    return ps.parse_args()


def cli(args):

    apply_corrections(
        # images=args.images,
        strip=args.strip,
        database=args.database,
        image_stub=args.image_stub,
        unapply_flux=args.unapply_flux,
        unapply_psf=args.unapply_psf,
        outbase=args.outbase
    )

if __name__ == "__main__":
    cli(get_args())

    