#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300 import plotting


def get_args():
    ps = ArgumentParser()
    ps.add_argument("matched_catalogue", type=str, help="Matched catalogue.")
    ps.add_argument("label", type=str, help="Label of external catalogue.")
    ps.add_argument("freq", type=float, help="Frequency (MHz) of external catalogue.")
    ps.add_argument("flux_key", type=str, help="Flux density  in external catalogue.")
    ps.add_argument("-a", "--alpha", default=-0.8, help="Spectral index. Default -0.8.")
    ps.add_argument("--overwrite", action="store_true")
    ps.add_argument("-o", "--outname", type=str, help="Output plot name.")
    ps.add_argument("-m", "--multiplier", type=float, default=1.)
    ps.add_argument("--total_flux_key", default="int_flux")
    ps.add_argument("--peak_flux_key", default="peak_flux")
    ps.add_argument("--local_rms_key", default="local_rms")
    ps.add_argument("--min_snr_for_hist", default=0., type=float)
    ps.add_argument("--ratio_range", nargs=2, default=[0.2, 5.], type=float)
    ps.add_argument("--flux_scale_factor", default=1., type=float)
    ps.add_argument("--max_n", default=1000, type=int)
    ps.add_argument("-q", default=None, type=float)
    ps.add_argument("--survey_peak_flux", default="Fpwide")
    ps.add_argument("--survey_rms", default="lrmswide")
    ps.add_argument("--median", action="store_true")
    ps.add_argument("--cmap", default="gnuplot2", type=str)
    ps.add_argument("--do_peak", action="store_true")
    ps.add_argument("--frequency", default=300., type=float)
    ps.add_argument("--do_fit", action="store_true")
    ps.add_argument("--point_color", default="red")
    ps.add_argument("--params", nargs=3, type=float, default=None)
    ps.add_argument("--nbins", default=31, type=int)
    return ps.parse_args()


def cli(args):

    try:
        args.alpha = float(args.alpha)
    except ValueError:
        pass

    if args.do_fit:
        plotting.clean_bias_fit(
            matched_catalogue=args.matched_catalogue,
            label=args.label,
            freq=args.freq,
            flux=args.flux_key,
            alpha=args.alpha,
            overwrite=args.overwrite,
            outname=args.outname,
            multiplier=args.multiplier,
            total_flux_key=args.total_flux_key,
            peak_flux_key=args.peak_flux_key,
            local_rms=args.local_rms_key,
            min_snr_for_hist=args.min_snr_for_hist,
            ratio_range=args.ratio_range,
            flux_scale_factor=args.flux_scale_factor,
            max_n=args.max_n,
            q=args.q,
            survey_peak_flux=args.survey_peak_flux,
            survey_rms=args.survey_rms,
            running_median=args.median,
            cmap1=args.cmap,
            do_fit=args.do_fit,
            point_color=args.point_color,
            popt=args.params,
            nbins=args.nbins

        )

    elif args.do_peak:
        plotting.flux_scale_comparison_peak(
            matched_catalogue=args.matched_catalogue,
            label=args.label,
            freq=args.freq,
            flux=args.flux_key,
            alpha=args.alpha,
            overwrite=args.overwrite,
            outname=args.outname,
            multiplier=args.multiplier,
            total_flux_key=args.total_flux_key,
            peak_flux_key=args.peak_flux_key,
            local_rms=args.local_rms_key,
            min_snr_for_hist=args.min_snr_for_hist,
            ratio_range=args.ratio_range,
            flux_scale_factor=args.flux_scale_factor,
            max_n=args.max_n,
            q=args.q,
            survey_peak_flux=args.survey_peak_flux,
            survey_rms=args.survey_rms,
            running_median=args.median,
            cmap1=args.cmap
        )

    else:
        plotting.flux_scale_comparison(
            matched_catalogue=args.matched_catalogue,
            label=args.label,
            freq=args.freq,
            flux=args.flux_key,
            alpha=args.alpha,
            overwrite=args.overwrite,
            outname=args.outname,
            multiplier=args.multiplier,
            total_flux_key=args.total_flux_key,
            peak_flux_key=args.peak_flux_key,
            local_rms=args.local_rms_key,
            min_snr_for_hist=args.min_snr_for_hist,
            ratio_range=args.ratio_range,
            flux_scale_factor=args.flux_scale_factor,
            max_n=args.max_n,
            q=args.q,
            survey_peak_flux=args.survey_peak_flux,
            survey_rms=args.survey_rms,
            running_median=args.median,
            cmap1=args.cmap
        )


if __name__ == "__main__":
    cli(get_args())