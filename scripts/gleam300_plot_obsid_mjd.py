#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300.plotting import (
    rms_obsids, 
    rms_obsids_colored_calibrators,
    int_peak_obsids, 
    int_peak_obsids_colored_calibrators,
    flux_ratio_obsids,
    flux_ratio_obsids_colored_calibrators,
    stats_obsids_colored_strips
)

def get_args():
    ps = ArgumentParser()
    ps.add_argument("mode", choices=["rms", "int_peak", "flux_ratio"])
    ps.add_argument("outname")
    ps.add_argument("catalogues", nargs="*")
    ps.add_argument("-l", "--label", default="")
    ps.add_argument("--ext", default="votable")
    ps.add_argument("--ra_key", default="ra")
    ps.add_argument("--dec_key", default="dec")
    ps.add_argument("--int_flux_key", default="int_flux")
    ps.add_argument("--peak_flux_key", default="peak_flux")
    ps.add_argument("--local_rms_key", default="local_rms")
    ps.add_argument("--survey_flux_key", default="S_200")
    ps.add_argument("--survey_flux_multiplier", default=1., type=float)
    ps.add_argument("--survey_freq", default=200., type=float)
    ps.add_argument("--overwrite", dest="useold", action="store_false")
    ps.add_argument("--strip", default=None, type=str)
    ps.add_argument("--all", action="store_true")
    ps.add_argument("--cmap", default="gnuplot2", type=str)
    ps.add_argument("--apply", action="store_true")
    ps.add_argument("--yrange", nargs=2, type=float, default=[0.1,10])
    return ps.parse_args()


def cli(args):

    if args.all:
        stats_obsids_colored_strips(
            database=args.catalogues[0],
            outname=args.outname, 
            label=args.label,
            cmap=args.cmap,
            mode=args.mode,
            apply=args.apply
        )
    elif args.strip is None:
        if args.mode == "int_peak":
            int_peak_obsids(
                catalogues=args.catalogues, 
                outname=args.outname, 
                label=args.label,
                useold=args.useold,
                int_flux_key=args.int_flux_key,
                peak_flux_key=args.peak_flux_key,
                ra_key=args.ra_key,
                dec_key=args.dec_key,
                local_rms_key=args.local_rms_key
            )
        elif args.mode == "rms":
            rms_obsids(
                catalogues=args.catalogues, 
                outname=args.outname, 
                label=args.label, 
                ext=args.ext,
                ra_key=args.ra_key,
                dec_key=args.dec_key,
                useold=args.useold
            )
        elif args.mode == "flux_ratio":
            flux_ratio_obsids(
                catalogues=args.catalogues,
                outname=args.outname,
                label=args.label,
                useold=args.useold,
                ra_key=args.ra_key,
                dec_key=args.dec_key,
                survey_flux_key=args.survey_flux_key,
                survey_freq=args.survey_freq,
                survey_flux_multiplier=args.survey_flux_multiplier,
                yrange=args.yrange
            )
        
    else:
        if args.mode == "rms":
            rms_obsids_colored_calibrators(
                database=args.catalogues[0],
                outname=args.outname, 
                label=args.label,
                strip=args.strip,
            )
        elif args.mode == "int_peak":
            int_peak_obsids_colored_calibrators(
                database=args.catalogues[0],
                outname=args.outname, 
                label=args.label,
                strip=args.strip,
                apply=args.apply
            )
        elif args.mode == "flux_ratio":
            flux_ratio_obsids_colored_calibrators(
                database=args.catalogues[0],
                outname=args.outname, 
                label=args.label,
                strip=args.strip,
                apply=args.apply
            )



if __name__ == "__main__":
    cli(get_args())


