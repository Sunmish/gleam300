#! /usr/bin/env python


from argparse import ArgumentParser

from gleam300.plotting import clean_threshold_plot


def get_args():
    ps = ArgumentParser()
    ps.add_argument("table")
    ps.add_argument("-o", "--outname", default="GLEAM300_clean_threshold.pdf")
    ps.add_argument("-c", "--cmap", default="trans")
    return ps.parse_args()


def cli(args):

    clean_threshold_plot(
        table=args.table,
        outname=args.outname,
        cmap=args.cmap
    )


if __name__ == "__main__":
    cli(get_args())