#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300.plotting import rms_obsids, int_peak_obsids


def get_args():
    ps = ArgumentParser()
    ps.add_argument("mode", choices=["rms", "int_peak", "flux_ratio"])
    ps.add_argument("outname")
    ps.add_argument("catalogues", nargs="*")
    ps.add_argument("-l", "--label", default="")
    ps.add_argument("--ext", default="votable")
    ps.add_argument("--ra_key", default="ra")
    ps.add_argument("--dec_key", default="dec")
    return ps.parse_args()


def cli(args):

    if args.mode == "int_peak":
        int_peak_obsids(
            catalogues=args.catalogues, 
            outname=args.outname, 
            label=args.label
        )
    elif args.mode == "rms":
        rms_obsids(
            catalogues=args.catalogues, 
            outname=args.outname, 
            label=args.label, 
            ext=args.ext,
            ra_key=args.ra_key,
            dec_key=args.dec_key
        )


if __name__ == "__main__":
    cli(get_args())


