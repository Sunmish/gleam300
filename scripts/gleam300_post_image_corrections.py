#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300 import post_image_corrections, plotting

def get_args():
    ps = ArgumentParser()
    ps.add_argument("database")
    ps.add_argument("mode", choices=["flux", "ip"])
    ps.add_argument("-p", "--plot", action="store_true")
    ps.add_argument("-P", "--only_plot", action="store_true")
    ps.add_argument("-c", "--cmap", default="cmr.neon")
    ps.add_argument("-o", "--outdir", default="./")
    ps.add_argument("--lat_limit", default=0., type=float)
    return ps.parse_args()


def cli(args):

    if args.mode == "ip":

        if not args.only_plot:

            post_image_corrections.fit_intpeak_ratio(
                database=args.database,
                polyorder=3,
                polydegrees=None,
                min_group_fit=10,
                max_jump=0.05,
                ratio_range=(0.5, 2.0),
                gal_lat_limit=args.lat_limit
            )        

        if args.plot:

            plotting.intpeak_ratio_with_model_by_all_nights(
                tablename=args.database,
                cmap=args.cmap,
                outdir=args.outdir,
                yrange=(0.67, 1.5),
                gal_lat_limit=args.lat_limit
            )

    elif args.mode == "flux":

        if not args.only_plot:

            post_image_corrections.fit_flux_density_ratio(
                database=args.database,
                polyorder=3,
                polydegrees=None,
                min_group_fit=10,
                apply_ip_correction=True,
                gal_lat_limit=args.lat_limit,
                match_scp=False
            )

        if args.plot:

            plotting.flux_ratio_with_model_by_all_nights(
                tablename=args.database,
                cmap=args.cmap,
                outdir=args.outdir,
                yrange=(0.2, 5.0),
                gal_lat_limit=args.lat_limit
            )
            


if __name__ == "__main__":
    cli(get_args())