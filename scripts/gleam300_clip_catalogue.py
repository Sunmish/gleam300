#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300.utils import clip_source_list


def get_args():
    ps = ArgumentParser()
    ps.add_argument("catalogue")
    ps.add_argument("--snr", default=0., type=float)
    ps.add_argument("--perc", default=1., type=float)
    return ps.parse_args()


def cli(args):
    clip_source_list(
        catalogue=args.catalogue,
        perc_err=args.perc,
        snr=args.snr
    )


if __name__ == "__main__":
    cli(get_args())