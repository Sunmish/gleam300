#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300.utils import tile_with_ref


def get_args():
    ps = ArgumentParser()
    ps.add_argument("-c", "-r", "--ref", default=[0., -26.7], nargs=2)
    ps.add_argument("-d", "--decs", 
        default=[-72.0, -63.1, -55.0, -47.5, -40.4, -33.5, -26.7, -19.9, -13.0, -5.9, 1.6, 9.7, 18.6],
        nargs="*",
        type=float
    )
    ps.add_argument("-rs", "--ra_sep",
        default=15.0,
        type=float
    )
    ps.add_argument("-ds", "--dec_sep",
        default=10.0,
        type=float
    )
    ps.add_argument("-f", "--factor",
        default=1.2,
        type=float,
    )
    ps.add_argument("-o", "--outbase",
        default="output.csv",
        type=str
    )
    ps.add_argument("--max_dec",
        default=None,
        type=float
    )
    ps.add_argument("--scp_cap",
        action="store_true",
    )
    return ps.parse_args()



def cli(args):
    
    tile_with_ref(
        ra_sep=args.ra_sep, 
        dec_sep=args.dec_sep, 
        factor=args.factor, 
        outbase=args.outbase, 
        ref_dec=args.ref[1],
        decs=args.decs,
        dec_range=(-80, 30),
        max_dec=args.max_dec,
        scp_cap=args.scp_cap
    )


if __name__ == "__main__":
    cli(get_args())