#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300.artefacts import filter_artefacts_radial


def get_args():

    ps = ArgumentParser()
    ps.add_argument("positive_catalogue")
    ps.add_argument("negative_catalogue")
    ps.add_argument("-i", "--image", default=None)
    ps.add_argument("-f1", "--flux_threshold1", default=1., type=float)
    # ps.add_argument("-f2", "--flux_threshold2", default=6., type=float) 
    ps.add_argument("-s1", "--sep_threshold1", default=[2., 10.], nargs=2, type=float)
    # ps.add_argument("-s2", "--sep_threshold2", default=12., type=float)
    ps.add_argument("-r1", "--ratio_threshold1", default=100., type=float)
    # ps.add_argument("-r2", "--ratio_threshold2", default=650., type=float)
    ps.add_argument("-p", "--plot_dir", default="./")
    ps.add_argument("--table_dir", default=None)
    ps.add_argument("--require_negative_nearby", action="store_true")
    ps.add_argument("--suffix", default=".ar.fits", type=str)
    return ps.parse_args()


def cli(args):

    filter_artefacts_radial(
        pos_catalogue=args.positive_catalogue,
        neg_catalogue=args.negative_catalogue,
        image=args.image,
        flux_threshold=args.flux_threshold1,
        # flux_threshold2=args.flux_threshold2,
        sep_threshold=args.sep_threshold1,
        # sep_threshold2=args.sep_threshold2,
        ratio_threshold=args.ratio_threshold1,
        # ratio_threshold2=args.ratio_threshold2,
        plot_dir=args.plot_dir,
        plot=True,
        table_dir=args.table_dir,
        require_negative_nearby=args.require_negative_nearby,
        suffix=args.suffix
    )


if __name__ == "__main__":
    cli(get_args())