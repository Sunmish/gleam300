#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300.plotting import reliability


def get_args():
    ps = ArgumentParser()
    ps.add_argument("sources")
    ps.add_argument("negative_sources")
    ps.add_argument("-o", "--outname", default="GLEAM300_reliability.pdf")
    ps.add_argument("--bins", default=10, type=int)
    ps.add_argument("--max_bin", default=None, type=float)
    return ps.parse_args()


def cli(args):

    reliability(
        sources=args.sources,
        negative_sources=args.negative_sources,
        outname=args.outname,
        snr_bins=args.bins,
        max_snr=args.max_bin,
    )


if __name__ == "__main__":
    cli(get_args())