#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300.plotting import astrometry_square, astrometry_4square


def get_args():
    ps = ArgumentParser()
    ps.add_argument("matched_catalogue", nargs="*")
    ps.add_argument("--overwrite", action="store_true")
    ps.add_argument("-s", "--snr", default=50., type=float)
    ps.add_argument("-o", "--outname", default=None)
    ps.add_argument("-H", "--hexbin", action="store_true")
    ps.add_argument("-S", "--survey", default=None)
    return ps.parse_args()


def cli(args):
    # if len(args.matched_catalogue) == 4:
    #     astrometry_4square(
    #         matched_catalogue=args.matched_catalogue,
    #         overwrite=args.overwrite,
    #         snr_limit=args.snr,
    #         outname=args.outname,
    #     )
    for i, mc in enumerate(args.matched_catalogue):
        astrometry_square(
            matched_catalogue=mc,
            overwrite=args.overwrite,
            snr_limit=args.snr,
            outname=args.outname,
            survey=args.survey,
            hexbin=args.hexbin
        )


if __name__ == "__main__":
    cli(get_args())



