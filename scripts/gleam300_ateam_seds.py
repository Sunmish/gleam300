#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300 import plotting


def get_args():

    ps = ArgumentParser()
    ps.add_argument("source", choices=["Fornax A"])

    return ps.parse_args()


def cli(args):
    plotting.ateam_sed(args.source)


if __name__ == "__main__":
    cli(get_args())