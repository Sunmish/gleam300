#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300.plotting import beam_example


def get_args():
    ps = ArgumentParser()
    ps.add_argument("obsid", type=int)
    ps.add_argument("--cmap", default="gnuplot2_r")
    ps.add_argument("--outname", default=None)
    return ps.parse_args()


def cli(args):
    beam_example(
        obsid=args.obsid,
        plotname=args.outname,
        cmap=args.cmap
    )


if __name__ == "__main__":
    cli(get_args())