#!python

from argparse import ArgumentParser

from gleam300.utils import clip_obsids


def get_args():
    ps = ArgumentParser()
    ps.add_argument("images", nargs="*")
    ps.add_argument("-t", "--table_stub", default="_deep-MFS-image-pb_comp.vot")
    ps.add_argument("-o", "--outfile", default="images")
    ps.add_argument("-r", "--max_rms", default=0.5, type=float)
    ps.add_argument("-f", "--max_flux", default=1000., type=float)
    ps.add_argument("-m", "--check_for_moon", action="store_true")
    ps.add_argument("-F", "--min_flux", default=-50., type=float)
    ps.add_argument("-s", "--check_for_solar_system_body", action="store_true")
    ps.add_argument("-R", "--ratio", nargs=2, default=[0.5, 2.0], type=float)
    return ps.parse_args()


def cli(args):

    clip_obsids(
        images=args.images,
        output_list=args.outfile,
        max_rms=args.max_rms,
        max_flux=args.max_flux,
        min_flux=args.min_flux,
        check_for_moon=args.check_for_moon,
        ratio_range=args.ratio,
        table_stub=args.table_stub,
        check_for_solar_system_body=args.check_for_solar_system_body
    )


if __name__ == "__main__":
    cli(get_args())