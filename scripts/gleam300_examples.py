#! /usr/bin/env python

from argparse import ArgumentParser

from gleam300 import plotting


def get_args():
    ps = ArgumentParser()
    ps.add_argument("example", choices=[
        "blanksky", 
        "smc",
        "gp",
        "vela",
        "a3667",
        "fornax"
    ])
    ps.add_argument("-c", "--cmap", default="cmr.flamingo_r")
    ps.add_argument("-tc", "--tc", default="black")
    ps.add_argument("-v", "--vscale", default=None, nargs=2)
    return ps.parse_args()


def cli(args):

    if args.example == "blanksky":
        if args.vscale is None:
            args.vscale = [-10e-3, 300e-3]
        plotting.blanksky_example(
            cmap=args.cmap, 
            tc=args.tc, 
            vsetting=args.vscale
        )
    elif args.example == "smc":
        if args.vscale is None:
            args.vscale = [-10e-3, 200e-3]
        plotting.smc_example(
            cmap=args.cmap, 
            tc=args.tc, 
            vsetting=args.vscale
        )
    elif args.example == "gp":
        if args.vscale is None:
            args.vscale = [-10e-3, 500e-3]
        plotting.gp_example(
            cmap=args.cmap, 
            tc=args.tc, 
            vsetting=args.vscale
        )
    elif args.example == "vela":
        if args.vscale is None:
            args.vscale = [-10e-3, 500e-3]
        plotting.vela_example(
            cmap=args.cmap, 
            tc=args.tc, 
            vsetting=args.vscale
        )
    elif args.example == "a3667":
        if args.vscale is None:
            args.vscale = [-10e-3, 500e-3]
        plotting.a3667_example(
            cmap=args.cmap, 
            tc=args.tc, 
            vsetting=args.vscale
        )
    elif args.example == "fornax":
        if args.vscale is None:
            args.vscale = [-10e-3, 500e-3]
        plotting.fornax_example(
            cmap=args.cmap, 
            tc=args.tc, 
            vsetting=args.vscale
        )
    

if __name__ == "__main__":
    cli(get_args())
