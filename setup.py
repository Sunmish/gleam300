#! /usr/bin/env python

import setuptools

with open("README.md", "r") as f:
    long_description = f.read()

scripts =[
"scripts/gleam300_peak_int_ratio_plot.py",
"scripts/gleam300_plot_sky_regions.py",
"scripts/gleam300_beam_plot.py",
"scripts/gleam300_snr_flux.py",
"scripts/gleam300_snr_flux_individual.py",
"scripts/gleam300_astrometry_square.py",
"scripts/gleam300_reliability.py",
"scripts/gleam300_sed_fitting.py",
"scripts/gleam300_examples.py",
"scripts/gleam300_snapshot_selection.py",
"scripts/gleam300_plot_obsid_mjd.py",
"scripts/gleam300_fit_flux_scale.py",
"scripts/gleam300_apply_correction_factor.py",
"scripts/gleam300_get_tile_regions.py",
"scripts/gleam300_get_obs_for_tile.py",
"scripts/gleam300_post_image_corrections.py",
"scripts/gleam300_plot_clean_threshold.py",
"scripts/gleam300_clip_catalogue.py",
"scripts/gleam300_plot_single_tile.py",
"scripts/gleam300_ateam_seds.py",
"scripts/gleam300_clip_catalogue.py",
"scripts/gleam300_remove_artefacts.py",
]


setuptools.setup(
    name="gleam300",
    version="1.0.0",
    author="SWD",
    author_email="stefanduchesne@gmail.com",
    description="GLEAM 300 MHz survey.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=["gleam300"],
    scripts=scripts,

)

