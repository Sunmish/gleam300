#! /usr/bin/env python

import os

import numpy as np
from astropy.table import Table
from astropy.coordinates import SkyCoord
from astropy import units as u

from gleam300.plotting import plot_artefact_removal

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def filter_artefacts(pos_catalogue : str, neg_catalogue : str, 
    image : str = None,
    flux_threshold : float = [2., 6.],
    # flux_threshold2 : float = 6.,
    sep_threshold : float = [5., 12.],
    # sep_threshold2 : float = 12.,
    ratio_threshold : float = [350., 650.],
    # ratio_threshold2 : float = 650.,
    flux_key : str = "int_flux",
    ra_key : str = "ra",
    dec_key : str = "dec",
    bmaj_key : str = "psf_a",
    plot : bool = False,
    require_negative_nearby : bool = True,
    suffix : str = ".ar.fits",
    plot_dir : str = "./",
    table_dir : str = None):
    """Filter artefacts based on two-step thresholding from Hurley-Walker+2022.

    First filter is faint, but close.
    Second filter is stronger, but farther.

    """

    pos_table = Table.read(pos_catalogue)
    pos_coords = SkyCoord(
        ra=pos_table[ra_key]*u.deg,
        dec=pos_table[dec_key]*u.deg
    )
    neg_table = Table.read(neg_catalogue)
    neg_coords = SkyCoord(
        ra=neg_table[ra_key]*u.deg,
        dec=neg_table[dec_key]*u.deg
    )



    artefact_indices_pos = []
    artefact_indices_neg = []
    checked_sources = []

    for (flux_threshold1, sep_threshold1, ratio_threshold1) in zip(
        flux_threshold[::-1], sep_threshold[::-1], ratio_threshold[::-1]
    ): 
    
       
        sep1_pos = pos_table[bmaj_key]*sep_threshold1
        # sep2_pos = pos_table[bmaj_key]*sep_threshold2

        sep1_neg = neg_table[bmaj_key]*sep_threshold1
        # sep2_neg = neg_table[bmaj_key]*sep_threshold2
        
        # cond1 = flux_threshold1 < pos_table[flux_key]
        # cond2 = pos_table[flux_key] < flux_threshold2
        # cond3 = flux_threshold2 < pos_table[flux_key]
        # idx1 = np.where(cond1 & cond2)[0]
        # idx2 = np.where(cond3)[0]

        idx1 = np.where(pos_table[flux_key] > flux_threshold1)[0]


        for i in idx1:
            if i in checked_sources:
                continue
            else:
                checked_sources.append(i)

            prep_plot = False

            coord_i = pos_coords[i]

            seps_pos = coord_i.separation(pos_coords)
            seps_neg = coord_i.separation(neg_coords)
            
            sep_idx1_pos = np.where(seps_pos.value*3600. < sep1_pos)[0]
            sep_idx1_neg = np.where(seps_neg.value*3600. < sep1_neg)[0]

            if require_negative_nearby and len(sep_idx1_neg) < 1:
                # no negative sources, likely no positive sources?
                continue
        

            if len(sep_idx1_neg) > 0:
                max_negative = np.nanmax(np.abs(neg_table[flux_key][sep_idx1_neg]))
                ratio_threshold1_i = pos_table[i][flux_key] / max_negative
            else:
                ratio_threshold1_i = ratio_threshold1
        
            thresh_idx1_pos = np.where(
                pos_table[i][flux_key]/pos_table[sep_idx1_pos][flux_key] >= ratio_threshold1_i
            )[0]

            thresh_idx1_neg = np.where(
                abs(pos_table[i][flux_key]/neg_table[sep_idx1_neg][flux_key]) >= ratio_threshold1_i 
            )[0]
            
            if len(thresh_idx1_pos) > 0:
                logger.debug("Removing {} positive artefacts around {:.2f} {:.2f}".format(
                    len(thresh_idx1_pos), coord_i.ra.value, coord_i.dec.value
                ))
                artefact_indices_pos.extend(list(thresh_idx1_pos))

                prep_plot = True

            if len(thresh_idx1_neg) > 0:
                logger.debug("Removing {} negative artefacts around {:.2f} {:.2f}".format(
                    len(thresh_idx1_neg), coord_i.ra.value, coord_i.dec.value
                ))
                artefact_indices_neg.extend(list(thresh_idx1_neg))

                prep_plot = True

            logger.debug(pos_table[i][flux_key])
            logger.debug(pos_table[sep_idx1_pos][thresh_idx1_pos][flux_key])

            if prep_plot and plot and image is not None:
                plot_artefact_removal(
                    positive=pos_table[sep_idx1_pos],
                    negative=neg_table[sep_idx1_neg],
                    positive_artefacts=pos_table[sep_idx1_pos][thresh_idx1_pos],
                    negative_artefacts=neg_table[sep_idx1_neg][thresh_idx1_neg],
                    image=image,
                    threshold=flux_threshold1,
                    radius=sep_threshold1*pos_table[i][bmaj_key]/3600.,
                    coords=(coord_i.ra.value, coord_i.dec.value),
                    rms=pos_table[i]["local_rms"],
                    outname="{}/{}_{:.2f}d_{:.2f}d_{:.0f}Jy.png".format(
                        plot_dir, 
                        os.path.basename(image).replace(".fits", ""), 
                        coord_i.ra.value, 
                        coord_i.dec.value, 
                        flux_threshold1
                    )
                )

            elif plot and image is not None:
                plot_artefact_removal(
                    image=image,
                    positive=pos_table[sep_idx1_pos],
                    negative=neg_table[sep_idx1_neg],
                    threshold=flux_threshold1,
                    radius=sep_threshold1*pos_table[i][bmaj_key]/3600.,
                    coords=(coord_i.ra.value, coord_i.dec.value),
                    rms=pos_table[i]["local_rms"],
                    outname="{}/{}_{:.2f}d_{:.2f}d_{:.0f}Jy_no_artefacts.png".format(
                        plot_dir, 
                        os.path.basename(image).replace(".fits", ""), 
                        coord_i.ra.value, 
                        coord_i.dec.value, 
                        flux_threshold1
                    )
                )

            if table_dir is not None:
                pos_table[sep_idx1_pos].write(
                   "{}/{}_{:.2f}d_{:.2f}d_{:.0f}Jy_pos.fits".format(
                        table_dir, 
                        os.path.basename(image).replace(".fits", ""), 
                        coord_i.ra.value, 
                        coord_i.dec.value, 
                        flux_threshold1
                    ),
                    overwrite=True 
                )
                neg_table[sep_idx1_neg].write(
                   "{}/{}_{:.2f}d_{:.2f}d_{:.0f}Jy_neg.fits".format(
                        table_dir, 
                        os.path.basename(image).replace(".fits", ""), 
                        coord_i.ra.value, 
                        coord_i.dec.value, 
                        flux_threshold1
                    ),
                    overwrite=True 
                )



def radial_function(flux, radial_range=(2., 10.), min_flux=1.):
    r = flux/min_flux * radial_range[0]
    if r > radial_range[1]:
        r = radial_range[1]
    return r


def filter_artefacts_radial(pos_catalogue : str, neg_catalogue : str, 
    image : str = None,
    flux_threshold : float = 1.,
    sep_threshold : float = (2., 10.),
    ratio_threshold : float = 100.,
    flux_key : str = "int_flux",
    ra_key : str = "ra",
    dec_key : str = "dec",
    bmaj_key : str = "psf_a",
    rms_key : str = "local_rms",
    plot : bool = False,
    require_negative_nearby : bool = True,
    suffix : str = ".ar.fits",
    plot_dir : str = "./",
    table_dir : str = None):
    """Filter artefacts based on two-step thresholding from Hurley-Walker+2022.

    First filter is faint, but close.
    Second filter is stronger, but farther.

    """

    pos_table = Table.read(pos_catalogue)
    pos_coords = SkyCoord(
        ra=pos_table[ra_key]*u.deg,
        dec=pos_table[dec_key]*u.deg
    )
    neg_table = Table.read(neg_catalogue)
    neg_coords = SkyCoord(
        ra=neg_table[ra_key]*u.deg,
        dec=neg_table[dec_key]*u.deg
    )

    artefact_indices_pos = []
    artefact_indices_neg = []
    checked_sources = []

    flux_threshold1 = flux_threshold
    sep_threshold1 = sep_threshold
    ratio_threshold1 = ratio_threshold

    rms = np.nanmedian(pos_table[rms_key])
    max_flux = np.nanmax(pos_table[flux_key])

    max_dr = max_flux / rms

    idx1 = np.where(pos_table[flux_key] > flux_threshold1)[0]

    for i in idx1:
        if i in checked_sources:
            continue
        else:
            checked_sources.append(i)

        prep_plot = False

        sep_threshold1_i = radial_function(
            flux=pos_table[i][flux_key],
            radial_range=sep_threshold1,
            min_flux=flux_threshold1
        )

        sep1_pos = pos_table[bmaj_key]*sep_threshold1_i
        sep1_neg = neg_table[bmaj_key]*sep_threshold1_i

        coord_i = pos_coords[i]

        seps_pos = coord_i.separation(pos_coords)
        seps_neg = coord_i.separation(neg_coords)
        
        sep_idx1_pos = np.where(seps_pos.value*3600. < sep1_pos)[0]
        sep_idx1_neg = np.where(seps_neg.value*3600. < sep1_neg)[0]

        if require_negative_nearby and len(sep_idx1_neg) < 1:
            # no negative sources, likely no positive sources?
            continue
    

        if len(sep_idx1_neg) > 0:
            max_negative = np.nanmax(np.abs(neg_table[flux_key][sep_idx1_neg]))
            ratio_threshold1_i = pos_table[i][flux_key] / max_negative
            sep2_pos = np.full_like(
                sep1_pos, np.max(seps_neg.value[sep_idx1_neg]))
        else:
            ratio_threshold1_i = ratio_threshold1
            sep2_pos = sep1_pos


        cond1_pos = pos_table[i][flux_key]/pos_table[sep_idx1_pos][flux_key] >= ratio_threshold1
        cond2_pos = pos_table[i][flux_key]/pos_table[sep_idx1_pos][flux_key] >= ratio_threshold1_i
        cond3_pos = seps_pos.value[sep_idx1_pos] < sep2_pos[sep_idx1_pos]

        thresh_idx1_pos = np.where(
            # pos_table[i][flux_key]/pos_table[sep_idx1_pos][flux_key] >= ratio_threshold1_i
            cond1_pos | (cond2_pos & cond3_pos)
            # either within max sep, 100*brightness or
            # within negative distance, as bright as negative source
        )[0]

        thresh_idx1_neg = np.where(
            abs(pos_table[i][flux_key]/neg_table[sep_idx1_neg][flux_key]) >= ratio_threshold1_i 
        )[0]
        
        if len(thresh_idx1_pos) > 0:
            logger.debug("Removing {} positive artefacts around {:.2f} {:.2f}".format(
                len(thresh_idx1_pos), coord_i.ra.value, coord_i.dec.value
            ))
            artefact_indices_pos.extend(list(sep_idx1_pos[thresh_idx1_pos]))

            prep_plot = True

            if len(thresh_idx1_neg) > 0:
                logger.debug("Removing {} negative artefacts around {:.2f} {:.2f}".format(
                    len(thresh_idx1_neg), coord_i.ra.value, coord_i.dec.value
                ))
                artefact_indices_neg.extend(list(sep_idx1_neg[thresh_idx1_neg]))

        if prep_plot and plot and image is not None:
            plot_artefact_removal(
                positive=pos_table[sep_idx1_pos],
                negative=neg_table[sep_idx1_neg],
                positive_artefacts=pos_table[sep_idx1_pos][thresh_idx1_pos],
                negative_artefacts=neg_table[sep_idx1_neg][thresh_idx1_neg],
                image=image,
                threshold=flux_threshold1,
                radius=sep_threshold1_i*pos_table[i][bmaj_key]/3600.,
                coords=(coord_i.ra.value, coord_i.dec.value),
                rms=pos_table[i]["local_rms"],
                outname="{}/{}_{:.2f}d_{:.2f}d_{:.1f}Jy_{:.1f}psf.png".format(
                    plot_dir, 
                    os.path.basename(image).replace(".fits", ""), 
                    coord_i.ra.value, 
                    coord_i.dec.value, 
                    pos_table[i][flux_key],
                    sep_threshold1_i
                )
            )

        elif plot and image is not None:
            plot_artefact_removal(
                image=image,
                positive=pos_table[sep_idx1_pos],
                negative=neg_table[sep_idx1_neg],
                threshold=flux_threshold1,
                radius=sep_threshold1_i*pos_table[i][bmaj_key]/3600.,
                coords=(coord_i.ra.value, coord_i.dec.value),
                rms=pos_table[i]["local_rms"],
                outname="{}/{}_{:.2f}d_{:.2f}d_{:.1f}Jy_{:.1f}psf_no_artefacts.png".format(
                    plot_dir, 
                    os.path.basename(image).replace(".fits", ""), 
                    coord_i.ra.value, 
                    coord_i.dec.value, 
                    pos_table[i][flux_key],
                    sep_threshold1_i
                )
            )

        if table_dir is not None:
            if len(artefact_indices_pos) > 0:
                pos_table[artefact_indices_pos].write(
                "{}/{}_{:.2f}d_{:.2f}d_{:.0f}Jy_pos.fits".format(
                        table_dir, 
                        os.path.basename(image).replace(".fits", ""), 
                        coord_i.ra.value, 
                        coord_i.dec.value, 
                        flux_threshold1
                    ),
                    overwrite=True 
                )
            if len(artefact_indices_neg) > 0:
                neg_table[artefact_indices_neg].write(
                "{}/{}_{:.2f}d_{:.2f}d_{:.0f}Jy_neg.fits".format(
                        table_dir, 
                        os.path.basename(image).replace(".fits", ""), 
                        coord_i.ra.value, 
                        coord_i.dec.value, 
                        flux_threshold1
                    ),
                    overwrite=True 
                )

    n_pos_removal = np.unique(artefact_indices_pos)
    n_neg_removal = np.unique(artefact_indices_neg)


    if len(pos_table) > 0:
        logger.info("Removing {} / {} ({:.5f}%) positive sources".format(
            len(n_pos_removal), len(pos_table), 100*len(n_pos_removal)/len(pos_table) 
        ))
    if len(neg_table) > 0:
        logger.info("Removing {} / {} ({:.5f}%) negative sources".format(
            len(n_neg_removal), len(neg_table), 100*len(n_neg_removal)/len(neg_table) 
        ))

    try:
        pos_table.remove_rows(n_pos_removal)
    except IndexError:
        pass
    try:
        neg_table.remove_rows(n_neg_removal)
    except IndexError:
        pass

    pos_table.write(
        pos_catalogue.replace(".fits", "") + suffix, overwrite=True
    )

    neg_table.write(
        neg_catalogue.replace(".fits", "") + suffix, overwrite=True
    )