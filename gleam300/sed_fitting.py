#! /usr/bin/env python

import numpy as np
from scipy.optimize import curve_fit

from astropy.io import fits
from astropy.wcs import WCS
from astropy.table import Table

from tqdm import trange

from gleam300 import stats
from fluxtools import measures

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# flux_keys = [
# "int_flux",
# 'Fint076_GLEAM',
# 'Fint084_GLEAM',
# 'Fint092_GLEAM',
# 'Fint099_GLEAM',
# 'Fint107_GLEAM',
# 'Fint115_GLEAM',
# 'Fint122_GLEAM',
# 'Fint130_GLEAM',
# 'Fint143_GLEAM',
# 'Fint151_GLEAM',
# 'Fint158_GLEAM',
# 'Fint166_GLEAM',
# 'Fint174_GLEAM',
# 'Fint181_GLEAM',
# 'Fint189_GLEAM',
# 'Fint197_GLEAM',
# 'Fint204_GLEAM',
# 'Fint212_GLEAM',
# 'Fint220_GLEAM',
# 'Fint227_GLEAM',
# "Total_flux_Source_LOW",
# "Total_flux_MID",
# "Total_flux_HIGH",
# ]

# flux_keys = [
# "int_flux",
# 'int_flux_076_GLEAMX',
# 'int_flux_084_GLEAMX',
# 'int_flux_092_GLEAMX',
# 'int_flux_099_GLEAMX',
# 'int_flux_107_GLEAMX',
# 'int_flux_115_GLEAMX',
# 'int_flux_122_GLEAMX',
# 'int_flux_130_GLEAMX',
# 'int_flux_143_GLEAMX',
# 'int_flux_151_GLEAMX',
# 'int_flux_158_GLEAMX',
# 'int_flux_166_GLEAMX',
# 'int_flux_174_GLEAMX',
# 'int_flux_181_GLEAMX',
# 'int_flux_189_GLEAMX',
# 'int_flux_197_GLEAMX',
# 'int_flux_204_GLEAMX',
# 'int_flux_212_GLEAMX',
# 'int_flux_220_GLEAMX',
# 'int_flux_227_GLEAMX',
# "Total_flux_Source_LOW",
# "Total_flux_MID",
# "Total_flux_HIGH",
# ]

flux_keys = [
"int_flux_corr",
'int_flux_W_087_GLEAMX',
'int_flux_W_118_GLEAMX',
'int_flux_W_154_GLEAMX',
'int_flux_W_185_GLEAMX',
'int_flux_W_215_GLEAMX',
"Total_flux_Source_LOW",
"Total_flux_MID",
"Total_flux_HIGH",
]

# freqs = [
# 300., 76., 84., 92., 99., 107., 115., 122., 130., 143., 151., 158., 166., 174., 
# 181., 189., 197., 204., 212., 220., 227., 887.5, 1367.5, 1655.5
# ]

freqs = [300., 88., 118., 154., 185., 216., 887.5, 1367.5, 1655.5]

# e_flux_keys = [
# "err_int_flux",
# 'e_Fint076_GLEAM',
# 'e_Fint084_GLEAM',
# 'e_Fint092_GLEAM',
# 'e_Fint099_GLEAM',
# 'e_Fint107_GLEAM',
# 'e_Fint115_GLEAM',
# 'e_Fint122_GLEAM',
# 'e_Fint130_GLEAM',
# 'e_Fint143_GLEAM',
# 'e_Fint151_GLEAM',
# 'e_Fint158_GLEAM',
# 'e_Fint166_GLEAM',
# 'e_Fint174_GLEAM',
# 'e_Fint181_GLEAM',
# 'e_Fint189_GLEAM',
# 'e_Fint197_GLEAM',
# 'e_Fint204_GLEAM',
# 'e_Fint212_GLEAM',
# 'e_Fint220_GLEAM',
# 'e_Fint227_GLEAM',
# "E_Total_flux_Source_LOW",
# "E_Total_flux_MID",
# "E_Total_flux_HIGH",
# ]

# e_flux_keys2 = [
# "err_int_flux2",
# 'e_Fint076_GLEAM2',
# 'e_Fint084_GLEAM2',
# 'e_Fint092_GLEAM2',
# 'e_Fint099_GLEAM2',
# 'e_Fint107_GLEAM2',
# 'e_Fint115_GLEAM2',
# 'e_Fint122_GLEAM2',
# 'e_Fint130_GLEAM2',
# 'e_Fint143_GLEAM2',
# 'e_Fint151_GLEAM2',
# 'e_Fint158_GLEAM2',
# 'e_Fint166_GLEAM2',
# 'e_Fint174_GLEAM2',
# 'e_Fint181_GLEAM2',
# 'e_Fint189_GLEAM2',
# 'e_Fint197_GLEAM2',
# 'e_Fint204_GLEAM2',
# 'e_Fint212_GLEAM2',
# 'e_Fint220_GLEAM2',
# 'e_Fint227_GLEAM2',
# "E_Total_flux_Source_LOW2",
# "E_Total_flux_MID2",
# "E_Total_flux_HIGH2"
# ]

# e_flux_keys = [
# "err_int_flux",
# 'err_int_flux_076_GLEAMX',
# 'err_int_flux_084_GLEAMX',
# 'err_int_flux_092_GLEAMX',
# 'err_int_flux_099_GLEAMX',
# 'err_int_flux_107_GLEAMX',
# 'err_int_flux_115_GLEAMX',
# 'err_int_flux_122_GLEAMX',
# 'err_int_flux_130_GLEAMX',
# 'err_int_flux_143_GLEAMX',
# 'err_int_flux_151_GLEAMX',
# 'err_int_flux_158_GLEAMX',
# 'err_int_flux_166_GLEAMX',
# 'err_int_flux_174_GLEAMX',
# 'err_int_flux_181_GLEAMX',
# 'err_int_flux_189_GLEAMX',
# 'err_int_flux_197_GLEAMX',
# 'err_int_flux_204_GLEAMX',
# 'err_int_flux_212_GLEAMX',
# 'err_int_flux_220_GLEAMX',
# 'err_int_flux_227_GLEAMX',
# "E_Total_flux_Source_LOW",
# "E_Total_flux_MID",
# "E_Total_flux_HIGH",
# ]

# e_flux_keys2 = [
# "err_int_flux2",
# 'err_int_flux_076_GLEAMX2',
# 'err_int_flux_084_GLEAMX2',
# 'err_int_flux_092_GLEAMX2',
# 'err_int_flux_099_GLEAMX2',
# 'err_int_flux_107_GLEAMX2',
# 'err_int_flux_115_GLEAMX2',
# 'err_int_flux_122_GLEAMX2',
# 'err_int_flux_130_GLEAMX2',
# 'err_int_flux_143_GLEAMX2',
# 'err_int_flux_151_GLEAMX2',
# 'err_int_flux_158_GLEAMX2',
# 'err_int_flux_166_GLEAMX2',
# 'err_int_flux_174_GLEAMX2',
# 'err_int_flux_181_GLEAMX2',
# 'err_int_flux_189_GLEAMX2',
# 'err_int_flux_197_GLEAMX2',
# 'err_int_flux_204_GLEAMX2',
# 'err_int_flux_212_GLEAMX2',
# 'err_int_flux_220_GLEAMX2',
# 'err_int_flux_227_GLEAMX2',
# "E_Total_flux_Source_LOW2",
# "E_Total_flux_MID2",
# "E_Total_flux_HIGH2"
# ]

e_flux_keys = [
"err_int_flux_corr",
'err_int_flux_W_087_GLEAMX',
'err_int_flux_W_118_GLEAMX',
'err_int_flux_W_154_GLEAMX',
'err_int_flux_W_185_GLEAMX',
'err_int_flux_W_215_GLEAMX',
"E_Total_flux_Source_LOW",
"E_Total_flux_MID",
"E_Total_flux_HIGH",
]

e_flux_keys2 = [
"err_int_flux_corr2",
'err_int_flux_W_087_GLEAMX2',
'err_int_flux_W_118_GLEAMX2',
'err_int_flux_W_154_GLEAMX2',
'err_int_flux_W_185_GLEAMX2',
'err_int_flux_W_215_GLEAMX2',
"E_Total_flux_Source_LOW2",
"E_Total_flux_MID2",
"E_Total_flux_HIGH2"
]

flux_multipliers = [1.]*len(freqs)
# flux_multipliers[-2] = 0.001
# flux_multipliers[-1] = 0.001
# flux_multipliers[-3] = 0.001

def from_index(x, x1, y1, index):
    """Calculate flux from measured value and measured/assumed index."""
    return y1*(x/x1)**index


def two_point_index(x1, x2, y1, y2):
    """Calculate spectral index from two measurements."""
    return np.log10(y1/y2)/np.log10(x1/x2)


def powerlaw(x, a, b):
    """Simple powerlaw function."""
    return a*(x**b)


def cpowerlaw(x, a, b, c):
    """Simple curved powerlaw function."""
    return a*(x**b)*np.exp(c*np.log(x)**2)


def cpowerlaw_from_ref(x, x0, y0, b, c):
    """Simple curved powerlaw function from reference value."""
    return y0*((x**b)/(x0**b)) * (np.exp(c*np.log(x)**2) / np.exp(c*np.log(x0)**2))


def powerlaw_amplitude(x0, y0, b):
    return y0/(x0**b)


def cpowerlaw_amplitude(x0, y0, b, c):
    """Return amplitude of curved powerlaw model."""
    return y0 / (x0**b * np.exp(c*np.log(x0)**2))


def fit(x, y, yerr=None, func=powerlaw, params=None):
    """Fit simple power law model."""

    if func == powerlaw and params is None:
        params = [powerlaw_amplitude(x[0], y[0], -1.), -1.]
        # logger.debug("initial params: {:.2f}, {:.2f}".format(params[0], params[1]))
    elif func == cpowerlaw and params is None:
        params = [cpowerlaw_amplitude(x[0], y[0], -2, 0), -2, 0]
        # logger.debug("initial params: {:.2f}, {:.2f}, {:.2f}".format(params[0], params[1], params[2]))

    if yerr is not None:
        yerr = np.asarray(yerr)

    popt, pcov = curve_fit(func, np.asarray(x), np.asarray(y), params,
        absolute_sigma=True,
        method="lm",
        sigma=yerr,
        maxfev=10000,
    )
    perr = np.sqrt(np.diag(pcov))   

    return popt, perr


def matched_catalogue_fitting(catalogue, 
    gleam300_flux_scale=0.13, 
    gleam300_flux_scale_key="flux_scale_err",
    gleam300_flux_key="int_flux_corr",
    gleam300_err_flux_key="err_int_flux_corr",
    overwrite=True):
    """Fit SEDs for matched sources."""

    table = Table.read(catalogue)
    flux_scale = np.full((len(freqs),), 0.08)
    flux_scale[0] = gleam300_flux_scale
    flux_scale[-3] = 0.07
    flux_scale[-2] = 0.06
    flux_scale[-1] = 0.1

    for key in ["pl_a", "pl_b", "cpl_a", "cpl_b", "cpl_c"]:
        if key not in table.columns:
            table[key] = np.full((len(table),), np.nan)
        elif not overwrite:
            logger.warning("SEDs already fit!")
            return
    for key in ["e_pl_a", "e_pl_b", "e_cpl_a", "e_cpl_b", "e_cpl_c"]:
        if key not in table.columns:
            table[key] = np.full((len(table),), np.nan)

    table[e_flux_keys2[0]] = np.sqrt(
        table[e_flux_keys[0]]**2 + (table["flux_scale_err"]*table[flux_keys[0]])**2
    )
    for i in range(1, len(e_flux_keys2)):

        

        key = e_flux_keys2[i]
        
        if i == 0:
            table[key] = np.sqrt(
                table[e_flux_keys[i]]**2 + (table[gleam300_flux_scale_key]*table[flux_keys[i]])**2
            )*flux_multipliers[i]

        else:
            table[key] = np.sqrt(
                table[e_flux_keys[i]]**2 + (flux_scale[i]*table[flux_keys[i]])**2
            )*flux_multipliers[i]

    for i, key in enumerate(flux_keys):

        table[key] *= flux_multipliers[i]

    pbar = trange(len(table))
    for i in pbar:

        flux = np.array(
            [table[key][i] for key in flux_keys]
        )
        eflux = np.array(
            [table[key][i] for key in e_flux_keys2]
        )

        try:
            popt, perr = fit(freqs, flux, eflux, func=powerlaw, params=None)
            copt, cerr = fit(freqs, flux, eflux, func=cpowerlaw, params=None)
        except (ValueError, RuntimeError):
            continue

        table["pl_a"][i] = popt[0]
        table["pl_b"][i] = popt[1]
        table["e_pl_a"][i] = perr[0]
        table["e_pl_b"][i] = perr[1]

        table["cpl_a"][i] = copt[0]
        table["cpl_b"][i] = copt[1]
        table["cpl_c"][i] = copt[2]
        table["e_cpl_a"][i] = cerr[0]
        table["e_cpl_b"][i] = cerr[1]
        table["e_cpl_c"][i] = cerr[2]

    table.write(catalogue, overwrite=True)


def extract_psf(psf_map, coords):
    with fits.open(psf_map) as f:

        wcs = WCS(f[0].header).celestial
        x, y = wcs.all_world2pix(coords[0], coords[1], 0)
        x = int(x)
        y = int(y)
        bmaj = f[0].data[0, y, x]
        bmin = f[0].data[1, y, x]
        bpa = f[0].data[2, y, x]
        try:
            blur = f[0].data[3, y, x]
        except IndexError:
            blur = 1.

    
    logger.debug("{} {} {} {}".format(psf_map,bmaj,bmin,bpa,blur))
    return bmaj, bmin, bpa, blur

def extract_rms(rms_map, coords):
    with fits.open(rms_map) as f:

        wcs = WCS(f[0].header).celestial
    
        x, y = wcs.all_world2pix(coords[0], coords[1], 0)
        x = int(x)
        y = int(y)

        rms = f[0].data[y, x]

    return rms


def fornax(
    region="fornaxA.reg", 
    pbflux="PB2017.fits", outname="FornaxA_measures.fits",
    snr_params=[0.232, 0.514, -0.074],
    snr_params_err=[0.021, 0.026, 0.007],
    ):
    """
    """

    awkward_table = """PKS J0133-3629&1.0440\pm0.0010&-0.662\pm0.002  &-0.225\pm0.006  &       &      &      &267&0.2 - 4 & 900
3C48      &1.3253\pm0.0005&-0.7553\pm0.0009&-0.1914\pm0.0011&  0.0498\pm0.0009&      &      &3.1&0.05 -50  & 1.2 
Fornax A  &2.218\pm0.003  &-0.661 \pm0.006 &                &&&          &17&0.2 -0.5  & 3000
3C123     &1.8017\pm0.0007&-0.7884\pm0.0012&-0.1035\pm0.0023&-0.0248\pm0.0013&0.0090\pm0.0013&      &1.9&0.05 -50 & 43  
PKS J0444-2809&0.9710\pm0.0011&-0.894\pm0.004  &-0.118\pm0.010  &       &      &      &3.3&0.2 -2.0  & 120
Pictor A  &1.9380\pm0.0010&-0.7470\pm0.0013&-0.074\pm0.005  &       &      &      &8.1&0.2 -4.0  & 480 
3C138     &1.0088\pm0.0009&-0.4981\pm0.0022&-0.155\pm0.003  &-0.010\pm0.07&0.022\pm0.003&      &1.5&0.2 -5 0 & 0.65 
Taurus A  &2.9516\pm0.0010&-0.217\pm0.003  &-0.047\pm0.005  &-0.067\pm0.013&      &      &1.9&0.05 -4.0 & 480 
3C147     &1.4516\pm0.0010&-0.6961\pm0.0017&-0.201\pm0.005  & 0.064\pm0.004&-0.046\pm0.004& 0.029\pm0.003&2.2&0.05 -50 & 0.7 
3C196     &1.2872\pm0.0007&-0.8530\pm0.0012&-0.153\pm0.002  &-0.0200\pm0.0013& 0.0201\pm0.0013&      &1.6&0.050 -50  & 6.0
Hydra A   &1.7795\pm0.0009&-0.9176\pm0.0012&-0.084\pm0.004  &-0.0139\pm0.0014&0.030\pm0.003&      &3.5&0.050 -12  & 420
Virgo A   &2.4466\pm0.0007&-0.8116\pm0.0020&-0.048\pm0.003  &      &      &      &2.0&0.05 -3  & 840
3C286     &1.2481\pm0.0005&-0.4507\pm0.0009&-0.1798\pm0.0011& 0.0357\pm0.0009&      &      &1.9&0.05 -50 & 3.5
3C295     &1.4701\pm0.0007&-0.7658\pm0.0012&-0.2780\pm0.0023&-0.0347\pm0.0013& 0.0399\pm0.0013&      &1.6&0.05 -50 & 6.5 
Hercules A&1.8298\pm0.0007&-1.0247\pm0.0009&-0.0951\pm0.0020&      &      &      &2.3&0.2 -12 & 195
3C353     &1.8627\pm0.0010&-0.6938\pm0.0014&-0.100\pm0.005  &-0.032\pm0.0005&      &      &2.2&0.2 -4 & 320 
3C380     &1.2320\pm0.0016&-0.791\pm0.004  &0.095\pm0.022   & 0.098\pm0.022&-0.18\pm0.06&-0.16\pm0.05&2.9&0.05 -50 & 18 
Cygnus A  &3.3498\pm0.0010&-1.0022\pm0.0014&-0.225\pm0.006  & 0.023\pm0.002&0.043\pm0.005&      &1.9&0.05 -12 & 110
3C444     &1.1064\pm0.0009&-1.005\pm0.002  &-0.075\pm0.004  &-0.077\pm0.005&      &      &5.7&0.2 -12  & 120
Cassiopeia A&3.3584\pm0.0010&-0.7518\pm0.0014&-0.035\pm0.005&-0.071\pm0.005&     &      &2.1&0.2 -4 & 480
PKS B1934-638&-30.7667\pm0&26.4908\pm0&-7.0977\pm0&0.605334\pm0& & & 0 & 0 & 1
"""

    def parse_awkward_table(s):
        lines = s.split("\n")[:-1]
        sources = {}
        sizes = {}
        for line in lines:
            # source = line.split("&")[0].rstrip().replace(" ", "")
            source = line.split("&")[0].rstrip()
            # if source in SOURCES_TO_USE:
            params = line.split("&")[1:-3]
            args = []
            for param in params:
                p = param.split("\pm")[0]
                try:
                    args.append(float(p))
                except Exception:
                    args.append(np.nan)
            sources[source] = args
            size = float(line.split("&")[-1])
            sizes[source] = size
        return sources, sizes
    sources, sizes = parse_awkward_table(awkward_table)

    def logpoly(nu, *args):
        terms = [args[i]*np.log10(nu)**i for i in range(len(args))]
        # logger.debug(terms)
        return 10**np.nansum(terms)

    source = "Fornax A"
    

    gleam_images = [
        "FornaxA_072-103.fits",
        "FornaxA_103-134.fits",
        "FornaxA_170-231.fits",
        "GLEAM300_J0336-34_cutout.fits"
    ]

    narrow_bands = ['103-111', '111-118', '118-126', '126-134',
                   '139-147', '147-154', '154-162', '162-170',
                   '170-177', '177-185', '185-193', '193-200',
                   '200-208', '208-216', '216-223', '223-231']

    gleam_sgp_images = [
        "mosaic_{}MHz_yr1-2.fits".format(f) for f in narrow_bands
    ]
    gleam_sgp_freqs = np.array([
        np.mean([float(i) for i in bit.split("-")]) for bit in narrow_bands
    ])
    logger.debug(gleam_sgp_freqs)

    gleam_uncs = [
        0.08, 0.08, 0.08, 0.13
    ]

    gleam_sgp_uncs = [0.08]*len(gleam_sgp_images)


    pbtable = Table.read(pbflux)
    pbfluxes = pbtable["ForA"][1:16]
    pbfreqs = pbtable["Freq"][1:16]*1e3
    pbnames = ["VLA"]*len(pbfluxes)

    gleam_fluxes = np.full((4,), np.nan)
    gleam_errs = np.full((4,), np.nan)
    gleam_freqs = np.array([88., 118., 200., 300.])

    for i in range(len(gleam_images)):

        params = measures.measure_flux_density(
            fitsimage=gleam_images[i],
            rms=gleam_images[i].replace(".fits", "_rms.fits"),
            region=region,
            r_index=0,
            sigma=0.,
            verbose=True,
            full_region_error=True,
            outname=gleam_images[i].replace(".fits", "_ww.fits")
        )

        if i == 3:
            snr = params[0] / params[3]
            logger.debug("SNR: {}".format(snr))
            if snr > 150:
                fbias = stats.snr_func(snr, *snr_params)
                fbias_err = stats.snr_func_err(
                    snr, 
                    a=snr_params[0], 
                    b=snr_params[1], 
                    c=snr_params[2],
                    a_err=snr_params_err[0],
                    b_err=snr_params_err[1],
                    c_err=snr_params_err[2]
                )
            else:
                fbias = 1.
                fbias_err = 0.
        else:
            fbias = 1.
            fbias_err = 0.

        params[0] /= fbias
        gleam_fluxes[i] = params[0]
        gleam_errs[i] = np.sqrt(
            (params[0]*gleam_uncs[i])**2 + \
            params[2]**2 + \
            ((fbias_err/fbias)*params[0])**2
        )

        

    gleam_sgp_fluxes = np.full((len(gleam_sgp_freqs),), np.nan)
    gleam_sgp_errs = np.full((len(gleam_sgp_freqs),), np.nan)

    for i in range(len(gleam_sgp_images)):

        bmaj, bmin, bpa, blur = extract_psf(
            gleam_sgp_images[i].replace("_yr1-2.fits", "_psf_yr1-2.fits"),
            coords=(50.6701490, -37.211119)
        )
        rms = extract_rms(
            gleam_sgp_images[i].replace("_yr1-2.fits", "_rms_yr1-2.fits"),
            coords=(50.6701490, -37.211119)
        )

        params = measures.measure_flux_density(
            fitsimage=gleam_sgp_images[i],
            rms=rms,
            region=region,
            r_index=0,
            sigma=0.,
            verbose=True,
            bmaj=bmaj,
            bmin=bmin,
            full_region_error=True,
            outname=gleam_sgp_images[i].replace(".fits", "_ww.fits")
        )

        gleam_sgp_fluxes[i] = params[0]
        gleam_sgp_errs[i] = np.sqrt(
            (params[0]*gleam_sgp_uncs[i])**2 + \
            params[2]**2
        )

    
    

    fluxes = np.full((len(gleam_freqs) + len(gleam_sgp_freqs) + len(pbnames),), np.nan)
    errs = fluxes.copy()
    freqs = fluxes.copy()
    names = np.full((len(fluxes),), 0, dtype=int)

    fluxes[0:4] = gleam_fluxes
    errs[0:4] = gleam_errs
    freqs[0:4] = gleam_freqs
    names[0:3] = 0
    names[3] = 1

    fluxes[4:20] = gleam_sgp_fluxes
    errs[4:20] = gleam_sgp_errs
    freqs[4:20] = gleam_sgp_freqs
    names[4:20] = 2

    fluxes[20:] = pbfluxes
    freqs[20:] = pbfreqs
    names[20:] = 3

    model_x = np.logspace(np.log10(min(freqs)), np.log10(max(freqs)), 1000)
    model_y = np.array([logpoly(x/1e3, *sources["Fornax A"]) for x in model_x])

    table = Table(
        [names, freqs, fluxes, errs], names=["label", "freq", "flux", "err_flux"]
    )

    table.write(outname, overwrite=True)

    return table, model_x, model_y

