#! /usr/bin/env python

from typing import List, Any, Optional, Tuple

import os
import numpy as np
from glob import glob

from subprocess import Popen

from astropy.table import Table, vstack
from astropy.io import fits
from astropy.time import Time
from astropy.stats import akaike_info_criterion_lsq, bayesian_info_criterion_lsq

from astropy.coordinates import SkyCoord
from astropy import units as u

from scipy.signal import savgol_filter
from scipy.interpolate import UnivariateSpline

from skymodel import get_beam, parsers

from tqdm import trange

from racs_sf import utils
from gleam300.plotting import flux_ratio_with_model_by_night 
from gleam300.stats import window_filter

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def radec_to_lm(ra, dec, ra0, dec0):
    """Convert RA,DEC to l,m."""

    l = np.cos(dec)*np.sin(ra-ra0)
    m = np.sin(dec)*np.cos(dec0) - np.cos(dec)*np.sin(dec0)*np.cos(ra-ra0)

    return l, m


def get_beam_value(ra, dec, t, delays, freq):

    stokes_i = get_beam.beam_value(
        ra=ra,
        dec=dec,
        t=t,
        delays=delays,
        freq=freq,
        return_I=True
    )

    return stokes_i


def parse_metafits(metafits):
    """Read in metafits file and return relevant information."""

    m = fits.getheader(metafits)

    delays = [int(d) for d in m["DELAYS"].split(",")]
    t = Time(m["DATE-OBS"], format="isot", scale="utc")
    freq = m["FREQCENT"] * 1.e6  # in Hz
    pnt = SkyCoord(ra=m["RA"], dec=m["DEC"], unit=(u.deg, u.deg))


    return t, delays, freq, pnt
    # return pnt, delays


def shift_source_list(table : str,  pnt : SkyCoord, 
    ra_key : str = "ra",
    dec_key : str = "dec"
    ):
    """Shift source lists positions to be l,m coordinates in the beam frame."""


    data = Table.read(table)

    if "Source_l" not in data.columns:
        data["Source_l"] = np.full((len(data),), np.nan)
    if "Source_m" not in data.columns:
        data["Source_m"] = np.full((len(data),), np.nan)
    if "Beam_l" not in data.columns:
        data["Beam_l"] = np.full((len(data),), pnt.ra.value)
    if "Beam_m" not in data.columns:
        data["Beam_m"] = np.full((len(data),), pnt.dec.value)
    if "OBSID" not in data.columns:
        data["OBSID"] = np.full((len(data),), int(table[0:10]))

    # data["Source_l"] = (data[ra_key] - data["Beam_l"])*np.cos(np.radians(data[dec_key]))
    # data["Source_m"] = data[dec_key] - pnt.dec.value
    data["Source_l"], data["Source_m"] = radec_to_lm(
        ra=np.radians(data[ra_key]),
        ra0=np.radians(pnt.ra.value),
        dec=np.radians(data[dec_key]),
        dec0=np.radians(pnt.dec.value)
    )

    data["Source_l"] = np.degrees(data["Source_l"])
    data["Source_m"] = np.degrees(data["Source_m"])

    return data


def get_beam_position(metafits : str, beam_dec : Optional[float] = None):
    t, delays, freq, pnt = parsers.parse_metafits(metafits)
    hdu = get_beam.make_beam_image(t, delays, freq, ra=pnt.ra.value, return_hdu=True)
    lobe_table = get_beam.find_lobes(hdu,
        perc=0.05,
        centroid=True,
        return_table=True
    )
    lobe_coords = SkyCoord(
        ra=lobe_table["ra"],
        dec=lobe_table["dec"],
        unit=(u.deg, u.deg)
    )
    pnt = SkyCoord(ra=pnt.ra, dec=beam_dec*u.deg)
    lobe_seps = pnt.separation(lobe_coords).value
    main_lobe = np.argmin(lobe_seps)
    main_lobe_coords = lobe_coords[main_lobe]
    return main_lobe_coords



def shift_multiple_source_lists(source_lists : List[str], 
    outname : str,
    ra_key : str = "old_ra",
    dec_key : str = "old_dec",
    beam_dec : Optional[float] = None,
    get_stokes_i : bool = False
    ):
    """Shift multiple source lists.
    """

    stacked_table = None

    pbar = trange(len(source_lists))

    for i in pbar:

        source_list = source_lists[i]

        obsid = os.path.basename(source_list)[0:10]

        pbar.set_description(f"{obsid}")

        if not os.path.exists(source_list):
            logger.warning(f"No {source_list} not found - skipping.")
            continue

        if not os.path.exists(f"{obsid}.metafits"):
            with open("make_metafits.log", "a+") as log:
                Popen("wget http://ws.mwatelescope.org/metadata/fits/?obs_id={0} -q -O {0}.metafits".format(obsid),
                    shell=True,
                    stdout=log,
                    stderr=log,
                ).wait()

        try:
            t, delays, freq, pnt = parse_metafits(f"{obsid}.metafits")
        except OSError:
            continue

        if beam_dec is not None:
            pnt = get_beam_position(
                metafits=f"{obsid}.metafits",
                beam_dec=beam_dec
            )

        table_i = shift_source_list(
            table=source_list,
            pnt=pnt,
            ra_key=ra_key,
            dec_key=dec_key
        ) 

        if get_stokes_i:
            table_i["StokesI"] = get_beam_value(
                ra=table_i[ra_key],
                dec=table_i[dec_key],
                t=t,
                delays=delays,
                freq=freq
            )

        if stacked_table is None:
            stacked_table = table_i
        else:
            stacked_table = vstack([stacked_table, table_i])

    stacked_table.write(outname, overwrite=True)


def split_by_obsid_group(stacked_table, max_sep=86400):

    table = Table.read(stacked_table)
    table.sort(keys="OBSID")

    groups = {}
    for i in range(len(table)):
        if i == 0:
            n = 0
            obsid_1 = table["OBSID"][i]
        elif i == len(table)-1:
            obsid_2 = table["OBSID"][i]
            groups["{}_{}".format(obsid_1, obsid_2)] = [n, i]
        else:
            if abs(table["OBSID"][i]-table["OBSID"][i-1]) > max_sep:
                obsid_2 = table["OBSID"][i-1]
                groups["{}_{}".format(obsid_1, obsid_2)] = [n, i-1]
                n = i
                obsid_1 = table["OBSID"][i]
        
    for g in groups.keys():

        table[
            groups[g][0]:groups[g][1]
        ].write(stacked_table.replace(".fits", "") + "_{}.fits".format(g), overwrite=True)


def make_dummy_beam_image(outname, 
    npix=5500, 
    cellsize=0.0038297, 
    dec=0.,
    catalogue=None):

    if catalogue is not None:
        table = Table.read(catalogue)
        ra = np.mean(table["Source_l"])
        dec = np.mean(table["Source_m"])
    else:
        ra = 0.

    hdu = fits.PrimaryHDU()
    arr = np.full((npix, npix), 0.)
    hdu.data = arr

    hdu.header["CTYPE1"] = "RA---SIN"
    hdu.header["CTYPE2"] = "DEC--SIN"
    hdu.header["CRVAL1"] = ra
    hdu.header["CRVAL2"] = dec
    hdu.header["CDELT1"] = -cellsize
    hdu.header["CDELT2"] = cellsize
    hdu.header["CRPIX1"] = npix//2 - 1
    hdu.header["CRPIX2"] = npix//2 - 1

    hdr = hdu.header

    fits.writeto(outname, arr, hdr, overwrite=True)





def get_flux_ratio(stacked_table, alpha=-0.75):
    """
    """

    table = Table.read(stacked_table)
    table["ratio"] = np.full((len(table),), np.nan)

    for i in range(len(table)):

        if not np.isnan(table["alpha"][i]):

            table["ratio"][i] = table["flux"][i]/(
                table["S_200"][i]*((300./200.)**(table["alpha"][i]))
            )
        else:
            table["ratio"][i] = table["flux"][i]/(
                table["S_200"][i]*(300./200.)**(alpha)
            )

    table.write(stacked_table, overwrite=True)



def make_binned_grid(stacked_table, cellsize, ncells, 
    min_n=1,
    func=np.nanmedian):

    table = Table.read(stacked_table) 
    outname = stacked_table.replace(".fits", "") + "_binned"

    n_cellsx = ncells
    n_cellsy = ncells

    boundaryx = abs(cellsize*n_cellsx)/2
    boundaryy = abs(cellsize*n_cellsy)/2

    mean_l = np.mean(table["Source_l"])
    mean_m = np.mean(table["Source_m"])

    

    xg, cellx = np.linspace(mean_l-boundaryx, mean_l+boundaryx, n_cellsx, retstep=True)
    yg, celly = np.linspace(mean_m-boundaryy, mean_m+boundaryy, n_cellsy, retstep=True)

    xh, yh = np.meshgrid(xg, yg)

    cell_number = []
    cell_median = []
    cell_beam = []
    cell_std = []
    fill = np.nan

    for x, y in zip(xh.flat, yh.flat):

        mkx = (table["Source_l"] > (x-cellx/2)) & (table["Source_l"] < (x+cellx/2))
        mky = (table["Source_m"] > (y-celly/2)) & (table["Source_m"] < (y+celly/2))

        isin = np.where(mkx & mky)[0]

        n = len(table["ratio"][isin])

        if n > min_n:
            
            if func == np.nanmedian:
                cell_median.append(func(table["ratio"][isin]))
            elif func == np.average:
                cell_median.append(
                    func(table["ratio"][isin], 
                    weights=1/(table["flux"][isin]/table["local_rms"][isin]))
                )
            try:
                cell_std.append(np.nanstd(table["ratio"][isin]))
            except Exception:
                # E.g. if only 1 entry:
                cell_std.append(fill)

            cell_number.append(n)

        else:
           
            cell_median.append(fill)
            cell_std.append(fill)
            cell_number.append(fill)
        
        

    array_number = np.array(cell_number).reshape(xh.shape)
    array_median = np.array(cell_median).reshape(xh.shape)
    array_std = np.array(cell_std).reshape(xh.shape)

    hdu = fits.PrimaryHDU()
    hdu.header["CTYPE1"] = "RA---SIN"
    hdu.header["CRVAL1"] = mean_l
    hdu.header["CDELT1"] = -cellx
    hdu.header["CRPIX2"] = n_cellsy // 2 + n_cellsy%2
    hdu.header["CTYPE2"] = "DEC--SIN"
    hdu.header["CRVAL2"] = mean_m
    hdu.header["CDELT2"] = celly
    hdu.header["CRPIX1"] = n_cellsx // 2 + n_cellsx%2
    hdu.header['EQUINOX'] = 2.000000000000E+03
    hdu.header['RADESYS'] = 'FK5     '
    hdu.header['SPECSYS'] = 'TOPOCENT'

    hdu.data = np.fliplr(array_median)
    hdu.writeto(outname + ".median.fits", overwrite=True)

    hdu.data = np.fliplr(array_number)
    hdu.writeto(outname + ".number.fits", overwrite=True)

    hdu.data = np.fliplr(array_std)
    hdu.writeto(outname + ".stddev.fits", overwrite=True)

    

    table_out = Table(
        [
            array_median.flatten(),
            array_number.flatten(),
            array_std.flatten(),
            xh.flatten(),
            yh.flatten()
        ],
        names=[
            "ratio",
            "number",
            "std",
            "Source_l",
            "Source_m"
        ]
    )

    idx = np.where(np.isfinite(table_out["ratio"]))

    table_out[idx].write(
        stacked_table.replace(".fits", "") + "_binned.fits",
        overwrite=True
    )

    return array_median, array_number, array_std, hdu, xg, yg, celly


def make_matched_lists(strip, 
    stub="_deep-MFS-image-pb_matched.fits",
    alpha=-0.75,
    get_stokes_i=False,
    overwrite=True):
    """
    """

    files = glob(f"*{stub}")
    outname = f"{strip}_matched"

    if not os.path.exists(outname+".fits") or overwrite:
        shift_multiple_source_lists(
            source_lists=files,
            outname=outname+".fits",
            get_stokes_i=False
        )

        get_flux_ratio(outname+".fits", alpha=alpha)

    split_by_obsid_group(outname+".fits")

    groups = glob(f"{strip}_matched_??????????_??????????.fits")

    if get_stokes_i:
        for group in groups:

            make_dummy_beam_image(
                outname=group.replace(".fits", "_beam.fits"),
                npix=401,
                cellsize=5./60.,
                catalogue=group
            )

    return groups


def smooth(y, box_pts):
    box = np.ones(box_pts)/box_pts
    y_smooth = np.convolve(y, box, mode='same')
    return y_smooth


def fit_flux_density_ratio(matched_list, 
    method="poly", 
    box_size=10, 
    polyorder=3,
    ratio_range=(0.1, 10),
    polydegrees=[i for i in range(20)],
    selector="bic",
    smooth=0.005,
    max_jump=0.1,
    min_group_fit=5,
    filter=True,
    plot_name_stub=".png",
    cmap="gnuplot2"):
    """"""


    table = Table.read(matched_list)
    logger.info(f"{matched_list}")

    obsids = list(set(list(table["OBSID"])))
    obsids.sort()

    ratios = np.full((len(obsids),), np.nan)
    weights = np.full((len(obsids),), 1e-30)
    stds = np.full((len(obsids),), np.nan)

    try:

        for i in range(len(obsids)):

            cond1 = table["OBSID"] == obsids[i]
            cond2 = np.isfinite(table["ratio"])
            cond3 = np.isfinite(table["eflux"])
            idx = np.where(cond1 & cond2 & cond3)[0]

            ratio, sum_weights = np.average(table["ratio"][idx], 
                weights=1/table["eflux"][idx]**2,
                returned=True
            )
            # std = np.nanstd(table["ratio"][idx])
            std = np.sqrt(np.cov(table["ratio"][idx], aweights=1/table["eflux"][idx]**2))

            if ratio_range[0] < ratio < ratio_range[1]:

                ratios[i] = ratio
                weights[i] = sum_weights
                stds[i] = std



        if filter:
            filtered_ratios = window_filter(ratios, 5)



        obsids = np.asarray(obsids)
        group_id = np.full((len(obsids),), 0, dtype=int)
        fit_obsids = obsids[np.where(np.isfinite(filtered_ratios))[0]]

        original_ratios = ratios.copy()
        weights = weights[np.where(np.isfinite(filtered_ratios))[0]]
        # stds = stds[np.where(np.isfinite(ratios))[0]]
        ratios = filtered_ratios[np.where(np.isfinite(filtered_ratios))[0]] 
        logger.debug(ratios)
        logger.debug(fit_obsids)

        if len(ratios) < 3:
            raise RuntimeError("Not enough ratios outside of {}".format(ratio_range))

        groups = {}
        n = 0
        i = 1
        init_group = fit_obsids[0]
        while i < len(fit_obsids):
            diff = np.diff(ratios)
            if abs(np.log10(ratios[i]) - np.log10(ratios[i-1])) > np.std(diff)+np.mean(abs(diff)) or \
                abs(np.log10(ratios[i]) - np.log10(ratios[i-1])) > max_jump:
                groups[n] = (init_group, fit_obsids[i-1])
                init_group = fit_obsids[i]
                n += 1
            elif i == len(fit_obsids)-1:
                groups[n] = (init_group, fit_obsids[i])

            i += 1


        logger.debug(groups)

        if method == "all_poly":
            table_cols = [obsids, ratios]
            table_names = ["OBSID", "ratio"]
            for deg in polydegrees:
                fit, params = np.polynomial.Polynomial.fit(
                    obsids, ratios,
                    w=weights,
                    deg=deg,
                    full=True 
                )
                table_cols.append(
                    fit(obsids)
                )
                table_names.append("model{}".format(deg))
                

            table_out = Table(table_cols, names=table_names)
        
            table_out.write(matched_list.replace(".fits", "_filtered.fits"), overwrite=True)
            return None
        

        elif method == "mean":
            mean = np.average(ratios, weights=weights)
            filtered = np.full((len(obsids),), mean)
            group_id = np.full((len(obsids),), -1, dtype=int)
        elif method == "none":
            filtered = original_ratios
            group_id = np.full((len(obsids),), -1, dtype=int)
            


        elif method == "savgol":
            filtered = savgol_filter(ratios, window_length=box_size, polyorder=3)
        elif method == "convolve":
            filtered = smooth(ratios, box_pts=box_size)
        elif method == "poly":


            group_fits = []
            groups_with_fits = []
            group_median_obsid = []

            for i in groups.keys():

                logger.debug("Group {}".format(i))

                aics = []
                fits = []

                cond1 = fit_obsids >= groups[i][0]
                cond2 = fit_obsids <= groups[i][1]
                idx = np.where(cond1 & cond2)[0]
                if len(idx) < min_group_fit:
                    continue

                group_ratios = ratios[idx]
                group_obsids = fit_obsids[idx]
                group_weights = weights[idx]

                for deg in polydegrees:
                    if len(group_ratios) < deg-1:
                        break
                    try:
                        fit, params = np.polynomial.Polynomial.fit(
                            group_obsids, group_ratios,
                            w=group_weights,
                            deg=deg,
                            full=True 
                        )
                        ssr1 = np.nansum((fit(group_obsids) - group_ratios)**2)
                        if selector == "aic":
                            aic = akaike_info_criterion_lsq(ssr1, len(fit.convert().coef), len(group_ratios))
                        else:
                            aic = bayesian_info_criterion_lsq(ssr1, len(fit.convert().coef), len(group_ratios))
                        aics.append(aic)
                        fits.append(fit)
                    except ZeroDivisionError:
                        break
                min_aic = np.argmin(np.asarray(aics))
                deg1 = polydegrees[min_aic]
                logger.info("Group {} - min AIC: {}".format(i, deg1))
                final_deg = deg1
                fit = fits[min_aic]
                group_fits.append(fit)
                group_median_obsid.append(np.nanmedian(group_obsids))
                groups_with_fits.append(i)
                # filtered = fit(obsids)


            group_median_obsid = np.asarray(group_median_obsid)

            logger.debug(group_median_obsid)
            logger.debug(groups_with_fits)

            filtered = np.full((len(obsids),), np.nan)
            for i in range(len(filtered)):
                # idx = (np.abs(group_median_obsid - obsids[i])).argmin()

                use_fit = False
                for g in groups_with_fits:
                    if groups[g][0] <= obsids[i] <= groups[g][1]:
                        use_fit = True
                        break

                if use_fit:
                    diffs1 = [abs(obsids[i]-groups[n][0]) for n in groups_with_fits]
                    diffs2 = [abs(obsids[i]-groups[n][1]) for n in groups_with_fits]
                    min_sep = [min([diffs1[n], diffs2[n]]) for n in range(len(diffs1))]
                    idx = np.argmin(min_sep) 
                    fit = group_fits[idx]
                    # logger.debug(group_median_obsid[idx])
                    filtered[i] = fit(obsids[i])
                    group_id[i] = groups_with_fits[idx]

            good_obsids = obsids[np.where(np.isfinite(filtered))[0]]
            good_filtered = filtered[np.where(np.isfinite(filtered))[0]]

            for i in range(len(filtered)):
                if np.isnan(filtered[i]):
                    filtered[i] = good_filtered[np.argmin(abs(obsids[i]-good_obsids))]
                    group_id[i] = -1 


        elif method == "spline":
            spline = UnivariateSpline(fit_obsids, ratios, w=weights, s=smooth)
            filtered = spline(obsids)

    except ValueError:
        original_ratios = ratios
        filtered = np.full((len(ratios),), np.nanmedian(ratios))
        group_id = np.full((len(ratios),), -1, dtype=int)


    residuals = original_ratios / filtered



    table_out = Table(
        [obsids, original_ratios, stds, filtered, residuals, group_id],
        names=["OBSID", "ratio", "std", "model", "res", "group_id"]
    )

    table_out.write(matched_list.replace(".fits", "_filtered.fits"), overwrite=True)

    flux_ratio_with_model_by_night(
        table=matched_list.replace(".fits", "_filtered.fits"),
        outname=matched_list.replace(".fits", f"_filtered{plot_name_stub}"),
        cmap=cmap
    )




def create_brightness_scale_db(strip, obsid_list, stub="_pb"):

    tables = glob(f"{strip}{stub}_matched_??????????_??????????_filtered.fits")
    obsids = []

    with open(obsid_list) as f:
        lines = f.readlines()
        for line in lines:
            if "#" not in line:
                bits = line.split()
                obsid = int(bits[0])
                if obsid not in obsids:
                    obsids.append(obsid)

    obsids = np.asarray(obsids)
    table = vstack([Table.read(t) for t in tables])

    correction_factors = np.full((len(obsids),), 1.)

    for i in range(len(obsids)):

        idx = np.argmin(np.abs(table["OBSID"] - obsids[i]))
        if table["OBSID"][idx] != obsids[i]:
            logger.debug("{} using model from {}".format(
                obsids[i], table["OBSID"][idx]))
        correction_factors[i] = table["model"][idx]


    factor_table = Table([obsids, correction_factors], 
        names=["OBSID", "model"]
    )

    factor_table.write(f"{strip}_correction_factors.fits", overwrite=True)