#! /usr/bin/env python


from typing import Optional, List, Tuple, Callable

import os
import numpy as np
from astropy.table import Table
import sqlite3
from subprocess import Popen

from astropy.coordinates import SkyCoord
from astropy import units as u

from scipy.stats import binned_statistic, iqr
from scipy.optimize import curve_fit

from uncertainties import ufloat

from astropy.io import fits

from tqdm import trange

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def make_metafits(obsid, outdir):
    with open("make_metafits.log", "a+") as log:
        Popen("wget http://ws.mwatelescope.org/metadata/fits/?obs_id={0} -O {1}/{0}.metafits".format(obsid, outdir),
              stdout=log, stderr=log, shell=True).wait()

def powerlaw(x, x0, y0, alpha):
    return y0*(x/x0)**alpha




def fit(
    f : Callable, 
    x : np.ndarray, 
    y : np.ndarray, 
    p0 : np.ndarray, 
    yerr : np.ndarray = None, 
    return_pcov : bool = False) -> Tuple[np.ndarray, np.ndarray]:
    """Wrapper for scipy's curve_fit."""

    if yerr is not None:
        yerr = np.asarray(yerr)

    popt, pcov = curve_fit(f, np.asarray(x), np.asarray(y), 
                           p0=p0,
                           absolute_sigma=True,
                           method="lm",
                           sigma=yerr,
                           maxfev=100000)

    perr = np.sqrt(np.diag(pcov))

    if return_pcov:
        return popt, pcov
    else:
        return popt, perr
    


def rms_obsids(catalogues, 
    ext="votable", 
    local_rms="local_rms",
    ra_key="ra",
    dec_key="dec",
    use_std=False,
    max_sources=2000):
    """Get rms from catalogues."""

    rms = np.full((len(catalogues),), np.nan)
    rms16 = np.full((len(catalogues),), np.nan)
    rms84 = np.full((len(catalogues),), np.nan)
    obsids = np.full((len(catalogues),), 0)
    ra = np.full((len(catalogues),), np.nan)
    dec = np.full((len(catalogues),), np.nan)
    stds = np.full((len(catalogues),), np.nan)
    pbar = trange(len(catalogues))
    for i in pbar:    
        table = Table.read(catalogues[i], format=ext)
        
        # pbar.set_description("{}".format(catalogues[i]))
        try:
            if len(table) > max_sources:
                raise ValueError
            rms_obsid = np.nanmedian(table[local_rms])
            if use_std:
                std = np.nanstd(table[local_rms])
                rms16_obsid = std
                rms84_obsid = std
            else:
                rms16_obsid = np.percentile(table[local_rms], 16.)
                rms84_obsid = np.percentile(table[local_rms], 84.)
            std_obsid = np.nanstd(table[local_rms])
            rms[i] = rms_obsid
            rms16[i] = rms16_obsid
            rms84[i] = rms84_obsid
            stds[i] = std_obsid 
            ra[i] = np.nanmean(table[ra_key])
            dec[i] = np.nanmean(table[dec_key])
            pbar.set_description("{}: {}: {}".format(catalogues[i], obsids[i], rms[i]))

            
        except ValueError:
            
            pass

        obsids[i] = int(os.path.basename(catalogues[i])[0:10])

    table = Table(
        [obsids, ra, dec, rms, rms16, rms84, stds],
        names=["OBSID", "RA", "DEC", "RMS", "RMS16", "RMS84", "RMS_STD"]
    )

    # return rms, rms16, rms84, obsids, ra, dec
    return table

def flux_ratio_obsids(catalogues,
    ra_key="old_ra",
    dec_key="old_dec",
    max_sources=2000,
    alpha=-0.8,
    survey_flux_key="S_200",
    survey_freq=200.,
    survey_flux_multiplier=1.):
    """Get flux ratio, from match_catalogues output."""

    rms = np.full((len(catalogues),), np.nan)
    rms16 = np.full((len(catalogues),), np.nan)
    rms84 = np.full((len(catalogues),), np.nan)
    obsids = np.full((len(catalogues),), 0)
    ra = np.full((len(catalogues),), np.nan)
    dec = np.full((len(catalogues),), np.nan)
    stds = np.full((len(catalogues),), np.nan)

    weighted_means = np.full((len(catalogues),), np.nan)
    weighted_stds = np.full((len(catalogues),), np.nan)
    sum_weights = np.full((len(catalogues),), np.nan)

    pbar = trange(len(catalogues))

    for i in pbar:

        try:

            table = Table.read(catalogues[i])

            if len(table) > max_sources:
                raise ValueError
            if survey_flux_key == "S_200":
                table["model_flux"] = powerlaw(300., 200., table["S_200"], table["alpha"])*survey_flux_multiplier
            else:
                table["model_flux"] = powerlaw(300., survey_freq, table[survey_flux_key+"_2"], alpha)*survey_flux_multiplier

            if "flux" in table.colnames:
                ratio = table["flux"]/table["model_flux"]
                key_app = ""
            
            else:
                ratio = table["int_flux_1"] / table["model_flux"]
                key_app = "_1"

            rms_obsid = np.nanmedian(ratio)
            rms16_obsid = np.percentile(ratio, 16.)
            rms84_obsid = np.percentile(ratio, 84.)
            std_obsid = np.nanstd(ratio)

            weighted_mean, sum_weight = np.average(
                ratio,
                weights=1/table["local_rms"+key_app]**2,
                returned=True
            )

            weighted_std = np.sqrt(
                np.cov(
                    ratio,
                    aweights=1/table["local_rms"+key_app]**2
                )
            )

            weighted_means[i] = weighted_mean
            weighted_stds[i] = weighted_std
            sum_weights[i] = sum_weight

            rms[i] = rms_obsid
            rms16[i] = rms16_obsid
            rms84[i] = rms84_obsid
            stds[i] = std_obsid 
            ra[i] = np.nanmean(table[ra_key])
            dec[i] = np.nanmean(table[dec_key])
            
            pbar.set_description("{}: {}: {}".format(catalogues[i], obsids[i], rms[i]))

        except (ValueError, ZeroDivisionError) as e:
            pass

        obsids[i] = int(os.path.basename(catalogues[i])[0:10])

    table = Table(
        [obsids, ra, dec, rms, rms16, rms84, stds,
            weighted_means, weighted_stds, sum_weights
        ],
        names=["OBSID", "RA", "DEC", 
            "FLUX_RATIO", "FLUX_RATIO16", "FLUX_RATIO84", "FLUX_RATIO_STD",
            "FLUX_RATIO_WMEAN",
            "FLUX_RATIO_WSTD",
            "FLUX_RATIO_WSUM"]
    )

    # return rms, rms16, rms84, obsids, ra, dec
    return table    


def int_peak_obsids(catalogues,
    int_flux="int_flux", 
    peak_flux="peak_flux",
    ra_key="ra",
    dec_key="dec",
    local_rms_key="local_rms",
    max_sources=2000):
    """Get rms from catalogues."""

    rms = np.full((len(catalogues),), np.nan)
    rms16 = np.full((len(catalogues),), np.nan)
    rms84 = np.full((len(catalogues),), np.nan)
    obsids = np.full((len(catalogues),), 0)
    ra = np.full((len(catalogues),), np.nan)
    dec = np.full((len(catalogues),), np.nan)
    stds = np.full((len(catalogues),), np.nan)

    weighted_means = np.full((len(catalogues),), np.nan)
    weighted_stds = np.full((len(catalogues),), np.nan)
    sum_weights = np.full((len(catalogues),), np.nan)

    pbar = trange(len(catalogues))
    for i in pbar:    
        try:
            table = Table.read(catalogues[i])
            if len(table) > max_sources:
                    raise ValueError

            ratio = table[int_flux]/table[peak_flux]
            rms_obsid = np.nanmedian(table[int_flux]/table[peak_flux])
            rms16_obsid = np.percentile(table[int_flux]/table[peak_flux], 16.)
            rms84_obsid = np.percentile(table[int_flux]/table[peak_flux], 84.)
            std_obsid = np.nanstd(ratio)

            weighted_mean, sum_weight = np.average(
                ratio,
                weights=1/table[local_rms_key]**2,
                returned=True
            )

            weighted_std = np.sqrt(
                np.cov(
                    ratio,
                    aweights=1/table[local_rms_key]**2
                )
            )

            weighted_means[i] = weighted_mean
            weighted_stds[i] = weighted_std
            sum_weights[i] = sum_weight


            rms[i] = rms_obsid
            rms16[i] = rms16_obsid
            rms84[i] = rms84_obsid
            stds[i] = std_obsid 
            ra[i] = np.nanmean(table[ra_key])
            dec[i] = np.nanmean(table[dec_key])
            pbar.set_description("{}: {}: {}".format(catalogues[i], obsids[i], rms[i]))

        except (ValueError, ZeroDivisionError) as e:
            pass

        obsids[i] = int(os.path.basename(catalogues[i])[0:10])

    table = Table(
        [
            obsids, ra, dec, rms, rms16, rms84, stds,
            weighted_means, weighted_stds, sum_weights
        ],
        names=["OBSID", "RA", "DEC", 
            "INT_PEAK", "INT_PEAK16", "INT_PEAK84", "INT_PEAK_STD",
            "INT_PEAK_WMEAN",
            "INT_PEAK_WSTD",
            "INT_PEAK_WSUM"
        ]
    )

    # return rms, rms16, rms84, obsids, ra, dec
    return table   


def get_compact(catalogue, flag_key="FLAG",
    bmaj_key="psf_a",
    bmin_key="psf_b",
    major_key="a",
    minor_key="b",
    size_ratio=0.1,
    use_median=True,
    snr_for_median=100,
    peak_flux_key="peak_flux",
    local_rms_key="local_rms"):
    """Apply flag for compact sources."""

    table = Table.read(catalogue)
    ratio = (table[major_key]*table[minor_key]) / \
        (table[bmaj_key]*table[bmin_key])
    print(ratio)

    if use_median:
        snr = table[peak_flux_key]/table[local_rms_key]
        idx = np.where(snr >= snr_for_median)[0]
        m = np.nanmedian(ratio[idx])
    else:
        m = 1.

    print(m)

    flags = np.full((len(table),), 0, dtype=int)

    lratio = np.log10(ratio)
    lm = np.log10(m)

    diff = np.abs(lm-lratio)
    print(diff)

    flag1 = np.where(diff > size_ratio)[0]

    flags[flag1] = 1

    table[flag_key] = flags

    table.write(catalogue, overwrite=True)



def window_filter(data, window_length, sigma=5):
    """Filter data based on a sliding window."""

    filtered_data = np.full((len(data),), np.nan)

    for i in range(len(data)):

        if i - window_length//2 < 0:
            lower = 0
        else:
            lower = i - window_length//2
        if i + window_length//2 > len(data)-1:
            upper = len(data)-1
        else:
            upper = i + window_length//2

        window = data[lower:upper]
        std = np.nanstd(window)
        med = np.nanmedian(window)
        if data[i] > med-sigma*std and data[i] < med+sigma*std:
            filtered_data[i] = data[i]

    return filtered_data 


def build_obsid_database(gleam_database, 
    indir, 
    strips, 
    calibrator_database, 
    outname):

    con = sqlite3.connect(gleam_database, timeout=120.)
    cur = con.cursor()

    cal_con = sqlite3.connect(calibrator_database)
    cal_cur = cal_con.cursor()
    cal_cur.execute("SELECT * FROM calibrators")
    cal_data = cal_cur.fetchall()
    cal_data = Table(np.asarray(cal_data), names=["CALID", "PROJECT", "SOURCE", "CHAN"])
    cal_data["CALID"] = cal_data["CALID"].astype(int)

    table = Table(names=(
        "OBSID", "STRIP", "RA", "DEC", 
        "RMS", "RMS16", "RMS84",
        "FLUX_RATIO", "FLUX_RATIO16", "FLUX_RATIO84", "FLUX_RATIO_STD", "FLUX_RATIO_WMEAN", "FLUX_RATIO_WSTD", "FLUX_RATIO_WSUM",
        "INT_PEAK", "INT_PEAK16", "INT_PEAK84", "INT_PEAK_STD", "INT_PEAK_WMEAN", "INT_PEAK_WSTD", "INT_PEAK_WSUM",
        "SOLUTIONS", "SOLUTIONS_TYPE",
    ),
        dtype=(
            "i4", "S10", "f4", "f4",
            "f4", "f4", "f4", "f4", "f4", "f4", "f4",
            "f4", "f4", "f4", "f4", "f4", "f4", "f4",
            "f4", "f4", "f4",
            "i4", "S2")
    )

    with open(strips) as f:
        lines = f.readlines()
        for line in lines:
            
            line = line.rstrip("\n")

            logger.info("Getting information for {}".format(line))

            rms_table = Table.read(f"{indir}/plots/{line}_obsids_rms_stats.fits")
            flux_table = Table.read(f"{indir}/plots/{line}_obsids_flux_stats.fits")
            intpeak_table = Table.read(f"{indir}/plots/{line}_obsids_intpeak_stats.fits")

            field = line.replace("GLEAM", "field")

            cur.execute(f"SELECT * FROM '{field}_235'")

            data = cur.fetchall()

            pbar = trange(len(data))
            for i in pbar:
                obsid = int(data[i][1])
                calid = int(data[i][6])

                if calid == obsid:
                    caltype = "S"  # self
                elif calid in cal_data["CALID"]:
                    caltype = "B"  # bandpass
                else:
                    caltype = "N"  # nearest

                pbar.set_description(f"{obsid} - {calid} - {caltype}")

                try:
                    mhdr = fits.getheader((f"{indir}/{line}/{obsid}.metafits"))
                except Exception:
                # if not os.path.exists(f"{indir}/{line}/{obsid}.metafits"):
                    make_metafits(obsid, f"{indir}/{line}")
                
                # with fits.open(f"{indir}/{line}/{obsid}.metafits") as m:
                ra, dec = mhdr["RA"], mhdr["DEC"]

                idx1 = np.where(rms_table["OBSID"] == obsid)[0]
                if len(idx1) == 0:
                    rms_i = (np.nan, np.nan, np.nan)
                else:
                    coords_i = (rms_table["RA"][idx1], rms_table["DEC"][idx1])
                    rms_i = (rms_table["RMS"][idx1], rms_table["RMS16"][idx1], rms_table["RMS84"][idx1])
                idx2 = np.where(flux_table["OBSID"] == obsid)[0]
                if len(idx2) == 0:
                    flux_i = (np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan)
                else:
                    flux_i = (
                        flux_table["FLUX_RATIO"][idx2], 
                        flux_table["FLUX_RATIO16"][idx2], 
                        flux_table["FLUX_RATIO84"][idx2],
                        flux_table["FLUX_RATIO_STD"][idx2],
                        flux_table["FLUX_RATIO_WMEAN"][idx2],
                        flux_table["FLUX_RATIO_WSTD"][idx2],
                        flux_table["FLUX_RATIO_WSUM"][idx2],
                        )
                idx3 = np.where(intpeak_table["OBSID"] == obsid)[0]
                if len(idx3) == 0:
                    intpeak_i = (np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan)
                else:
                    intpeak_i = (
                        intpeak_table["INT_PEAK"][idx3], 
                        intpeak_table["INT_PEAK16"][idx3], 
                        intpeak_table["INT_PEAK84"][idx3],
                        intpeak_table["INT_PEAK_STD"][idx3],
                        intpeak_table["INT_PEAK_WMEAN"][idx3],
                        intpeak_table["INT_PEAK_WSTD"][idx3],
                        intpeak_table["INT_PEAK_WSUM"][idx3])

                table.add_row(
                    [obsid, line.replace("GLEAM", ""), 
                    ra, dec,
                    rms_i[0], rms_i[1], rms_i[2],
                    flux_i[0], flux_i[1], flux_i[2], flux_i[3], flux_i[4], flux_i[5], flux_i[6],
                    intpeak_i[0], intpeak_i[1], intpeak_i[2], intpeak_i[3], intpeak_i[4], intpeak_i[5], intpeak_i[6],
                    calid, caltype]
                )


        table.write(outname, overwrite=True)



def get_strip_stats(database):

    table = Table.read(database)

    strips = [
        "g235m86.0",
        "g235m72.0",
        "g235m63.1",
        "g235m55.0",
        "g235m47.5",
        "g235m40.4",
        "g235m33.5",
        "g235m26.7",
        "g235m19.9",
        "g235m13.0",
        "g235m5.9",
        "g235p1.6",
        "g235p9.7",
        "g235p18.6"
    ][::-1]

    for strip in strips:

        idx = np.where(table["STRIP"] == strip)[0]
        table_j = table[idx]
        table_i = table_j[table_j["RMS"] < 1.]

        rms_scaled = table_i["RMS"]/table_i["FLUX_RATIO_MODEL"]

        median_rms = np.nanmedian(table_i["RMS"])
        rms16, rms84 = np.nanpercentile(table_i["RMS"], [16., 84.])
        rms16 = median_rms-rms16
        rms84 = rms84-median_rms

        median_rms_scaled = np.nanmedian(rms_scaled)
        rms16_scaled, rms84_scaled = np.nanpercentile(rms_scaled, [16., 84.])
        rms16_scaled = median_rms_scaled-rms16_scaled
        rms84_scaled = rms84_scaled-median_rms_scaled


        median_flux_ratio = np.nanmedian(table_i["FLUX_RATIO"])
        flux16, flux84 = np.nanpercentile(table_i["FLUX_RATIO"], [16., 84.])

        median_int_peak = np.nanmedian(table_i["INT_PEAK"])
        ip16, ip84 = np.nanpercentile(table_i["INT_PEAK"], [16., 84.])

        n_B = len(np.where(table_i["SOLUTIONS_TYPE"] == "B")[0])
        n_S = len(np.where(table_i["SOLUTIONS_TYPE"] == "S")[0])
        n_N = len(np.where(table_i["SOLUTIONS_TYPE"] == "N")[0])

        n_nan = len(np.where(np.isnan(table_i["RMS"]))[0])

        n_snapshots = len(table_j)

        logger.info(f"{strip}")
        logger.info("Calibrators: {:.0f}\\% / {:.0f}\\% / {:.0f}\\% ".format(
            100.*n_B/len(table_i),
            100.*n_N/len(table_i),
            100.*n_S/len(table_i)
        ))
        logger.info("RMS: ${:.0f}_{{-{:.0f}}}^{{+{:.0f}}}$".format(
            median_rms*1000., rms16*1000., rms84*1000.
        ))
        logger.info("RMS scaled: ${:.0f}_{{-{:.0f}}}^{{+{:.0f}}}$".format(
            median_rms_scaled*1000., rms16_scaled*1000., rms84_scaled*1000.
        ))
        # logger.info("Flux ratio: ${:.2f}_{{-{:.2f}}}^{{+{:.2f}}}$".format(
        #     median_flux_ratio, flux16, flux84
        # ))
        # logger.info("Int/peak: ${:.1f}_{{-{:.1f}}}^{{+{:.1f}}}$".format(
        #     median_int_peak, ip16, ip84
        # ))

        

        logger.info("Good %: {:.0f}\\%".format(
            100.*len(table_i)/n_snapshots
        ))

    table_j = table
    table_i = table_j[table_j["RMS"] < 1.]

    rms_scaled = table_i["RMS"]/table_i["FLUX_RATIO_MODEL"]

    median_rms = np.nanmedian(table_i["RMS"])
    rms16, rms84 = np.nanpercentile(table_i["RMS"], [16., 84.])
    rms16 = median_rms-rms16
    rms84 = rms84-median_rms

    median_rms_scaled = np.nanmedian(rms_scaled)
    rms16_scaled, rms84_scaled = np.nanpercentile(rms_scaled, [16., 84.])
    rms16_scaled = median_rms_scaled-rms16_scaled
    rms84_scaled = rms84_scaled-median_rms_scaled

    median_flux_ratio = np.nanmedian(table_i["FLUX_RATIO"])
    flux16, flux84 = np.nanpercentile(table_i["FLUX_RATIO"], [16., 84.])

    median_int_peak = np.nanmedian(table_i["INT_PEAK"])
    ip16, ip84 = np.nanpercentile(table_i["INT_PEAK"], [16., 84.])

    n_B = len(np.where(table_i["SOLUTIONS_TYPE"] == "B")[0])
    n_S = len(np.where(table_i["SOLUTIONS_TYPE"] == "S")[0])
    n_N = len(np.where(table_i["SOLUTIONS_TYPE"] == "N")[0])

    n_nan = len(np.where(np.isnan(table_i["RMS"]))[0])

    n_snapshots = len(table_j)

    logger.info(f"All")
    logger.info("Calibrators: {:.0f}\\% / {:.0f}\\% / {:.0f}\\% ".format(
        100.*n_B/len(table_i),
        100.*n_N/len(table_i),
        100.*n_S/len(table_i)
    ))
    logger.info("RMS: ${:.0f}_{{-{:.0f}}}^{{+{:.0f}}}$".format(
        median_rms*1000., rms16*1000., rms84*1000.
    ))
    logger.info("RMS scaled: ${:.0f}_{{-{:.0f}}}^{{+{:.0f}}}$".format(
        median_rms_scaled*1000., rms16_scaled*1000., rms84_scaled*1000.
    ))
    # logger.info("Flux ratio: ${:.2f}_{{-{:.2f}}}^{{+{:.2f}}}$".format(
    #     median_flux_ratio, flux16, flux84
    # ))
    # logger.info("Int/peak: ${:.1f}_{{-{:.1f}}}^{{+{:.1f}}}$".format(
    #     median_int_peak, ip16, ip84
    # ))

    

    logger.info("Good %: {:.0f}\\%".format(
        100.*len(table_i)/n_snapshots
    ))


# def trueS(S0, q, r):
#    return S0 * (0.5 + 0.5*np.sqrt(1 - ((4*q + 4) / (r**2))))

def edd_bias_(flux, q, snr):
    """Correct flux density for Eddington bias."""

    term1 = np.sqrt(
        (4*q + 4) / snr**2
    )
    return flux/2 * (1 + term1)

def edd_bias(flux, q, snr):
   return flux * (0.5 + 0.5*np.sqrt(1 - ((4*q + 4) / (snr**2))))


def catalogue_stats(catalogue="GLEAM300_sources.fits",
    flux_key="int_flux",
    peak_flux_key="peak_flux",
    ra_key="ra",
    dec_key="dec",
    psf_keys=["BMAJ", "BMIN", "BPA"],
    lat_key="Gal_lat",
    local_rms_key="local_rms",
    gal_lat_limit=10.):
    """
    """

    table = Table.read(catalogue)

    if lat_key not in table.columns:
        coords = SkyCoord(ra=table[ra_key], dec=table[dec_key], unit=(u.deg, u.deg))
        table[lat_key] = coords.galactic.b.value
        table[lat_key.replace("lat", "")+"lon"] = coords.galactic.l.value


    exgal_idx = np.where(np.abs(table[lat_key]) > gal_lat_limit)[0]
    table2 = table[exgal_idx]

    
    noise_median = np.nanmedian(table[local_rms_key])*1000.
    noise_params = np.nanpercentile(table[local_rms_key], [16., 84.])*1000.

    noise_median2 = np.nanmedian(table2[local_rms_key])*1000.
    noise_params2 = np.nanpercentile(table2[local_rms_key], [16., 84.])*1000.

    logger.info("RMS noise: {:.0f}_{{-{:.0f}}}^{{+{:.0f}}} mJy/beam".format(
        noise_median, noise_median-noise_params[0], noise_params[1]-noise_median
    ))

    logger.info("RMS noise (EG): {:.0f}_{{-{:.0f}}}^{{+{:.0f}}} mJy/beam".format(
        noise_median2, noise_median2-noise_params2[0], noise_params2[1]-noise_median2
    ))

    logger.info("N sources: {}".format(len(table)))
    logger.info("N sources (EG): {}".format(len(table2)))

    logger.info("Median PSF: {:.1f}\" * {:.1f}\"".format(
        np.median(table[psf_keys[0]])*3600., np.median(table[psf_keys[1]])*3600.
    ))    

    logger.info("Max PSF: {:.1f}\" * {:.1f}\"".format(
        np.max(table[psf_keys[0]])*3600., np.max(table[psf_keys[1]])*3600.
    ))    
    logger.info("Min PSF: {:.1f}\" * {:.1f}\"".format(
        np.min(table[psf_keys[0]])*3600., np.min(table[psf_keys[1]])*3600.
    ))    


def add_flux_uncertainty(catalogue, 
    base_unc=0.1,
    flux_scale_key="flux_scale_err",
    dec_key="dec"):

    table = Table.read(catalogue)

    table["flux_scale_err"] = np.full((len(table),), 0.)

    for i in range(len(table)):

        if table[dec_key][i] < -72.0:
            table[flux_scale_key][i] = 0.16
        elif table[dec_key][i] < 18.5:
            table[flux_scale_key][i] = 0.13
        else:
            table[flux_scale_key][i] = 0.16

    table.write(catalogue, overwrite=True)



def snr_func1(x, a, b, c):
    return c + a*x**b

def snr_func2(x, a, b, c, d):
    return c + a*x**b

def snr_func(x, a, b, c):
    return a + c*np.log10(x)**2 + b*np.log10(x)

# def snr_func(x, a, b):
#     y = np.array([
#         b + a*np.log(x_) if x_ < 200 else 1 for x_ in x
#     ])
#     y = np.piecewise(x, 
#             condlist=[x>200, x<=200],
#             funclist=[1., lnfunc])
#     return y

def snr_func_(x, a, b, c):
    return c  / (1 + (a*np.exp(-x*b)))


def snr_func_err(x, a, b, c, a_err, b_err, c_err):
    dda = 1
    ddb = np.log10(x)
    ddc = np.log10(x)**2

    y_err = np.sqrt(
        (ddc**2)*(c_err)**2 + \
        (ddb**2)*(b_err)**2 + \
        (dda**2)*(a_err)**2 
    )

    return y_err


def snr_func3(x, a, b, A, B):
    return snr_func3(x, a, b, 1)*snr_func1(x, A, B)

def snr_func_err_(x, a, b, c, a_err, b_err, c_err):
    ddc =  1  / (1 + (a*np.exp(-x*b)))
    ddb =  (a*c*x*np.exp(x*b))  / (a + np.exp(x*b))**2
    dda = -(c*np.exp(x*b))  / (a + np.exp(x*b))**2

    y_err = np.sqrt(
        (ddc**2)*(c_err)**2 + \
        (ddb**2)*(b_err)**2 + \
        (dda**2)*(a_err)**2 
    )

    return y_err


def bootstrap_snr_ratio(snr, ratio, stds, nboots=100):
    """
    """

    snr = np.asarray(snr)
    ratio = np.asarray(ratio)

    cond1 = np.isfinite(snr)
    cond2 = np.isfinite(ratio)

    idx = np.where(cond1 & cond2)[0]
    # snr = np.log10(snr[idx])
    # ratio = np.log10(ratio[idx])
    print(snr)
    print(ratio)

    # params = [1., 1., 1., 1., 1., 1.]
    params = [1.]*3
    popt, perr = fit(
        f=snr_func,
        x=snr,
        y=ratio,
        p0=params
    )

    print(popt)

    # residuals = np.log10(snr_func(snr, *popt)) - np.log10(ratio)
    residuals = snr_func(snr, *popt)/ratio
    # residuals = snr_func(snr, *popt)-ratio
    # print(residuals)
    sigma_res = np.std(np.log10(residuals))
    # sigma_res = np.std(residuals)
    sigma_mean = np.mean(np.log10(snr_func(snr, *popt)))
    # sigma_mean = np.mean(snr_func(snr, *popt))
    sigma_err_total = np.sqrt(sigma_res**2)
    # sigma_err_total = np.std(np.log10(ratio))
    # print(10**sigma_res,10**sigma_mean)
    ps = []
    pbar = trange(nboots)
    new_ys = []
    for i in pbar:
        # randomdelta = np.asarray(
            # [np.random.normal(0., sigma_err_total[j], 1)[0] for j in range(len(snr))]
        # )
        randomdelta = np.random.normal(0., sigma_err_total, len(snr))
        randomy = np.log10(ratio)+randomdelta
        # randomy = ratio+randomdelta
        print(randomy)
        ropt, rerr = fit(
            f=snr_func,
            x=snr,
            y=10**randomy,
            # y=randomy,
            p0=params
        )
        ps.append(ropt)
        new_ys.append(10**randomy)

        pbar.set_description("{}".format(ropt))


    ps = np.array(ps)
    
    logger.debug(ps)

    mean_popt = np.median(ps, 0)
    mean_perr = np.std(ps, 0)

    fresults = "Final results: "

    popt_f = []
    perr_f = []
    for i in range(len(mean_popt)):
        fresults += "{} ".format(
            ufloat(mean_popt[i], mean_perr[i])
        ).replace("+/-", "\\pm")
        if "e" in fresults:
            popt_f.append(mean_popt[i])
            perr_f.append(mean_perr[i])
        else:
            bits = "{}".format(ufloat(mean_popt[i], mean_perr[i])).split("+/-")
            bits = [b.replace("(", "").replace(")", "") for b in bits]
            popt_f.append(float(bits[0]))
            perr_f.append(float(bits[1]))
            
    # popt_f[2] = np.nanmean(ratio[-4:-1])

    logger.info(fresults)
    logger.info(", ".join([str(s) for s in popt_f]))
    logger.info(", ".join([str(s) for s in perr_f]))

    logger.info("Best coeffs:   {}".format(popt_f))
    logger.info("Bootstrap best coeffs:   {}".format(mean_popt))
    logger.info("Bootstrap err on coeffs: {}".format(mean_perr))

    return popt_f, perr_f, new_ys, 10**snr


def fit_snr_ratio(snr, ratio):
    """
    """
    snr = np.asarray(snr)
    ratio = np.asarray(ratio)

    params = [1., 1., 1.]
    popt, perr = fit(
        f=snr_func,
        x=snr,
        y=ratio,
        p0=params
    )


    logger.info(popt)

    
    # mean = np.nanmean(np.log10(ratio))
    # std = np.nanstd(np.log10(ratio))
    # print(np.nanstd(ratio))
    # x_range = np.logspace(np.log10(np.nanmin(snr)), np.log10(np.nanmax(snr)), len(snr))
    # gauss_noise = np.random.normal(mean, std, len(x_range))
    # print(gauss_noise)
    # y_range = np.log10(snr_func(x_range, *popt))
    # print(mean,std)
    # print(10**(y_range+gauss_noise))
    # print(x_range)  
    # _, perr = fit(
    #     f=snr_func,
    #     x=x_range,
    #     y=10**(y_range+gauss_noise),
    #     p0=params
    # )

    logger.info("{:.3f}, {:.3f}".format(round(popt[0], 3), round(popt[1], 3)))
    # popt = [round(p, 3) for p in popt]

    # logger.info("Mean y:        {}".format(10**mean))
    # logger.info("STDDEV y:      {}".format(10**std))
    logger.info("Best coeffs:   {}".format(popt))
    logger.info("Err on coeffs: {}".format(perr))

    return popt, perr


def apply_snr_func(catalogue, params,
    params_err=None,
    key_name="snr_fac",
    flux_keys=["int_flux", "peak_flux"],
    peak_flux_key="peak_flux",
    rms_key="local_rms"):
    """"""
    
    table = Table.read(catalogue)
    snr = table[peak_flux_key] / table[rms_key]
    
    snr_fac = snr_func(snr, *params)
    table[key_name] = snr_fac

    for key in flux_keys:
        table[key+"_corr"] = table[key] / snr_fac
        idx = np.where(table["err_"+key] < 0)[0]
        table["err_"+key][idx] = 0.

    if params_err is not None:
        snr_fac_err = snr_func_err(
            snr, 
            a=params[0], 
            b=params[1], 
            c=params[2],
            a_err=params_err[0],
            b_err=params_err[1],
            c_err=params_err[2]
        )
        table["err_"+key_name] = snr_fac_err

        for key in flux_keys:
            table["err_"+key+"_corr"] = np.sqrt(
                table["err_"+key]**2 + \
                (table[key+"_corr"]*(snr_fac_err/snr_fac))**2
            )

    table.write(catalogue, overwrite=True)
