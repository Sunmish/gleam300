#! /usr/bin/env python

import os
import numpy as np
from glob import glob
import warnings

import sqlite3

import shutil

from astropy.io import fits
from astropy.coordinates import SkyCoord, get_body, EarthLocation
from astropy import units as u
from astropy.table import Table, vstack, Column
from astropy.io.votable import from_table, writeto
from astropy.io.votable.tree import VOTableFile, Resource, Field, Param
from astropy.io.votable.tree import Table as vTable
from astropy.wcs import WCS, FITSFixedWarning
from astropy.stats import sigma_clip
from astropy.time import Time

from uncertainties import ufloat

from tqdm import trange


from astropy.io.fits.verify import VerifyWarning
warnings.simplefilter("ignore", category=VerifyWarning)
warnings.simplefilter("ignore", category=FITSFixedWarning)

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

MWA = EarthLocation.from_geodetic(
    lat=-26.703319*u.deg, 
    lon=116.67081*u.deg, 
    height=377*u.m
)

def format_single_value_to_n_sigfig(value, err, n=2):

    if err < 0:
        logger.warning("negative error: {} {}".format(value, err))
        err = np.nan
    uf = ufloat(value, err)
    sf = "{:1.{n}u}".format(uf, n=n)
    sf_bits = sf.split("+/-")
    v, e = float(sf_bits[0]), float(sf_bits[1])
    return v, e, sf

def format_to_n_sigfig(values, errs, n=2):

    values_n = np.full_like(values, np.nan)
    errs_n = np.full_like(errs, np.nan)

    pbar = trange(len(values))
    for i in pbar:

        v, e, sf = format_single_value_to_n_sigfig(values[i], errs[i], n)
        pbar.set_description("{} {} {} {} {}".format(values[i], errs[i], v, e, sf))
        values_n[i] = v
        errs_n[i] = e

    return values_n, errs_n


def get_polygon_for_tile(image, color="green", inc=100):

    with fits.open(image) as f:
        
        w = WCS(f[0].header).celestial

        ra = []
        dec = []

        polygon_string = ""
        
        r, d = w.all_pix2world(
            range(0, f[0].data.shape[-1]),
            np.full((f[0].data.shape[-1],), 0), 0
        )

        

        ra = r[::-1]
        dec = d[::-1]

        r, d = w.all_pix2world(
            np.full((f[0].data.shape[-2],), 0),
            range(0, f[0].data.shape[-2]), 0
        )

        
        

        ra = [*ra, *r]
        dec = [*dec, *d]


        
        r, d = w.all_pix2world(
            range(0, f[0].data.shape[-1]),
            np.full((f[0].data.shape[-1],), f[0].data.shape[-2]), 0
        )
        

        ra = [*ra, *r]
        dec = [*dec, *d]

        


        r, d = w.all_pix2world(
            np.full((f[0].data.shape[-2],), f[0].data.shape[-1]),
            range(0, f[0].data.shape[-2])[::-1], 0
        )

        ra = [*ra, *r][0::inc]
        dec = [*dec, *d][0::inc]
        # r, d = w.all_pix2world(
        #     range(0, f[0].data.shape[0]),
        #     np.full((f[0].data.shape[0],), f[0].data.shape[1]), 0
        # )

        # for i in range(len(r)):
        #     polygon_string = "{} {:.4f} {:.4f}".format(
        #         polygon_string, d[i], r[i]
        #     )
        
        # r, d = w.all_pix2world(
        #     np.full((f[0].data.shape[1],), 0),
        #     range(0, f[0].data.shape[1]), 0
        # )

        # for i in range(len(r)):
        #     polygon_string = "{} {:.4f} {:.4f}".format(
        #         polygon_string, d[i], r[i]
        #     )
        
        # r, d = w.all_pix2world(
        #     np.full((f[0].data.shape[1],), f[0].data.shape[0]),
        #     range(0, f[0].data.shape[1]), 0
        # )

        # for i in range(len(r)):
        #     polygon_string = "{} {:.4f} {:.4f}".format(
        #         polygon_string, d[i], r[i]
        #     )
    
    for i in range(len(ra)):
        polygon_string = "{},{:.4f},{:.4f}".format(polygon_string, ra[i], dec[i])
    polygon_string = polygon_string[1:]
    polygon_string = "polygon({}) # color={} fill=1".format(polygon_string, color)
    
    return polygon_string


def get_catalogue_for_tile(image, return_table=False, inc=10, tile_number=0):
    """
    """

    with fits.open(image) as f:

        w = WCS(f[0].header).celestial
        x, y = np.indices(f[0].data.shape)
        x = x.flatten()[0::inc]
        y = y.flatten()[0::inc]
        r, d = w.all_pix2world(y, x, 0)
        v = np.full((len(r),), 1)
        tile = np.full((len(r),), tile_number)
        t = Table()
        t["ra"] = r
        t["dec"] = d
        t["value"] = v
        t["tile"] = tile

 

    if return_table:
        return t   
    else:    
        t.write(image.replace(".fits", "") + "_positions.fits", overwrite=True)


def big_tile_catalogues(outname, pattern="GLEAM300_J???????.fits", inc=10):

    images = glob(pattern)

    table = get_catalogue_for_tile(images[0], return_table=True, inc=inc, tile_number=0)

    pbar = trange(1, len(images))
    for i in pbar:
        pbar.set_description(f"{images[i]}")
        t = get_catalogue_for_tile(images[i], return_table=True, inc=inc, tile_number=i)
        table = vstack([table, t])

    table.write(outname, overwrite=True)


def big_tile_regions(outname=None, pattern="GLEAM300_J???????.fits", inc=10, 
    color="green"):

    images = glob(pattern)

    
    pbar = trange(len(images))
    if outname is None:
        for i in pbar:
            with open(images[i].replace(".fits", "") + "_positions.reg", "w+") as f:
                f.write("# Region file format: DS9 version 4.1\n")
                f.write("fk5\n")
                pbar.set_description(f"{images[i]}")
                t = get_polygon_for_tile(images[i], color=color, inc=inc)
                f.write(t)
    else:
        with open(outname, "w+") as f:
            f.write("# Region file format: DS9 version 4.1\n")
            f.write("fk5\n")
            for i in pbar:
                pbar.set_description(f"{images[i]}")
                t = get_polygon_for_tile(images[i], color=color, inc=inc)
                f.write(t)


def scale_flux(flux, freq, alpha, out_freq):
    """Scale flux assuming power law."""

    return flux*(out_freq/freq)**alpha


def clip_catalogue_snr(catalogue, 
    peak_flux_key="peak_flux", 
    local_rms_key="local_rms",
    min_snr=5.):
    """Clip catalogue based on minimum SNR."""

    table = Table.read(catalogue)

    snr = abs(table[peak_flux_key]/table[local_rms_key])
    idx = np.where(snr >= min_snr)[0]

    n_faint = len(table) - len(idx)

    logger.info("{}: {} sources".format(catalogue, len(table)))
    logger.info("{}: {} sources < {} sigma".format(catalogue, n_faint, min_snr))
    logger.info("{}: {} source >= {} sigma".format(catalogue, len(idx), min_snr))

    table = table[idx]

    table.write(catalogue, overwrite=True)


def clip_catalogue_flags(catalogue,
    flag=0,
    flag_key="flag",
    ):

    table = Table.read(catalogue)
    idx = np.where(table[flag_key] == flag)[0]
    logger.info("{}/{} source clipped for flag != {}".format(len(idx), len(table), flag))
    table = table[idx]
    table.write(catalogue, overwrite=True)

def clip_catalogue_gp(catalogue, outname, b=5., b_key="Gal_lat"):

    table = Table.read(catalogue)

    logger.info("{}: {} sources".format(catalogue, len(table)))

    idx = np.where(abs(table[b_key]) > b)[0]
    table = table[idx]

    logger.info("{}: {} source outside of |b|>{}".format(
        catalogue, len(table), b)
    )

    table.write(outname, overwrite=True)


def clip_obsids(images,
    output_list,
    table_stub="_deep-MFS-image-pb_comp.vot",
    max_rms=500e-3, 
    max_flux=1000.,
    min_flux=-50.,
    check_for_moon=True,
    check_for_solar_system_body=False,
    flux_key="int_flux",
    peak_flux_key="peak_flux", 
    ratio_range=(0.5, 2.0)
    ):

    output_images = []
    with_moon = []
    with_solar_system_body = []
    with_solar_system_body_type = []
    high_rms = []
    high_flux = []
    low_flux = []
    bad_ratio = []

    pbar = trange(len(images))
    for i in pbar:
        image = images[i]
    
        bits = os.path.basename(image).split("_")
        obsid = bits[0]
        strip = bits[1]
        
        if not os.path.exists(image):
            pbar.set_description("Missing {}".format(image))
            continue
        else:
            pbar.set_description("{}".format(image))

        with fits.open(image) as f:

            try:
                rms = np.nanstd(sigma_clip(f[0].data[
                    int(0.2*f[0].data.shape[0]):int(0.8*f[0].data.shape[0]),
                    int(0.2*f[0].data.shape[1]):int(0.8*f[0].data.shape[1])],
                    sigma=3, maxiters=5)
                )
            except ValueError:
                high_rms.append(image)
                continue

            

            if rms > max_rms:
                high_rms.append(image)
                continue
            
            try:
                max_val = np.nanmax(np.squeeze(f[0].data))
            except ValueError:
                high_flux.append(image)
                continue
            try:
                min_val = np.nanmin(np.squeeze(f[0].data))
            except ValueError:
                low_flux.append(image)
                continue
            
            if max_val > max_flux:
                high_flux.append(image)
                continue
            if min_val < min_flux:
                low_flux.append(image)
                continue

            if check_for_moon:

                x_indices = np.indices(f[0].data.shape)[-1]
                y_indices = np.indices(f[0].data.shape)[-2]

                time = Time(int(image[0:10])+60, format="gps")
                moon_coords = get_body("moon", time=time, location=MWA)
                wcs = WCS(f[0].header).celestial
                # logger.debug("{} {}".format(moon_coords.ra.value, moon_coords.dec.value))
                y, x = wcs.all_world2pix(moon_coords.ra.value, moon_coords.dec.value, 0)
                if np.isfinite(x) and np.isfinite(y):
                    if int(x) in x_indices and \
                        int(y) in y_indices:
                        
                        with_moon.append(image)
                        continue

                if check_for_solar_system_body:
                    for body in ["sun", "mercury", "venus", "mars", "jupiter", "saturn", "uranus", "neptune"]:
                        body_coords = get_body(body, time=time, location=MWA)
                        logger.debug("{}: {} {}".format(body, body_coords.ra.value, body_coords.dec.value))
                        wcs = WCS(f[0].header).celestial
                        y, x = wcs.all_world2pix(body_coords.ra.value, body_coords.dec.value, 0)
                        if np.isfinite(x) and np.isfinite(y):
                            if int(x) in x_indices and \
                                int(y) in y_indices:
                                
                                with_solar_system_body.append(image)
                                with_solar_system_body_type.append(body)
                                continue    


            try:
                table = Table.read("{}_{}{}".format(obsid, strip, table_stub))
                
                med_ratio = np.nanmedian(
                    table[flux_key]/table[peak_flux_key]
                )
                if med_ratio > ratio_range[1] or med_ratio < ratio_range[0]:
                    bad_ratio.append(image)
                    continue
            except Exception:
                pass

            output_images.append(image)


    logger.debug("Skipping high rms images: {}".format(high_rms))
    logger.debug("Skipping high flux images: {}".format(high_flux))
    logger.debug("Skipping low flux images: {}".format(low_flux))
    logger.debug("Skipping bad ratio images: {}".format(bad_ratio))
    logger.debug("Skipping images with the Moon: {}".format(with_moon))

    
    with open(output_list+".txt", "w+") as f:
        for i in range(len(output_images)):
            f.write(f"{output_images[i]}\n")
    with open(output_list+"_rms.txt", "w+") as f:
        for i in range(len(high_rms)):
            f.write(f"{high_rms[i]}\n")
    with open(output_list+"_flux.txt", "w+") as f:
        for i in range(len(high_flux)):
            f.write(f"{high_flux[i]}\n")
    with open(output_list+"_low_flux.txt", "w+") as f:
        for i in range(len(low_flux)):
            f.write(f"{low_flux[i]}\n")
    with open(output_list+"_ratio.txt", "w+") as f:
        for i in range(len(bad_ratio)):
            f.write(f"{bad_ratio[i]}\n")
    with open(output_list+"_moon.txt", "w+") as f:
        for i in range(len(with_moon)):
            f.write(f"{with_moon[i]}\n")


    for body in ["sun", "mercury", "venus", "mars", "jupiter", "saturn", "uranus", "neptune"]:
        idx = np.where(np.asarray(with_solar_system_body_type) == body)[0]
        logger.debug("Skipping images with {}: {}".format(body, len(idx)))
        with open(output_list+"_{}.txt".format(body), "w+") as f:
            for i in idx:
                f.write(f"{with_solar_system_body[i]}\n")




def apply_correction_factor(image, factor_table, outname=None):

    hdu = fits.open(image)
    table = Table.read(factor_table)
    obsid = int(image[0:10])
    idx = np.argmin(np.abs(table["OBSID"] - obsid))
    logger.debug("Applying {} to {}".format(table["model"][idx], image))
    hdu[0].data /= table["model"][idx]
    hdu[0].header["FSCALE"] = table["model"][idx]
    if outname is None:
        outname = image

    hdu.writeto(outname, overwrite=True)



def tile_with_ref(ra_sep, dec_sep, factor, outbase,
    decs=None,
    ref_dec=0.,
    dec_range=(-80, 30),
    max_dec=None,
    scp_cap=False):
    """
    """

    cra, cdec = [], []
    rseps = []
    dseps = []
    tiles = []

    if decs is None:
        dec_abs_range = abs(-90-dec_range[1])
        nd = dec_abs_range//dec_sep
        dec_sep = dec_abs_range/nd
        decs = np.concatenate([
            np.arange(ref_dec, dec_range[0], -dec_sep), 
            np.arange(ref_dec+dec_sep, dec_range[1], dec_sep)
        ])
        print(decs)
    else:
        dec_abs_range = abs(min(decs)-max(decs))
        nd = dec_abs_range//dec_sep
        dec_sep =dec_abs_range/nd
        print(decs)
    # decs = np.arange(dec_range[0], dec_range[1], dec_sep)

    for dec in decs:

        if max_dec is not None:
            if dec > max_dec:
                continue

        rsep = ra_sep/np.cos(np.radians(dec))
        # rsep = ra_sep
        nr = 360.//rsep
        rsep = 360./nr
        ras = np.arange(0., 359, rsep)
        rfactor = factor*np.cos(np.radians(dec+dec_sep*0.5))
        for ra in ras:
            cra.append(ra)
            cdec.append(dec)
            rseps.append(rsep*rfactor)
            dseps.append(dec_sep*factor)

            tile_coord = SkyCoord(ra=ra*u.deg, dec=dec*u.deg)
            filename = "J{:0>4}{}".format(
                # tile_coord.ra.to_string(u.hour, sep="")[0:4], 
                # tile_coord.dec.to_string(sep="", alwayssign=True, pad=True)[0:3]
            tile_coord.ra.to_string(u.hour, sep="", fields=2), 
                tile_coord.dec.to_string(sep="", alwayssign=True, pad=True, fields=1)
            )

            tiles.append(filename)

    if scp_cap:
        cra.append(0.)
        cdec.append(-90.)
        rseps.append(dec_sep*3)
        dseps.append(dec_sep*3)
        tile_coord = SkyCoord(ra=0.*u.deg, dec=-90*u.deg)
        filename = "J{:0>4}{}".format(
            # tile_coord.ra.to_string(u.hour, sep="")[0:4], 
            # tile_coord.dec.to_string(sep="", alwayssign=True, pad=True)[0:3]
        tile_coord.ra.to_string(u.hour, sep="", fields=2), 
            tile_coord.dec.to_string(sep="", alwayssign=True, pad=True, fields=1)
        )
        tiles.append(filename)

    t = Table()
    t.add_column(tiles, name="TILE")
    t.add_column(cra, name="RA")
    t.add_column(cdec, name="DEC")
    t.add_column(rseps, name="RA_SIZE")
    t.add_column(dseps, name="DEC_SIZE")
    t.write(outbase+".fits", overwrite=True)

    with open(outbase+".csv", "w+") as f:
        f.write("RA,DEC,RA_SIZE,DEC_SIZE\n")
        for i in range(len(cra)):
            f.write("{},{},{},{},{}\n".format(tiles[i],cra[i], cdec[i], rseps[i], dseps[i]))

    with open(outbase+".txt", "w+") as f:
        for i in range(len(cra)):
            f.write("{} {} {} {} {}\n".format(tiles[i],cra[i], cdec[i], rseps[i], dseps[i]))


def get_tile(sky_tiling, tile):
    table = Table.read(sky_tiling)
    idx = np.where(table["TILE"] == tile)[0]
    tile_ra = np.asarray(table["RA"][idx])[0]
    tile_dec = np.asarray(table["DEC"][idx])[0]
    size_ra = np.asarray(table["RA_SIZE"][idx])[0]
    size_dec = np.asarray(table["DEC_SIZE"][idx])[0]
    return tile_ra, tile_dec, size_ra, size_dec


def find_obs(tile_coord, tile_size, database,
    sep=10.,
    stub="_deep-MFS-image-pb_warp.fits",
    indir="./",
    sep_inner=None,
    outbase="tile",
    include_scp=False):

    obsids = Table.read(database)
    # table = Table.read(database)
    coords = SkyCoord(
        ra=obsids["IMAGE_RA"]*u.deg,
        dec=obsids["IMAGE_DEC"]*u.deg
    )
    tile_coord = SkyCoord(
        ra=tile_coord[0]*u.deg,
        dec=tile_coord[1]*u.deg
    )

    obsids_tile = []
    images_tile = []
    ra, dec = [], []

    boundaries = [
        (tile_coord.ra.value, tile_coord.dec.value),
        (tile_coord.ra.value-tile_size[0], tile_coord.dec.value),
        (tile_coord.ra.value-tile_size[0], tile_coord.dec.value+tile_size[1]),
        (tile_coord.ra.value-tile_size[0], tile_coord.dec.value-tile_size[1]),
        (tile_coord.ra.value+tile_size[0], tile_coord.dec.value),
        (tile_coord.ra.value+tile_size[0], tile_coord.dec.value+tile_size[1]),
        (tile_coord.ra.value+tile_size[0], tile_coord.dec.value-tile_size[1]),
        (tile_coord.ra.value, tile_coord.dec.value+tile_size[1]),
        (tile_coord.ra.value, tile_coord.dec.value-tile_size[1]),
    ]

    for b, boundary in enumerate(boundaries):
        logger.debug(boundary)
        try:
            boundary_coords = SkyCoord(
                ra=boundary[0]*u.deg,
                dec=boundary[1]*u.deg
            )
        except ValueError:
            if boundary[1] < 0:
                boundaries[b] = (boundary[0], -90)
            else:
                boundaries[b] = (boundary[0], 90)
            boundary_coords = SkyCoord(
                ra=boundary[0]*u.deg,
                dec=boundaries[b][1]*u.deg
            )
        seps = boundary_coords.separation(coords)
        idx = np.where(seps.value < sep)[0]
        for i in idx:
            obsid = str(int(obsids["OBSID"][i]))
            if obsid not in obsids_tile:
                if obsids["STRIP"][i] == "g235m86.0" and not include_scp:
                    continue
                if obsids["IMAGE_SELECT"][i] == 0:
                    obsids_tile.append(obsid)
                    # image = "{}/{}{}".format(indir, obsids["OBSID"][i], stub)
                    # image = "{}/{}".format(indir, obsids["IMAGE_IMAGE"][i])
                    image = "{}/{}_{}{}".format(
                        indir, obsids["OBSID"][i], obsids["STRIP"][i], stub
                    )
                    images_tile.append(image)
                    ra.append(obsids["IMAGE_RA"][i])
                    dec.append(obsids["IMAGE_DEC"][i])

    if sep_inner is not None:
        boundary_coords = SkyCoord(
            ra=boundaries[0][0]*u.deg,
            dec=boundaries[0][1]*u.deg
        )
        seps = boundary_coords.separation(coords)
        idx = np.where(seps.value < sep_inner)[0]
        for i in idx:
            obsid = str(int(obsids["OBSID"][i]))
            if obsid not in obsids_tile:
                if obsids["STRIP"][i] == "g235m86.0" and not include_scp:
                    continue
                if obsids["IMAGE_SELECT"][i] == 0:
                    obsids_tile.append(obsid)
                    # image = "{}/{}".format(indir, obsids["IMAGE"][i])
                    image = "{}/{}_{}{}".format(
                        indir, obsids["OBSID"][i], obsids["STRIP"][i], stub
                    )
                    images_tile.append(image)
                    ra.append(obsids["IMAGE_RA"][i])
                    dec.append(obsids["IMAGE_DEC"][i])

    
    t = Table()
    t.add_column(obsids_tile, name="OBSID")
    t.add_column(images_tile, name="IMAGE")
    t.add_column(ra, name="IMAGE_RA")
    t.add_column(dec, name="IMAGE_DEC")


    filename = "J{:0>4}{}".format(
        # tile_coord.ra.to_string(u.hour, sep="")[0:4], 
        # tile_coord.dec.to_string(sep="", alwayssign=True, pad=True)[0:3]
    tile_coord.ra.to_string(u.hour, sep="", fields=2), 
        tile_coord.dec.to_string(sep="", alwayssign=True, pad=True, fields=1)
    )

    t2 = Table(
        [[b[0] for b in boundaries], [b[1] for b in boundaries]],
        names=["RA", "DEC"]
    )
    t2.write(f"{outbase}_{filename}_boundaries.fits", overwrite=True)
    
    t.write("{}_{}_list.fits".format(outbase, filename
    ), overwrite=True)

    filename = "{}_{}_list.txt".format(outbase, filename
    )
    with open(filename, "w+") as f:
        for i in range(len(images_tile)):
            f.write("{}\n".format(images_tile[i]))
    
    with open(filename.replace(".txt", ".reg"), "w+") as f:
        for i in range(len(images_tile)):
            f.write("fk5;circle({} {} {}) # color=green ".format(
                ra[i], 
                dec[i], 
                sep/2) + r" text={" + "{}".format(obsids_tile[i]) + r"}" + "\n"
            )
    
    print("{} {} {} {} {}".format(
        filename, tile_coord.ra.value, tile_coord.dec.value, tile_size[0]*2, tile_size[1]*2)
    )



def get_image_stats(indir, stub="GLEAM300", outname="GLEAM300_image_stats.csv"):
    """
    """

    images = glob(f"{indir}/{stub}_J???????.fits")
    
    name = []
    ra = []
    dec = []
    rms = []
    bmaj = []
    bmin = []
    bpa = []     
    for image in images:
        with fits.open(image) as f:

            hdr = f[0].header
            data = np.squeeze(f[0].data)

            name_i = os.path.basename(image.replace(".fits", ""))
            name.append(name_i)

            ra.append(hdr["CRVAL1"])
            dec.append(hdr["CRVAL2"])
            bmaj.append(hdr["BMAJ"]*3600.)
            bmin.append(hdr["BMIN"]*3600.)
            bpa.append(hdr["BPA"])

            rms_i = np.nanstd(sigma_clip(data[
                int(0.2*data.shape[0]):int(0.8*data.shape[0]),
                int(0.2*data.shape[1]):int(0.8*data.shape[1])],
                sigma=3, maxiters=5)
            )*1000.

            rms.append(rms_i) 

            logger.info(
                "{}: {:.2f} mJy/beam".format(name_i, rms_i)
            )

    table = Table(
        [
            name, ra, dec, rms, bmaj, bmin, bpa
        ],
        names=["FIELD", "RA", "DEC", "RMS", "BMAJ", "BMIN", "BPA"]
    )

    table.write(outname, overwrite=True)


def merge_scp_calibrator_info(input_database,
    output_database,
    input_field="g235m26.7_235",
    output_field="g235m86.0_235"):
    """Merge dec -26.7 calibrator info to SCP dec strip."""

    con1 = sqlite3.connect(input_database, timeout=120.)
    cur1 = con1.cursor()
    cur1.execute(f"SELECT * FROM 'field{input_field}'")
    data_zenith = cur1.fetchall()
    data_zenith = Table(
        np.asarray(data_zenith), 
        names=["INDEX", "OBSID", "NOTE1", "STATUS", "JOBID", "CALIBRATOR", "CALID", "IMAGE_STATUS", "NOTE2"]
    )

    con2 = sqlite3.connect(output_database, timeout=120.)
    cur2 = con2.cursor()
    cur2.execute(f"SELECT * FROM 'field{output_field}'")
    data_scp = cur2.fetchall()

    obsids = []
    calids = []
    calnames = []

    for i in range(len(data_scp)):

        obsid = data_scp[i][1]
        idx = np.where(data_zenith["OBSID"] == obsid)[0]
        if len(idx) > 0:
            calid = int(data_zenith["CALID"][idx])
            calname = int(data_zenith["CALIBRATOR"][idx])
            calname = str(calname)
            # data_scp[i][5] = data_zenith["CALIBRATOR"][idx]
            # data_scp[i][6] = int(data_zenith["CALID"][idx])

            logger.info("Updating {} with {}/{}".format(obsid, calname, calid))

            obsids.append(obsid)
            calids.append(calid)
            calnames.append(calname)
            # cur2.execute(f"UPDATE 'field{output_field}' SET calname = ? WHERE snapshot = ?", (calname, obsid))
            # cur2.execute(f"UPDATE 'field{output_field}' SET calid = ? WHERE snapshot = ?", (calid, obsid))
        


    con1.close()  
    
    for i in range(len(obsids)):
        cur2.execute(f"UPDATE 'field{output_field}' SET calname = ? WHERE snapshot = ?", (calnames[i], obsids[i]))
        cur2.execute(f"UPDATE 'field{output_field}' SET calid = ? WHERE snapshot = ?", (calids[i], obsids[i]))
    
    con2.commit()
    con2.close() 



def add_night_number(tablename, obsid_key="OBSID", max_sep=24*3600.):

    table = Table.read(tablename)

    table = table[table.argsort(keys=["OBSID"])]
    
    night_id = np.full((len(table),), 0, dtype=int)
    
    strips = np.unique(table["STRIP"])

    for strip in strips:

        idx = np.where(table["STRIP"] == strip)[0]

        night_id_i = np.full((len(table[idx]),), 0, dtype=int)

        night = 0
        logger.debug("{}".format(strip))

        pbar = trange(1, len(table[idx]))
        for i in pbar:
            if (table[idx][i]["OBSID"]-table[idx][i-1]["OBSID"]) > max_sep:
                night += 1
                # logger.debug("{} ends night {}, {} starts night {}".format(
                #     table[idx][i-1]["OBSID"], night-1, 
                #     table[idx][i]["OBSID"], night
                # ))
            
            pbar.set_description("{}".format(night))
            night_id_i[i] = night

        night_id[idx] = night_id_i

    try:
        table.add_column(night_id, name="NIGHT")
    except ValueError:
        table["NIGHT"] = night_id
    
    table.write(tablename, overwrite=True)




def ignore_sources(coords, values, sources=["CenA", "LMC", "SMC"]):
    source_coords = {
        "CenA": SkyCoord(ra="13h25m28s", dec="-43d01m09s", unit=(u.hourangle, u.deg)),
        "LMC": SkyCoord(ra="05h23m35s", dec="-69d45m22s", unit=(u.hourangle, u.deg)),
        "SMC": SkyCoord(ra="00h52m38s", dec="-72d48m01s", unit=(u.hourangle, u.deg))
    }

    source_radius = {
        "CenA": 9.,
        "LMC": 5.5,
        "SMC": 2.5
    }

    for source in sources:

        seps = source_coords[source].separation(coords)
        idx = np.where(seps.value < source_radius)
        values[idx] = np.nan
        logger.debug("{} obsids near {}".format(len(idx), source))

    return values


    
def add_image_details_to_database(database, 
    image_selection,
    image_avoid_moon,
    image_avoid_ratio,
    image_avoid_rms,
    image_details,
    outname=None):

    table = Table.read(database)

    keys = ["IMAGE_RA", "IMAGE_DEC", "BMAJ", "BMIN", "BPA"]
    for key in keys:
        if key not in table.colnames:
            table.add_column(col=np.full((len(table),), np.nan, dtype=float), name=key)
    
    if "IMAGE_SELECT" not in table.colnames:
        table.add_column(col=np.full((len(table),), -1, dtype=int), name="IMAGE_SELECT")

    with open(image_selection) as f:
        lines = f.readlines()
        for line in lines:
            bits = line.split("_")
            obsid = int(bits[0])
            strip = bits[1]

            cond1 = table["OBSID"] == obsid
            cond2 = table["STRIP"] == strip
            idx = np.where(cond1 & cond2)[0]

            table["IMAGE_SELECT"][idx] = 0


    with open(image_avoid_moon) as f:
        lines = f.readlines()
        for line in lines:
            bits = line.split("_")
            obsid = int(bits[0])
            strip = bits[1]

            cond1 = table["OBSID"] == obsid
            cond2 = table["STRIP"] == strip
            idx = np.where(cond1 & cond2)[0]

            table["IMAGE_SELECT"][idx] = 1

    with open(image_avoid_ratio) as f:
        lines = f.readlines()
        for line in lines:
            bits = line.split("_")
            obsid = int(bits[0])
            strip = bits[1]

            cond1 = table["OBSID"] == obsid
            cond2 = table["STRIP"] == strip
            idx = np.where(cond1 & cond2)[0]

            table["IMAGE_SELECT"][idx] = 2

    with open(image_avoid_rms) as f:
        lines = f.readlines()
        for line in lines:
            bits = line.split("_")
            obsid = int(bits[0])
            strip = bits[1]

            cond1 = table["OBSID"] == obsid
            cond2 = table["STRIP"] == strip
            idx = np.where(cond1 & cond2)[0]

            table["IMAGE_SELECT"][idx] = 3
        
    detail_table = Table.read(image_details, format="ascii.csv")
    obsids = np.asarray(detail_table["OBSID"]).astype(int)

    for i in range(len(detail_table)):

        cond1 = table["OBSID"] == obsids[i]
        cond2 = table["STRIP"] == detail_table["STRIP"][i]

        idx = np.where(cond1 & cond2)[0]

        for key in ["IMAGE_RA", "IMAGE_DEC", "BMAJ", "BMIN", "BPA"]:
            table[key][idx] = detail_table[key][i]
    
    if outname is None:
        outname = database
    table.write(outname, overwrite=True)



def source_lm(
    tab: Table,
    ra0: float,
    dec0: float,
    ra_key: str = "ra",
    dec_key: str = "dec",
    source_l="Source_l",
    source_m="Source_m",
    pol_axis=-45.0,
    pa=45.0,
) -> Table:
    """l,m coordinates with reference to pointing centre (ra0,dec0)

    Args:
        tab (Table): Table containing ra and dec columns
        ra0 (float): Reference right ascension in degrees
        dec0 (float): Reference declination in degrees
        ra_key (str, optional): Right ascension column. Defaults to "ra".
        dec_key (str, optional): Declination column. Defaults to "dec".
        source_l (str, optional): Output offset `l` column. Defaults to "Source_l".
        source_m (str, optional): Output offset `m` column. Defaults to "Source_m".
        pol_axis (float, optional): Field rotation in degrees. Defaults to -45.0.
        pa (float, optional): Feed rotation in degrees. Defaults to 45.0. Will probably never change for ASKAP.

    Returns:
        Table: Updated table with `l` and `m` columns
    """
    centre_coord = SkyCoord(ra0, dec0, unit=(u.deg, u.deg))
    coords = SkyCoord(tab[ra_key], tab[dec_key], unit=(u.deg, u.deg))
    seps = centre_coord.separation(coords)
    pas = centre_coord.position_angle(coords)
    footprint_pa = pa * u.deg + pol_axis * u.deg
    offset_l = seps * np.sin(pas - footprint_pa)
    offset_m = seps * np.cos(pas - footprint_pa)
    tab[source_l] = offset_l.to(u.deg).value
    tab[source_m] = offset_m.to(u.deg).value
    return tab


def add_lm_coordinates(image_database, catalogues,
    ra_key="ra",
    dec_key="dec",
    l_key="Source_l",
    m_key="Source_m",
    dist_key="dist"):
    """
    """

    obsids = Table.read(image_database)

    pbar = trange(len(catalogues))
    for i in pbar:

        catalogue = catalogues[i]
        obsid = int(os.path.basename(catalogue)[0:10])
        strip = os.path.dirname(catalogue).replace("GLEAM", "")

        pbar.set_description("{}: {}".format(strip, obsid))

        cond1 = obsids["OBSID"] == obsid
        cond2 = obsids["STRIP"] == strip
        idx = np.where(cond1 & cond2)[0]

        table = Table.read(catalogue)
        table = source_lm(
            tab=table,
            ra0=obsids["RA"][idx],
            dec0=obsids["DEC"][idx],
            ra_key=ra_key,
            dec_key=dec_key,
            source_l=l_key,
            source_m=m_key,
            pol_axis=0.,
            pa=0.
        )

        coords = SkyCoord(
            ra=obsids["RA"][idx],
            dec=obsids["DEC"][idx],
            unit=(u.deg, u.deg)
        )

        coords2 = SkyCoord(
            ra=table[ra_key],
            dec=table[dec_key],
            unit=(u.deg, u.deg)
        )
        dist = coords.separation(coords2).value

        table[dist_key] = dist

        table.write(catalogue, overwrite=True)


def clip_source_list(
    catalogue : str,
    snr : float = 0,
    perc_err : float = 1.,
    int_flux_key : str = "int_flux",
    err_int_flux_key : str = "err_int_flux",
    peak_flux_key : str = "peak_flux",
    local_rms_key : str = "local_rms"
):
    """

    """

    table = Table.read(catalogue)
    cond1 = table[err_int_flux_key] / table[int_flux_key] < perc_err
    idx_1 = np.where(~cond1)[0]
    logger.info("{}: {} sources flux error > {}".format(catalogue, len(idx_1), perc_err))
    cond2 = abs(table[peak_flux_key] / table[local_rms_key]) > snr
    idx_2 = np.where(~cond2)[0]
    logger.info("{}: {} sources SNR < {}".format(catalogue, len(idx_2), snr))
    idx = np.where(cond1 & cond2)[0]
    table = table[idx]

    table.write(catalogue, overwrite=True)
    


def format_catalogue_for_publication(
    catalogue : str, outname : str, 
    round_to_n : int = 2,
    catalogue_name : str = "GLEAM300"
):
    """
    """

    table = Table.read(catalogue)

    source_names = []
    coords = SkyCoord(
        ra=table["ra"]*u.deg,
        dec=table["dec"]*u.deg
    )
    for i in range(len(table)):
        c = coords[i]
        source_name = "GLEAM-300 J{r}{d}".format(
            r=c.ra.to_string(unit=u.hourangle, sep="", precision=2,pad=True)[0:6],
            d=c.dec.to_string(sep="",precision=2, alwayssign=True, pad=True)[0:7]
        )
        source_names.append(source_name)

    table["name"] = source_names

    table["ra"].name = "RAJ2000"
    table["dec"].name = "DEJ2000"
    table["err_ra"].name = "err_RAJ2000"
    table["err_dec"].name = "err_DEJ2000"
    
    for k in ["int_flux", "err_int_flux", "peak_flux", "err_peak_flux"]:
        table[k].name = "fit_"+k
        table[k+"_corr"].name = k

    table["snr_fac"].name = "fbias"
    table["err_snr_fac"].name = "err_fbias"

    table["BMAJ"].name = "bmaj"
    table["BMIN"].name = "bmin"
    table["BPA"].name = "bpa"

    table["bmaj"] *= 3600.
    table["bmin"] *= 3600.
    
    columns = [
        "name",
        "RAJ2000",
        "err_RAJ2000",
        "DEJ2000",
        "err_DEJ2000",
        "local_rms",
        "background",
        "peak_flux",
        "err_peak_flux",
        "int_flux",
        "err_int_flux",
        "a",
        "err_a",
        "b",
        "err_b",
        "pa",
        "err_pa",
        "bmaj",
        "bmin",
        "bpa",
        "flags",
        "flux_scale_err",
        "fbias",
        "err_fbias"
    ]

    for k in ["a", "err_a", "b", "err_b", "bmaj", "bmin"]:
        table[k].unit = u.arcsec
    for k in ["RAJ2000", "err_RAJ2000", "DEJ2000", "err_DEJ2000", "pa", "err_pa", "bpa"]:
        table[k].unit = u.deg
    for k in ["local_rms", "background", "peak_flux", "err_peak_flux"]:
        table[k].unit = u.jansky/u.beam
    for k in ["int_flux", "err_int_flux"]:
        table[k].unit = u.jansky
        


    column_descs = {
        "name": "Component name following IAU convention: GLEAM-300 JHHMMSS+/-DDMMSS.",
        "RAJ2000": "J2000 right ascension of the component.",
        "err_RAJ2000": "Uncertainty on right ascension from fitting the component.",
        "DEJ2000": "J2000 declination of the component.",
        "err_DEJ2000": "Uncertainty on declination from fitting the component.",
        "local_rms": "Local estimate of the root-mean-square noise.",
        "background": "Local estimate of the background.",
        "peak_flux": "Peak flux density of the component.",
        "err_peak_flux": "Uncertainty in the peak flux density.",
        "int_flux": "Integrated flux density of the component.",
        "err_int_flux": "Uncertainty in the integrated flux density of the component.",
        "a": "FWHM of the major axis of the component.",
        "err_a": "Uncertainty in the FWHM of the major axis.",
        "b": "FWHM of the minor axis of the component.",
        "err_b": "Uncertainty in the FWHM of the minor axis.",
        "pa": "Position angle of the component.",
        "err_pa": "Uncertainty in the position angle.",
        "bmaj": "Local major axis of the beam.",
        "bmin": "Local minor axis of the beam.",
        "bpa": "Local position angle of the beam.",
        "flags": "Aegean fitting flags.",
        "flux_scale_err": "Flux density scale uncertainty.",
        "fbias": "Faint source bias factor.",
        "err_fbias": "Uncertainty in the faint source bias factor."
    }

    column_ucds = {
        "name": "meta.id;meta.main",
        "RAJ2000": "pos.eq.ra;meta.main",
        "err_RAJ2000": "stat.error;pos.eq.ra",
        "DEJ2000": "pos.eq.dec;meta.main",
        "err_DEJ2000": "stat.error;pos.eq.dec",
        "local_rms": "stat.stdev;phot.flux.density.sb",
        "background": " stat.error.sys;phot.flux.density.sb",
        "peak_flux": "phot.flux.density;stat.max;em.radio;stat.fit",
        "err_peak_flux": "stat.error;phot.flux.density;em.radio;stat.fit",
        "int_flux": "phot.flux.density;em.radio;stat.fit",
        "err_int_flux": "stat.error;phot.flux.density;em.radio;stat.fit",
        "a": "phys.angSize.smajAxis;em.radio;stat.fit",
        "err_a": "stat.error;phys.angSize.smajAxis;em.radio",
        "b": "phys.angSize.sminAxis;em.radio;stat.fit",
        "err_b": "stat.error;phys.angSize.sminAxis;em.radio",
        "pa": "stat.error;phys.angSize;pos.posAng;em.radio",
        "err_pa": "Uncertainty in the position angle.",
        "bmaj": "instr.det.psf",
        "bmin": "instr.det.psf",
        "bpa": "instr.det.psf",
        "flags": "meta.code",
        "flux_scale_err": "stat.error.sys;phot.calib",
        "fbias": "stat.error.sys;phot.calib",
        "err_fbias": "stat.error;phot.calib"
    }



    for k in [
        "int_flux", "peak_flux",
        "RAJ2000", "DEJ2000",
        "a", "b", "pa",
        "fbias"
    ]:
        logger.info("Rounding to {} sig figs for {} / {}".format(round_to_n, k, "err_"+k))
        table[k], table["err_"+k] = format_to_n_sigfig(
            values=table[k],
            errs=table["err_"+k],
            n=round_to_n
        )

    np.around(table["local_rms"], decimals=4, out=table["local_rms"])
    np.around(table["background"], decimals=4, out=table["background"])

    for k in ["bmaj", "bmin", "bpa"]:
        np.around(table[k], decimals=3, out=table[k])

    final_columns = []
    for c in columns:
        logger.info("Adding {}".format(c))
        column = Column(
            data=table[c].value,
            name=c,
            dtype=table[c].dtype,
            unit=table[c].unit,
            description=column_descs[c]
        )
        final_columns.append(column)

    final_table = Table(final_columns)
    final_table.write(outname, overwrite=True)

    outname_vo = outname.replace(".fits", "") + ".xml"
    indexed_fields = ",".join(
        ["name", "RAJ2000", "DEJ2000", "int_flux", "peak_flux", "a", "b", "pa", "Flag"]
    )
    principal_fields = ",".join(
        ["name", "RAJ2000", "DEJ2000" ,"int_flux", "err_int_flux", "a", "b", "pa", "local_rms"]
    )
    votable = VOTableFile(version="1.3")
    resource = Resource()
    votable.resources.append(resource)
    table = vTable.from_table(votable, final_table)
    table.params.append(Param(votable, 
        name="Catalogue Name", 
        value=catalogue_name,
        arraysize="59")
    )
    table.params.append(Param(votable, 
        name="Indexed Fields",
        value=indexed_fields,
        arraysize="255")
    )
    table.params.append(Param(votable, 
        name="Principal Fields", 
        value=principal_fields,
        arraysize="255")
    )
    for i in range(len(table.fields)):
        table.fields[i].ucd = column_ucds[table.fields[i].ID]
        table.fields[i].description = column_descs[table.fields[i].ID]
    resource.tables.append(table)
    writeto(votable, outname_vo)

    reduce_votable(outname_vo, temp_suffix=".tmp")
    os.remove(outname_vo)
    shutil.copy2(outname_vo+".tmp", outname_vo)
    os.remove(outname_vo+".tmp")


def reduce_votable(votable, temp_suffix=".tmp"):
    with open(votable, "r") as f:
        with open(votable+temp_suffix, "w+") as g:
            lines = f.readlines()
            for line in lines:
                g.write(line.lstrip())




def get_planet_appearances(planet_obsid_lists, outname,
    tile_list="GLEAM300_tiles.fits",
    planet="jupiter"):
    """
    """

    obsids, ras, decs, tiles = [], [], [], []

    table = Table.read(tile_list)
    tile_coords = SkyCoord(
        ra=table["RA"],
        dec=table["DEC"],
        unit=(u.deg, u.deg)
    )

    for l in planet_obsid_lists:
        logger.info(l)
        with open(l) as f:
            lines = f.readlines()
            for line in lines:
                obsid = int(line[0:10])
                time = Time(obsid, format="gps")
                coords = get_body(planet, 
                    time=time,
                    location=MWA
                )

                seps = coords.separation(tile_coords)
                idx = np.argmin(seps.value)
                tile = table[idx]["TILE"]
                logger.info("{} ({:.3f}, {:.3f}) {} {}".format(
                    obsid, coords.ra.value, coords.dec.value, tile,
                    time.iso
                ))

                obsids.append(obsid)
                ras.append(coords.ra.value)
                decs.append(coords.dec.value)
                tiles.append(tile)

    outtable = Table(
        [obsids, ras, decs, tiles],
        names=["OBSID", "RA", "DEC", "TILE"],
    )

    outtable.write(outname, overwrite=True)



def filter_artefacts(pos_catalogue : str, neg_catalogue : str, image : str,
    flux_threshold1 : float = 2.,
    flux_threshold2 : float = 6.,
    sep_threshold1 : float = 5.,
    sep_threshold2 : float = 12.,
    ratio_threshold1 : float = 350.,
    ratio_threshold2 : float = 650.,
    flux_key : str = "int_flux",
    ra_key : str = "ra",
    dec_key : str = "dec",
    bmaj_key : str = "psf_a",
    plot : bool = False,
    require_negative_nearby : bool = True,
    suffix : str = ".ar.fits"):
    """Filter artefacts based on two-step thresholding from Hurley-Walker+2022.

    First filter is faint, but close.
    Second filter is stronger, but farther.

    """

    pos_table = Table.read(pos_catalogue)
    pos_coords = SkyCoord(
        ra=pos_table[ra_key]*u.deg,
        dec=pos_table[dec_key]*u.deg
    )
    neg_table = Table.read(neg_catalogue)
    neg_coords = SkyCoord(
        ra=neg_table[ra_key]*u.deg,
        dec=neg_table[dec_key]*u.deg
    )

    sep1_pos = pos_table[bmaj_key]*sep_threshold1
    sep2_pos = pos_table[bmaj_key]*sep_threshold2

    sep1_neg = neg_table[bmaj_key]*sep_threshold1
    sep2_neg = neg_table[bmaj_key]*sep_threshold2
    
    cond1 = flux_threshold1 < pos_table[flux_key] < flux_threshold2
    cond2 = flux_threshold2 < pos_table[flux_key]
    idx1 = np.where(cond1)[0]
    idx2 = np.where(cond2)[0]

    artefact_indices_pos = np.array([])
    artefact_indices_neg = np.array([])

    for i in idx1:

        prep_plot = False

        coord_i = pos_coords[i]

        seps_pos = coord_i.separation(pos_coords)
        seps_neg = coord_i.separation(neg_coords)
        
        sep_idx1_pos = np.where(seps_pos.value*3600. < sep1_pos)[0]
        sep_idx1_neg = np.where(seps_neg.value*3600. < sep1_neg)[0]

        if require_negative_nearby and len(sep_idx1_neg) < 1:
            # no negative sources, likely no positive sources?
            continue
    
        thresh_idx1_pos = np.where(
            ratio_threshold1*pos_table[sep_idx1_pos][flux_key] < pos_table[i][flux_key]
        )[0]

        thresh_idx1_neg = np.where(
            abs(ratio_threshold1*neg_table[sep_idx1_neg][flux_key]) < pos_table[i][flux_key]
        )[0]
        
        if len(thresh_idx1_pos) > 0:
            logger.debug("Removing {} positive artefacts around {:.2f} {:.2f}".format(
                len(thresh_idx1_pos), coord_i.ra.value, coord_i.dec.value
            ))
            artefact_indices_pos = np.concatenate(
                artefact_indices_pos, thresh_idx1_pos
            )

            prep_plot = True

        if len(thresh_idx1_neg) > 0:
            logger.debug("Removing {} negative artefacts around {:.2f} {:.2f}".format(
                len(thresh_idx1_neg), coord_i.ra.value, coord_i.dec.value
            ))
            artefact_indices_neg = np.concatenate(
                artefact_indices_neg, thresh_idx1_neg
            )

            prep_plot = True

        if prep_plot and plot:
            plotting.plot_artefact_removal(
                pos_table[thresh_idx1_pos],
                neg_table[thresh_idx1_neg],

            )
