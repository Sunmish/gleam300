#! /usr/bin/env python

import numpy as np
from subprocess import Popen
import os
from glob import glob

from astropy.visualization.wcsaxes import Quadrangle
from astropy.table import Table
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.wcs import WCS
from astropy.visualization.wcsaxes.frame import EllipticalFrame
from astropy.visualization.wcsaxes import SphericalCircle
from astropy.utils.data import get_pkg_data_filename
from astropy.io import fits
from astropy.time import Time
from astropy.visualization import simple_norm
from astropy.stats import sigma_clip

from regions import Regions

from tqdm import trange

import matplotlib.pylab as plt
import matplotlib as mpl
mpl.use("Agg")

from random import shuffle

from matplotlib.colors import LinearSegmentedColormap
from matplotlib.gridspec import GridSpec, SubplotSpec
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.ticker as ticker
from matplotlib.ticker import NullFormatter, MultipleLocator, FormatStrFormatter, ScalarFormatter, MaxNLocator, LogLocator, FixedLocator
from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle
from matplotlib.colors import rgb2hex, LinearSegmentedColormap, LogNorm, SymLogNorm

from scipy.stats import binned_statistic, iqr

from gleam300 import stats, utils, sed_fitting

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

try:
    import basic_axes
    import cmasher as cmr
    from skymodel import get_beam
    # from aplpy import regions
    import brokenaxes
except ImportError:
    logger.warning("Missing some plotting packages.")




params = {'text.usetex': False, 'mathtext.fontset': "dejavusans", 'axes.formatter.useoffset': False}
plt.rcParams.update(params)
mpl.rcParams.update({'axes.formatter.useoffset': False})

white_labels = False
if white_labels:
    mpl.rcParams.update({"text.color" : "white",
        "axes.labelcolor" : "white",
        "axes.edgecolor": "white",
        "axes.facecolor": "black",
        "savefig.facecolor" : "black",
        "ytick.color": "white",
        "xtick.color": "white"})


trans_map = LinearSegmentedColormap.from_list('trans', (
    # Edit this gradient at https://eltos.github.io/gradient/#0077FF-FFFDF6-FF006C
    (0.000, (0.000, 0.467, 1.000)),
    (0.500, (1.000, 0.992, 0.965)),
    (1.000, (1.000, 0.000, 0.424))))

special_cmaps = {"trans": trans_map}

GLEAMX_DR2_LABEL=r"GLEAM" + u"\u2010" + r"X\,DR2"
GLEAMSGP_LABEL=r"GLEAM SGP"



def parse_cmap(cmap : str):
    """Parse special colourmaps defined above."""

    if cmap in special_cmaps.keys():
        return special_cmaps[cmap]
    else:
        return plt.get_cmap(cmap)

try:
    COLOR_CMAP = parse_cmap("cmr.neon")
except ValueError:
    COLOR_CMAP = parse_cmap("gnuplot2")
COLOR_CMAP = [COLOR_CMAP(0.1*i) for i in range(11)]
COLOR_DICT = {
              "GLEAM": COLOR_CMAP[0],
              "GLEAMSGP": COLOR_CMAP[1],
              "GLEAMXDR2": COLOR_CMAP[2],
              "TGSS": COLOR_CMAP[3],
              "VCSS": COLOR_CMAP[4],
              "WISH": COLOR_CMAP[5],
              "TXS": COLOR_CMAP[6],
              "MRC": COLOR_CMAP[7],
              "SUMSS": COLOR_CMAP[8],
              "NVSS": COLOR_CMAP[9],
              }

def color_and_label(label):
    color = COLOR_DICT[label]
    if label == "GLEAMXDR2":
        label = GLEAMX_DR2_LABEL
    if label == "GLEAMSGP":
        label = GLEAMSGP_LABEL
    return label, color


def complement(rgb):
    """Get complementary color from rgb tuple.

    from https://stackoverflow.com/a/40234924/6058788
    """
    k = sum([min(rgb), max(rgb)])
    return tuple(k-c for c in rgb)


class MidpointNormalize(mpl.colors.Normalize):
    """Get linear normalization with a midpoint.
    https://stackoverflow.com/questions/20144529/shifted-colorbar-matplotlib/20146989#20146989
    """
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        mpl.colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))




def add_region_(region, header, axis, zorder=0, alpha=1.):
    """
    """

    PC, TC = regions.ds9(region, header, zorder=zorder)
    for t in PC.artistlist:
        t.set_alpha(alpha)
    for t in TC.artistlist:
        t.set_alpha(alpha)
    PC.add_to_axes(axis)
    TC.add_to_axes(axis)

    return axis


def add_region(region, header, axis, zorder=0, alpha=1):

    wcs = WCS(header).celestial
    regions = Regions.read(region, format="ds9")
    for region in regions:
        region = region.to_pixel(wcs)
        artist = region.as_artist()
        artist.set_zorder(zorder)
        artist.set_alpha(alpha)
        axis.add_artist(artist)
    return axis




def log_minor_fix(ax):
    locmaj = LogLocator(base=10.0, subs=(1.0, ), numticks=100)
    ax.set_major_locator(locmaj)
    locmin = LogLocator(base=10.0, subs=np.arange(2, 10) * .1,
        numticks=100)
    ax.set_minor_locator(locmin)
    ax.set_minor_formatter(NullFormatter())
    return ax



def set_latitude_grid2(ax, degrees, ends=(-90, 90)):
        """
        Set the number of degrees between each latitude grid.
        """
        # Skip -90 and 90, which are the fixed limits.
        grid = np.arange(ends[0], ends[1]+degrees, degrees)
        ax.yaxis.set_major_locator(FixedLocator(np.deg2rad(grid)))
        ax.yaxis.set_major_formatter(ax.ThetaFormatter(degrees))
        return ax


def make_template_image(ra=45., dec=-26.7, npix=500):
    """Make a template image to create WCS axes."""

    hdu = fits.PrimaryHDU()
    arr = np.full((npix, npix), 1.)
    hdu.data = arr
    hdu.header["CTYPE1"] = "RA---SIN"
    hdu.header["CTYPE2"] = "DEC--SIN"
    hdu.header["CRVAL1"] = ra
    hdu.header["CRVAL2"] = dec
    hdu.header["CDELT1"] = -(180./npix)
    hdu.header["CDELT2"] = 180./npix
    hdu.header["CRPIX1"] = npix//2 - 1
    hdu.header["CRPIX2"] = npix//2 - 1

    w = WCS(hdu.header).celestial

    indices = np.indices(hdu.data.shape)
    indices_x = indices[0].flatten()
    indices_y = indices[1].flatten()
    for i in range(len(indices_x)):
        r, d = w.all_pix2world(indices_y[i], indices_x[i], 0)
        if np.isnan(r) or np.isnan(d):
            arr[indices_x[i], indices_y[i]] = np.nan

    bbox = get_beam.minimal_bounding_box(hdu.data)
    hdu.data = hdu.data[bbox[0]:bbox[1], bbox[2]:bbox[3]]
    xdiff = hdu.header["CRPIX1"] - bbox[2]
    ydiff = hdu.header["CRPIX2"] - bbox[0]
    hdu.header["CRPIX1"] = xdiff
    hdu.header["CRPIX2"] = ydiff


    return hdu


def plot_sky_regions(outname : str = "GLEAM300_sky_regions.pdf",
    table : str = None,
    color : str = "dodgerblue",
    dpi : float = 72.):
    """Show regions used for mosaicking and source finding."""

    fontlabels = 14
    fontticks = 12

    filename = get_pkg_data_filename('allsky/allsky_rosat.fits')
    hdu = fits.open(filename)[0]
    wcs = WCS(hdu.header)
    wcs.wcs.ctype = ["RA---AIT", "DEC--AIT"]
    wcs.wcs.crval = [0, 0.]

    
    fig = plt.figure(figsize=(10, 5))
    ax = fig.add_subplot(111, projection=wcs, frame_class=EllipticalFrame)

    im = ax.imshow(hdu.data, vmin=-1e3, vmax=0., cmap="gray", origin='lower')

    # Clip the image to the frame
    im.set_clip_path(ax.coords.frame.patch)
    # ax = set_latitude_grid2(ax, 20, (-80, 80))
    # ax.set_latietude_grid(40)
    # ax.set_longitude_grid_ends(80)
    # ax.grid(True)
    ax.coords.grid(color='black', alpha=0.5, linestyle='solid')

    if table is not None:
        logger.info("Adding {}".format(table))
        table = Table.read(table)
        
        for i in range(len(table)):
            rfactor = 1.1*np.cos(np.radians(table[i]["DEC"]+table[i]["DEC_SIZE"]*0.5))
            r = Rectangle((table[i]["RA"], table[i]["DEC"]), 
                    table[i]["RA_SIZE"]/rfactor, 
                    table[i]["DEC_SIZE"],
                    edgecolor=color, 
                    facecolor=color,
                    alpha=0.2,
                    clip_on=True,
                    transform=ax.get_transform("fk5"))
            ax.add_patch(r)

    fig.savefig(outname, dpi=dpi)


def plot_sin_skyregions(outname, ra=45., dec=-26.7, table=None, npix=100,
    color="blue", alpha=0.5):
    """Plot sky regions for mosaicking/cataloguing in SIN projection."""

    plt.close("all")

    hdu = make_template_image(ra, dec, npix=npix)
    header = hdu.header
    data = hdu.data

    fontlabels = 15
    fontticks = 14

    wcs = WCS(header)

    fig = plt.figure(figsize=(8, 8))
    axes = [0.05, 0.05, 0.95, 0.95]
    caxes = [0.1, 0.86, 1-0.2, 0.02]

    ax =  fig.add_axes(axes, projection=wcs, frame_class=EllipticalFrame)

    im = ax.imshow(data,
        cmap="gray_r",
        vmin=0, vmax=1e10,
        origin="lower"
    )

    im.set_clip_path(ax.coords.frame.patch)

    ax.coords.grid(color="black")
    ax.coords[0].set_ticklabel(exclude_overlapping=True, size=fontlabels)
    ax.coords[1].set_ticklabel(size=fontlabels)

    if table is not None:
        
        for i, region in enumerate(table):
            print(region)
            add_region(region, header, ax, zorder=i, alpha=alpha)

        # if table is not None:
        #     logger.info("Adding {}".format(table))
        #     table = Table.read(table)
            
        #     for i in range(len(table)):
        #         rfactor = 1.2*np.cos(np.radians(table[i]["DEC"]+table[i]["DEC_SIZE"]*0.5))
        #         r = Rectangle((table[i]["RA"], table[i]["DEC"]), 
        #                 table[i]["RA_SIZE"], 
        #                 table[i]["DEC_SIZE"],
        #                 edgecolor=color, 
        #                 facecolor=color,
        #                 alpha=0.2,
        #                 clip_on=True,
        #                 transform=ax.get_transform("fk5"))
        #         ax.add_patch(r)


    # cax = fig.add_axes(caxes)

    plt.savefig(outname, dpi=72, bbox_inches="tight")



def rms_obsids(catalogues, outname, label="", ext="votable", 
    ra_key="ra", 
    dec_key="dec",
    yrange=(10, 1000),
    useold=True):
    """Plot rms as a function of OBSID."""

    plt.close("all")
    fontlabels = 15
    figsize = (15, 3)
    fig = plt.figure(figsize=figsize)
    # axes = fig.add_axes([0.05, 0.15, 0.94, 0.8])
    # sp = GridSpec(1, 1)


    if os.path.exists(outname.replace(".pdf", "") + "_stats.fits") and useold:
        table = Table.read(outname.replace(".pdf", "") + "_stats.fits")
    else:
        table = stats.rms_obsids(catalogues, 
            ext=ext,
            ra_key=ra_key, 
            dec_key=dec_key
        )
        table.write(outname.replace(".pdf", "") + "_stats.fits", overwrite=True)
    
    rms = table["RMS"]
    rms16 = table["RMS16"]
    rms84 = table["RMS84"]
    x_data = table["OBSID"]

    times = Time(x_data, format="gps")
    x_data = [time.mjd for time in times]

    x_sep = 1
    x_buff = 0
    xlimits = []
    xstart = x_data[0] - x_buff
    dates = []
    dates_x = []
    for i in range(len(x_data)-1):
        if abs(x_data[i+1] - x_data[i]) > x_sep:

            xlimits.append((xstart, x_data[i] + x_buff))
            xstart = x_data[i+1] - x_buff
    xlimits.append((xstart, x_data[-1] + x_buff))


    bax = brokenaxes.brokenaxes(
        xlims=xlimits,
        despine=False,
        tilt=60.,
        d=0.005,
        wspace=0.07,
        left=0.075, 
        right=0.99, 
        top=0.95, 
        bottom=0.25
        )

    bax.plot(x_data, rms*1000., 
        ls="",
        marker="o",
        c="crimson",
        ms=2,
        zorder=100,
    )

    for ax in bax.axs:
        x_formatter = ScalarFormatter(useOffset=False)
        ax.yaxis.set_major_formatter(x_formatter)

    bax.fill_between(x_data, rms16*1000., rms84*1000.,
        ls="",
        color="crimson",
        alpha=0.2
    )

    bax.tick_params(
        which="major", 
        axis="both",
        labelsize=fontlabels,
        length=7
    )
    bax.tick_params(
        which="minor",
        axis="both",
        length=5
    )

    bax.set_yscale("log")

    bax.set_ylabel(r"$\sigma_\mathrm{\mathsf{rms}}$ / mJy$\,$beam$^{-1}$", 
        fontsize=fontlabels,
        labelpad=100) 
    bax.set_xlabel(r"MJD", 
        fontsize=fontlabels,
        labelpad=0)
    
    bax.axhline(20., ls="--", c="black")
    bax.axhline(50., ls="--", c="black")
    bax.axhline(100., ls="--", c="black")

    bax.set_ylim(yrange)

    bax.axs[0].text(0.05, 0.95, "{}".format(label), fontsize=fontlabels, va="top", ha="left", transform=bax.axs[0].transAxes)

    plt.savefig(outname, dpi=72)
    plt.savefig(outname.replace(".pdf", ".png"), dpi=72)


def rms_obsids_colored_calibrators(database, strip, outname, label="",
    yrange=(10, 1000)):
    """Plot rms as a function of OBSID."""

    plt.close("all")
    fontlabels = 15
    figsize = (15, 3.2)
    fig = plt.figure(figsize=figsize)
    # axes = fig.add_axes([0.05, 0.15, 0.94, 0.8])
    # sp = GridSpec(1, 1)


    table = Table.read(database, format="ascii.csv")
    table = table[np.where(table["STRIP"] == strip)[0]]

    x_data = table["OBSID"]
    rms = table["RMS"]
    rms16 = table["RMS16"]
    rms84 = table["RMS84"]

    times = Time(x_data, format="gps")
    x_data = [time.mjd for time in times]

    x_sep = 1
    x_buff = 0
    xlimits = []
    xstart = x_data[0] - x_buff
    dates = []
    dates_x = []
    for i in range(len(x_data)-1):
        if abs(x_data[i+1] - x_data[i]) > x_sep:

            xlimits.append((xstart, x_data[i] + x_buff))
            xstart = x_data[i+1] - x_buff
    xlimits.append((xstart, x_data[-1] + x_buff))


    bax = brokenaxes.brokenaxes(
        xlims=xlimits,
        despine=False,
        tilt=60.,
        d=0.005,
        wspace=0.07,
        left=0.075, 
        right=0.99, 
        top=0.9, 
        bottom=0.25
        )

    idx1 = np.where(table["SOLUTIONS_TYPE"] == "B")[0]
    idx2 = np.where(table["SOLUTIONS_TYPE"] == "N")[0]
    idx3 = np.where(table["SOLUTIONS_TYPE"] == "S")[0]
    x_data = np.asarray(x_data)

    logger.info("{} B / {} S / {} N".format(len(idx1),len(idx2),len(idx3)))

    colors = ["black", "crimson", "dodgerblue"]
    markers = ["o", "s", "D"]
    labels = ["Calibrator (C)", "Nearest (N)", "Self (S)"]

    for i, idx in enumerate([idx1, idx2, idx3]):

        bax.plot(x_data[idx], rms[idx]*1000., 
            ls="",
            marker=markers[i],
            c=colors[i],
            ms=5,
            zorder=100,
            # mec="gray",
            mew=0.2,
            alpha=0.8,
            label=labels[i]
        )

    bax.fill_between(x_data, 
        rms16*1000., rms84*1000.,
        ls="",
        color="black",
        alpha=0.2,
        lw=0,
        edgecolor=None
    )

    bax.tick_params(
        which="major", 
        axis="both",
        labelsize=fontlabels,
        length=7
    )
    bax.tick_params(
        which="minor",
        axis="both",
        length=5
    )

    for ax in bax.axs:
        x_formatter = ScalarFormatter(useOffset=False)
        ax.yaxis.set_major_formatter(x_formatter)
    bax.set_yscale("log")

    bax.set_ylabel(r"$\sigma_\mathrm{\mathsf{rms}}$ / mJy$\,$beam$^{-1}$", 
        fontsize=fontlabels,
        labelpad=100) 
    bax.set_xlabel(r"MJD", 
        fontsize=fontlabels,
        labelpad=0)
    
    bax.axhline(20., ls="--", c="black")
    bax.axhline(50., ls="--", c="black")
    bax.axhline(100., ls="--", c="black")

    legend = bax.axs[0].legend(loc="upper left",
        shadow=False,
        fancybox=False,
        frameon=False,
        fontsize=fontlabels-2,
        numpoints=1,
        handletextpad=-0.2,
        columnspacing=0.5,
        bbox_to_anchor=(0.001, 1.2),
        framealpha=0.,
        ncol=3,
        markerscale=1.5
    )
    # legend.get_frame().set_edgecolor("dimgrey")

    bax.set_ylim(yrange)

    bax.axs[0].text(0.05, 0.95, "{}".format(label), fontsize=fontlabels, va="top", ha="left", transform=bax.axs[0].transAxes)

    plt.savefig(outname, dpi=72)
    plt.savefig(outname.replace(".pdf", ".png"), dpi=72)


def stats_obsids_colored_strips(database, outname, mode, label="", 
    cmap="gnuplot2", apply=False):
    """Plot rms as a function of OBSID."""

    plt.close("all")
    fontlabels = 15
    figsize = (15, 5.5)
    fig = plt.figure(figsize=figsize)

    table = Table.read(database)

    # strips = np.unique(table["STRIP"])
    strips = [
        "g235m86.0",
        "g235m72.0",
        "g235m63.1",
        "g235m55.0",
        "g235m47.5",
        "g235m40.4",
        "g235m33.5",
        "g235m26.7",
        "g235m19.9",
        "g235m13.0",
        "g235m5.9",
        "g235p1.6",
        "g235p9.7",
        "g235p18.6"
    ]

    x_data = table["OBSID"]
    factor = 1.
    if mode == "rms":
        rms = table["RMS"]
        rms16 = table["RMS16"]
        rms84 = table["RMS84"]
        factor = 1000.
        ylabel = r"$\sigma_\mathrm{\mathsf{rms}}$ / mJy$\,$beam$^{-1}$"
        yrange = (10, 1000)
        if apply:
            rms /= table["FLUX_RATIO_MODEL"]
            rms16 /= table["FLUX_RATIO_MODEL"]
            rms84 /= table["FLUX_RATIO_MODEL"]
            ylabel = ylabel + ", corrected"
    elif mode == "int_peak":
        rms = table["INT_PEAK"]
        rms16 = table["INT_PEAK16"]
        rms84 = table["INT_PEAK84"]
        ylabel = r"$S_\mathrm{\mathsf{int}} / S_\mathrm{\mathsf{peak}}$"
        yrange = (0.67, 1.5)
        if apply:
            rms /= table["INT_PEAK_MODEL"]
            rms16 /= table["INT_PEAK_MODEL"]
            rms84 /= table["INT_PEAK_MODEL"]
            ylabel = ylabel + ", corrected"
    elif mode == "flux_ratio":
        rms = table["FLUX_RATIO"]
        rms16 = table["FLUX_RATIO16"]
        rms84 = table["FLUX_RATIO84"]
        ylabel = r"$S_\mathrm{\mathsf{300\,MHz}} / S_\mathrm{\mathsf{model}}$"
        yrange = (0.2, 5.0)
        if apply:
            rms *= table["INT_PEAK_MODEL"]
            rms16 *= table["INT_PEAK_MODEL"]
            rms84 *= table["INT_PEAK_MODEL"]
            rms /= table["FLUX_RATIO_MODEL"]
            rms16 /= table["FLUX_RATIO_MODEL"]
            rms84 /= table["FLUX_RATIO_MODEL"]
            ylabel = ylabel + ", corrected"

    times = Time(x_data, format="gps")
    x_data = [time.mjd for time in times]
    x_data = table["RA"]

    x_sep = 100000000000
    x_buff = 0
    xlimits = []
    xstart = x_data[0] - x_buff
    dates = []
    dates_x = []
    for i in range(len(x_data)-1):
        if abs(x_data[i+1] - x_data[i]) > x_sep:

            xlimits.append((xstart, x_data[i] + x_buff))
            xstart = x_data[i+1] - x_buff
    xlimits.append((xstart, x_data[-1] + x_buff))

    bax = fig.add_axes([0.075, 0.13, 0.92, 0.75])

    x_data = np.asarray(x_data)

    cmap = plt.get_cmap(cmap)
    colors = [cmap(i*(1/len(strips))) for i in range(len(strips))]
    # shuffle(colors) # easier to read

    # for i, idx in enumerate([idx1, idx2, idx3]):
    for i in range(len(strips)):

        if strips[i] == "g235m86.0":
            label = "SCP"
        elif "p" in strips[i]:
            bits = strips[i].split("p")
            label = "$+{}^\\circ$".format(bits[-1])
        else:
            bits = strips[i].split("m")
            label = "$-{}^\\circ$".format(bits[-1])

        idx = np.where(table["STRIP"] == strips[i])[0]

        bax.plot(x_data[idx], rms[idx]*factor, 
            ls="",
            marker="o",
            c=colors[i],
            ms=5,
            zorder=100,
            mec="black",
            mew=0.2,
            alpha=0.8,
            label=label,
            rasterized=True
        )

    legend = bax.legend(loc="upper left",
        shadow=False,
        fancybox=False,
        frameon=False,
        fontsize=fontlabels-2,
        numpoints=1,
        handletextpad=-0.2,
        columnspacing=0.5,
        bbox_to_anchor=(0.001, 1.16),
        framealpha=0.,
        ncol=7,
        markerscale=3
    )

    bax.tick_params(
        which="major", 
        axis="both",
        labelsize=fontlabels,
        length=7
    )
    bax.tick_params(
        which="minor",
        axis="both",
        length=5,
        labelsize=fontlabels
    )

    bax.set_yscale("log")

    bax.set_ylabel(ylabel, 
        fontsize=fontlabels) 
    bax.set_xlabel(r"$\alpha_\mathrm{\mathsf{J2000}}$ / deg", 
        fontsize=fontlabels,
        labelpad=0)
    
    if mode == "rms":
        bax.axhline(20., ls="--", c="black", zorder=101, lw=2.)
        bax.axhline(50., ls="--", c="black", zorder=101, lw=2.)
        bax.axhline(100., ls="--", c="black", zorder=101, lw=2.)
    elif mode == "int_peak":
        bax.axhline(0.83, ls=":", c="black", zorder=101, lw=2.)
        bax.axhline(1., ls="--", c="black", zorder=101, lw=2.)
        bax.axhline(1.2, ls=":", c="black", zorder=101, lw=2.)
        bax.set_ylim([0.67, 1.5])
        bax.set_yticks([0.67, 1.0, 1.5])
        bax.set_yticklabels(["0.67", "1.0", "1.5"])
        bax.yaxis.set_minor_locator(ticker.LogLocator(subs=[0.67, 1.5]))
        bax.yaxis.set_minor_formatter(ScalarFormatter())
        bax.set_yticks([0.67, 1.0, 1.5])
        bax.set_yticklabels(["0.67", "1.0", "1.5"])
    elif mode == "flux_ratio":
        bax.axhline(0.83, ls=":", c="black", zorder=101, lw=2.)
        bax.axhline(1., ls="--", c="black", zorder=101, lw=2.)
        bax.axhline(1.2, ls=":", c="black", zorder=101, lw=2.)
        bax.set_ylim([0.2, 5])
        bax.yaxis.set_minor_locator(ticker.LogLocator(subs=[2, 5]))
        bax.yaxis.set_minor_formatter(ScalarFormatter())
        bax.yaxis.set_major_formatter(ScalarFormatter())


    bax.set_ylim(yrange)
    bax.set_xlim([0., 360.])

    plt.gca().invert_xaxis()

    plt.savefig(outname, dpi=300)
    plt.savefig(outname.replace(".pdf", ".png"), dpi=72)



def int_peak_obsids(catalogues, outname, 
    label="", 
    useold=True,
    int_flux_key="int_flux",
    peak_flux_key="peak_flux",
    ra_key="ra",
    dec_key="dec",
    local_rms_key="local_rms"):
    """Plot rms as a function of OBSID."""

    plt.close("all")
    fontlabels = 15
    figsize = (15, 3)
    fig = plt.figure(figsize=figsize)

    if os.path.exists(outname.replace(".pdf", "") + "_stats.fits") and useold:
        table = Table.read(outname.replace(".pdf", "") + "_stats.fits")
    else:
        table = stats.int_peak_obsids(
            catalogues,
            int_flux=int_flux_key,
            peak_flux=peak_flux_key,
            ra_key=ra_key,
            dec_key=dec_key,
            local_rms_key=local_rms_key
        )
        table.write(outname.replace(".pdf", "") + "_stats.fits", overwrite=True)
    
    rms = table["INT_PEAK"]
    rms16 = table["INT_PEAK16"]
    rms84 = table["INT_PEAK84"]
    x_data = table["OBSID"]
    ra = table["RA"]
    dec = table["DEC"]

    # rms, rms16, rms84, x_data, ra, dec = stats.int_peak_obsids(catalogues
    times = Time(x_data, format="gps")
    x_data = np.asarray([time.mjd for time in times])

    coords = SkyCoord(ra=ra*u.deg, dec=dec*u.deg)
    idx1 = np.where(abs(coords.galactic.b.value) < 5)[0]
    idx2 = np.where(abs(coords.galactic.b.value) >= 5)[0]

    x_sep = 1
    x_buff = 0
    xlimits = []
    xstart = x_data[0] - x_buff
    for i in range(len(x_data)-1):
        if abs(x_data[i+1] - x_data[i]) > x_sep:

            xlimits.append((xstart, x_data[i] + x_buff))
            xstart = x_data[i+1] - x_buff
    xlimits.append((xstart, x_data[-1] + x_buff))


    bax = brokenaxes.brokenaxes(
        xlims=xlimits,
        despine=False,
        tilt=60.,
        d=0.005,
        wspace=0.07,
        left=0.075, 
        right=0.99, 
        top=0.95, 
        bottom=0.25
        )

    bax.plot(x_data, rms, 
        ls="",
        marker="o",
        c="crimson",
        ms=2,
        zorder=100,
    )

    for ax in bax.axs:
        x_formatter = ScalarFormatter(useOffset=False)
        ax.yaxis.set_major_formatter(x_formatter)

    bax.fill_between(x_data, rms16, rms84,
        ls="",
        color="crimson",
        alpha=0.2,
        lw=0.
    )

    # bax.plot(x_data[idx2], rms[idx2], 
    #     ls="",
    #     marker="o",
    #     c="crimson",
    #     ms=2,
    #     zorder=100,
    # )

    # bax.fill_between(x_data[idx2], rms16[idx2], rms84[idx2],
    #     ls="",
    #     color="crimson",
    #     alpha=0.2
    # )

    bax.tick_params(
        which="major", 
        axis="both",
        labelsize=fontlabels,
        length=7
    )
    bax.tick_params(
        which="minor",
        axis="both",
        length=3.5,
        labelsize=fontlabels
    )

    bax.axhline(1.0, ls="--", c="black")
    bax.axhline(1.2, ls=":", c="black")
    bax.axhline(0.83, ls=":", c="black")

    bax.set_yscale("log")
    bax.set_ylim([0.67, 1.5])
    bax.set_yticks([0.67, 1.0, 1.5])
    bax.set_yticklabels(["0.67", "1.0", "1.5"])
    bax.axs[0].yaxis.set_minor_locator(ticker.LogLocator(subs=[0.67, 1.5]))
    # bax.axs[0].yaxis.set_major_formatter(ScalarFormatter())
    bax.axs[0].yaxis.set_minor_formatter(ScalarFormatter())
    bax.axs[0].set_yticks([0.67, 1.0, 1.5])
    bax.axs[0].set_yticklabels(["0.67", "1.0", "1.5"])
    


    bax.set_ylabel(r"$S_\mathrm{\mathsf{int}} / S_\mathrm{\mathsf{peak}}$", 
        fontsize=fontlabels,
        labelpad=100) 
    bax.set_xlabel(r"MJD", 
        fontsize=fontlabels,
        labelpad=0)

    bax.axs[0].text(0.05, 0.95, label, fontsize=fontlabels, va="top", ha="left", transform=bax.axs[0].transAxes)

    plt.savefig(outname, dpi=72)
    plt.savefig(outname.replace(".pdf", ".png"), dpi=72)


def int_peak_obsids_colored_calibrators(database, strip, outname, label="", apply=False):
    """Plot rms as a function of OBSID."""

    plt.close("all")
    fontlabels = 15
    figsize = (15, 3.2)
    fig = plt.figure(figsize=figsize)

    table = Table.read(database, format="ascii.csv")
    table = table[np.where(table["STRIP"] == strip)[0]]

    x_data = table["OBSID"]
    rms = table["INT_PEAK"]
    rms16 = table["INT_PEAK16"]
    rms84 = table["INT_PEAK84"]
    if apply:
        rms /= table["INT_PEAK_MODEL"]
        rms16 /= table["INT_PEAK_MODEL"]
        rms84 /= table["INT_PEAK_MODEL"]

    ra = table["RA"]
    dec = table["DEC"]

    # rms, rms16, rms84, x_data, ra, dec = stats.int_peak_obsids(catalogues
    times = Time(x_data, format="gps")
    x_data = np.asarray([time.mjd for time in times])

    coords = SkyCoord(ra=ra*u.deg, dec=dec*u.deg)
    idx1 = np.where(abs(coords.galactic.b.value) < 5)[0]
    idx2 = np.where(abs(coords.galactic.b.value) >= 5)[0]

    x_sep = 1
    x_buff = 0
    xlimits = []
    xstart = x_data[0] - x_buff
    for i in range(len(x_data)-1):
        if abs(x_data[i+1] - x_data[i]) > x_sep:

            xlimits.append((xstart, x_data[i] + x_buff))
            xstart = x_data[i+1] - x_buff
    xlimits.append((xstart, x_data[-1] + x_buff))


    bax = brokenaxes.brokenaxes(
        xlims=xlimits,
        despine=False,
        tilt=60.,
        d=0.005,
        wspace=0.07,
        left=0.075, 
        right=0.99, 
        top=0.9, 
        bottom=0.25
        )

    idx1 = np.where(table["SOLUTIONS_TYPE"] == "B")[0]
    idx2 = np.where(table["SOLUTIONS_TYPE"] == "N")[0]
    idx3 = np.where(table["SOLUTIONS_TYPE"] == "S")[0]
    x_data = np.asarray(x_data)

    logger.info("{} B / {} S / {} N".format(len(idx1),len(idx2),len(idx3)))

    colors = ["black", "crimson", "dodgerblue"]
    markers = ["o", "s", "D"]
    labels = ["Calibrator (C)", "Nearest (N)", "Self (S)"]

    for i, idx in enumerate([idx1, idx2, idx3]):

        bax.plot(x_data[idx], rms[idx], 
            ls="",
            marker=markers[i],
            c=colors[i],
            ms=5,
            zorder=100,
            # mec="gray",
            mew=0.2,
            alpha=0.8,
            label=labels[i],
        )

    bax.fill_between(x_data, 
        rms16, rms84,
        ls="",
        color="black",
        alpha=0.2,
        lw=0,
        edgecolor=None
    )

    for ax in bax.axs:
        x_formatter = ScalarFormatter(useOffset=False)
        ax.yaxis.set_major_formatter(x_formatter)

    bax.tick_params(
        which="major", 
        axis="both",
        labelsize=fontlabels,
        length=7
    )
    bax.tick_params(
        which="minor",
        axis="both",
        length=3.5,
        labelsize=fontlabels
    )

    bax.axhline(1.0, ls="--", c="black")
    bax.axhline(1.2, ls=":", c="black")
    bax.axhline(0.83, ls=":", c="black")

    bax.set_yscale("log")
    bax.set_ylim([0.67, 1.5])
    bax.set_yticks([0.67, 1.0, 1.5])
    bax.set_yticklabels(["0.67", "1.0", "1.5"])
    bax.axs[0].yaxis.set_minor_locator(ticker.LogLocator(subs=[0.67, 1.5]))
    # bax.axs[0].yaxis.set_major_formatter(ScalarFormatter())
    bax.axs[0].yaxis.set_minor_formatter(ScalarFormatter())
    bax.axs[0].set_yticks([0.67, 1.0, 1.5])
    bax.axs[0].set_yticklabels(["0.67", "1.0", "1.5"])
    
    legend = bax.axs[0].legend(loc="upper left",
        shadow=False,
        fancybox=False,
        frameon=False,
        fontsize=fontlabels-2,
        numpoints=1,
        handletextpad=-0.2,
        columnspacing=0.5,
        bbox_to_anchor=(0.001, 1.2),
        framealpha=0.,
        ncol=3,
        markerscale=1.5
    )
    # for i in range(3):
        # legend.legendHandles[i]._sizes = [100]

    ylabel = r"$S_\mathrm{\mathsf{int}} / S_\mathrm{\mathsf{peak}}$"
    if apply:
        ylabel = ylabel + ", corrected"

    bax.set_ylabel(ylabel, 
        fontsize=fontlabels,
        labelpad=100) 
    bax.set_xlabel(r"MJD", 
        fontsize=fontlabels,
        labelpad=0)

    bax.axs[0].text(0.05, 0.95, label, fontsize=fontlabels, va="top", ha="left", transform=bax.axs[0].transAxes)

    plt.savefig(outname, dpi=72)
    plt.savefig(outname.replace(".pdf", ".png"), dpi=72)



def flux_ratio_obsids_GLEAM_SUMSS(catalogues, outname, 
    label="", 
    useold=True,
    apply=False,
    yrange=[0.1, 10],
    survey_flux_multiplier=1.):
    """Plot flux density ratios as a function of OBSID."""

    plt.close("all")
    fontlabels = 15
    figsize = (15, 3)
    fig = plt.figure(figsize=figsize)
    # axes = fig.add_axes([0.05, 0.15, 0.94, 0.8])
    # sp = GridSpec(1, 1)

    tables = []
    for catalogue in catalogues:
        table = Table.read(catalogue)
        if "STRIP" in table.columns:
            idx = np.where(table["STRIP"] == "g235m86.0")[0]
            table = table[idx]
        tables.append(table)

    labels = ["GLEAM", "SUMSS"]
    rms = tables[0]["FLUX_RATIO_WMEAN"]
    rms2 = tables[1]["FLUX_RATIO_WMEAN"]
    rms16 = tables[0]["FLUX_RATIO16"]
    rms84 = tables[0]["FLUX_RATIO84"]
    x_data = tables[0]["OBSID"]

    if apply:
        rms *= tables[0]["INT_PEAK_MODEL"]
        rms /= tables[0]["FLUX_RATIO_MODEL"]

    rms162 = tables[1]["FLUX_RATIO16"]
    rms842 = tables[1]["FLUX_RATIO84"]
    x_data2 = tables[1]["OBSID"]

    if apply:
        for i in range(len(x_data2)):
            idx = np.where(x_data == x_data2[i])[0]
            rms2[i] *= tables[0]["INT_PEAK_MODEL"][idx]
            rms2[i] /= tables[0]["FLUX_RATIO_MODEL"][idx]

    obsids = x_data
    times = Time(x_data, format="gps")
    times2 = Time(x_data2, format="gps")
    x_data = np.asarray([time.mjd for time in times])
    x_data2 = np.asarray([time.mjd for time in times2])


    # flux_table = Table(
    #     [obsids, x_data, rms, rms16, rms84, ra, dec],
    #     names=["obsid", "mjd", "ratio", "ratio16", "ratio84", "ra", "dec"]
    # )

    # flux_table.write(outname.replace(".pdf", "") + ".fits", overwrite=True)

    x_sep = 1
    x_buff = 0
    xlimits = []
    xstart = x_data[0] - x_buff
    for i in range(len(x_data)-1):
        if abs(x_data[i+1] - x_data[i]) > x_sep:

            xlimits.append((xstart, x_data[i] + x_buff))
            xstart = x_data[i+1] - x_buff
    xlimits.append((xstart, x_data[-1] + x_buff))


    bax = brokenaxes.brokenaxes(
        xlims=xlimits,
        despine=False,
        tilt=60.,
        d=0.005,
        wspace=0.07,
        left=0.075, 
        right=0.99, 
        top=0.95, 
        bottom=0.25
        )

    bax.plot(x_data, rms, 
        ls="",
        marker="o",
        c="crimson",
        ms=2,
        zorder=100,
        alpha=0.7,
        label=labels[0]
    )

    bax.plot(x_data2, rms2, 
        ls="",
        marker="s",
        c="black",
        ms=2,
        alpha=0.8,
        zorder=99,
        label=labels[1]
    )

    for ax in bax.axs:
        x_formatter = ScalarFormatter(useOffset=False)
        ax.yaxis.set_major_formatter(x_formatter)

    # bax.fill_between(x_data, rms16, rms84,
    #     ls="",
    #     color="crimson",
    #     alpha=0.2,
    #     lw=0.
    # )

    # bax.fill_between(x_data2, rms162, rms842,
    #     ls="",
    #     color="black",
    #     alpha=0.2,
    #     lw=0.
    # )

    bax.tick_params(
        which="major", 
        axis="both",
        labelsize=fontlabels,
        length=7
    )
    bax.tick_params(
        which="minor",
        axis="both",
        length=3.5,
        labelsize=fontlabels
    )

    bax.axhline(1.0, ls="--", c="black")
    bax.axhline(1.2, ls=":", c="black")
    bax.axhline(0.83, ls=":", c="black")

    bax.set_yscale("log")
    bax.set_ylim(yrange)
    # bax.set_yticks([0.67, 1.0, 1.5])
    # bax.set_yticklabels(["0.67", "1.0", "1.5"])
    bax.axs[0].yaxis.set_minor_locator(ticker.LogLocator(subs=[2, 5]))
    # bax.axs[0].yaxis.set_major_formatter(ScalarFormatter())
    bax.axs[0].yaxis.set_minor_formatter(ScalarFormatter())
    bax.axs[0].yaxis.set_major_formatter(ScalarFormatter())
    # bax.axs[0].set_yticks([0.67, 1.0, 1.5])
    # bax.axs[0].set_yticklabels(["0.67", "1.0", "1.5"])
    

    ylabel = r"$S_\mathrm{\mathsf{300\,MHz}} / S_\mathrm{\mathsf{model}}$"
    if apply:
        ylabel = ylabel + ", corrected"

    bax.set_ylabel(ylabel, 
        fontsize=fontlabels,
        labelpad=100) 
    bax.set_xlabel(r"MJD", 
        fontsize=fontlabels,
        labelpad=0)

    legend = bax.axs[0].legend(loc="lower center",
        shadow=False,
        fancybox=False,
        frameon=True,
        fontsize=fontlabels-2,
        numpoints=1,
        handletextpad=-0.2,
        columnspacing=0.5,
        # bbox_to_anchor=(0.001, 1.2),
        # framealpha=0.,
        markerscale=3,
        ncol=2
    )

    bax.axs[0].text(0.05, 0.95, label, fontsize=fontlabels, va="top", ha="left", transform=bax.axs[0].transAxes)

    plt.savefig(outname, dpi=72)
    plt.savefig(outname.replace(".pdf", ".png"), dpi=72)



def flux_ratio_obsids(catalogues, outname, 
    label="", 
    useold=True,
    ra_key="old_ra",
    dec_key="old_dec",
    survey_flux_key="S_200",
    survey_freq=200.,
    yrange=[0.1, 10],
    survey_flux_multiplier=1.):
    """Plot flux density ratios as a function of OBSID."""

    plt.close("all")
    fontlabels = 15
    figsize = (15, 3)
    fig = plt.figure(figsize=figsize)
    # axes = fig.add_axes([0.05, 0.15, 0.94, 0.8])
    # sp = GridSpec(1, 1)

    if os.path.exists(outname.replace(".pdf", "") + "_stats.fits") and useold:
        table = Table.read(outname.replace(".pdf", "") + "_stats.fits")
    else:
        table = stats.flux_ratio_obsids(
            catalogues,
            ra_key=ra_key,
            dec_key=dec_key,
            survey_flux_key=survey_flux_key,
            survey_freq=survey_freq,
            survey_flux_multiplier=survey_flux_multiplier
        )
        table.write(outname.replace(".pdf", "") + "_stats.fits", overwrite=True)
    
    rms = table["FLUX_RATIO"]
    rms16 = table["FLUX_RATIO16"]
    rms84 = table["FLUX_RATIO84"]
    x_data = table["OBSID"]
    ra = table["RA"]
    dec = table["DEC"]

    obsids = x_data
    times = Time(x_data, format="gps")
    x_data = np.asarray([time.mjd for time in times])

    flux_table = Table(
        [obsids, x_data, rms, rms16, rms84, ra, dec],
        names=["obsid", "mjd", "ratio", "ratio16", "ratio84", "ra", "dec"]
    )

    flux_table.write(outname.replace(".pdf", "") + ".fits", overwrite=True)

    coords = SkyCoord(ra=ra*u.deg, dec=dec*u.deg)
    idx1 = np.where(abs(coords.galactic.b.value) < 5)[0]
    idx2 = np.where(abs(coords.galactic.b.value) >= 5)[0]

    x_sep = 1
    x_buff = 0
    xlimits = []
    xstart = x_data[0] - x_buff
    for i in range(len(x_data)-1):
        if abs(x_data[i+1] - x_data[i]) > x_sep:

            xlimits.append((xstart, x_data[i] + x_buff))
            xstart = x_data[i+1] - x_buff
    xlimits.append((xstart, x_data[-1] + x_buff))


    bax = brokenaxes.brokenaxes(
        xlims=xlimits,
        despine=False,
        tilt=60.,
        d=0.005,
        wspace=0.07,
        left=0.075, 
        right=0.99, 
        top=0.95, 
        bottom=0.25
        )

    bax.plot(x_data, rms, 
        ls="",
        marker="o",
        c="crimson",
        ms=2,
        zorder=100,
    )

    for ax in bax.axs:
        x_formatter = ScalarFormatter(useOffset=False)
        ax.yaxis.set_major_formatter(x_formatter)

    bax.fill_between(x_data, rms16, rms84,
        ls="",
        color="crimson",
        alpha=0.2,
        lw=0.
    )

    bax.tick_params(
        which="major", 
        axis="both",
        labelsize=fontlabels,
        length=7
    )
    bax.tick_params(
        which="minor",
        axis="both",
        length=3.5,
        labelsize=fontlabels
    )

    bax.axhline(1.0, ls="--", c="black")
    bax.axhline(1.2, ls=":", c="black")
    bax.axhline(0.83, ls=":", c="black")

    bax.set_yscale("log")
    bax.set_ylim(yrange)
    # bax.set_yticks([0.67, 1.0, 1.5])
    # bax.set_yticklabels(["0.67", "1.0", "1.5"])
    # bax.axs[0].yaxis.set_minor_locator(ticker.LogLocator(subs=[0.67, 1.5]))
    # bax.axs[0].yaxis.set_major_formatter(ScalarFormatter())
    bax.axs[0].yaxis.set_minor_formatter(ScalarFormatter())
    # bax.axs[0].set_yticks([0.67, 1.0, 1.5])
    # bax.axs[0].set_yticklabels(["0.67", "1.0", "1.5"])
    


    bax.set_ylabel(r"$S_\mathrm{\mathsf{300\,MHz}} / S_\mathrm{\mathsf{model}}$", 
        fontsize=fontlabels,
        labelpad=100) 
    bax.set_xlabel(r"MJD", 
        fontsize=fontlabels,
        labelpad=0)

    bax.axs[0].text(0.05, 0.95, label, fontsize=fontlabels, va="top", ha="left", transform=bax.axs[0].transAxes)

    plt.savefig(outname, dpi=72)
    plt.savefig(outname.replace(".pdf", ".png"), dpi=72)


def flux_ratio_obsids_colored_calibrators(database, strip, outname, 
    label="", apply=False):
    """Plot flux density ratios as a function of OBSID."""

    plt.close("all")
    fontlabels = 15
    figsize = (15, 3.2)
    fig = plt.figure(figsize=figsize)
    # axes = fig.add_axes([0.05, 0.15, 0.94, 0.8])
    # sp = GridSpec(1, 1)

    table = Table.read(database, format="ascii.csv")
    table = table[np.where(table["STRIP"] == strip)[0]]

    x_data = table["OBSID"]
    rms = table["FLUX_RATIO_WMEAN"]
    rms16 = table["FLUX_RATIO16"]
    rms84 = table["FLUX_RATIO84"]
    if apply:
        rms *= table["INT_PEAK_MODEL"]
        rms /= table["FLUX_RATIO_MODEL"]

        rms16 *= table["INT_PEAK_MODEL"]
        rms16 /= table["FLUX_RATIO_MODEL"]

        rms84 *= table["INT_PEAK_MODEL"]
        rms84 /= table["FLUX_RATIO_MODEL"]

    ra = table["RA"]
    dec = table["DEC"]

    obsids = x_data
    times = Time(x_data, format="gps")
    x_data = np.asarray([time.mjd for time in times])

    coords = SkyCoord(ra=ra*u.deg, dec=dec*u.deg)
    idx1 = np.where(abs(coords.galactic.b.value) < 5)[0]
    idx2 = np.where(abs(coords.galactic.b.value) >= 5)[0]

    x_sep = 1
    x_buff = 0
    xlimits = []
    xstart = x_data[0] - x_buff
    for i in range(len(x_data)-1):
        if abs(x_data[i+1] - x_data[i]) > x_sep:

            xlimits.append((xstart, x_data[i] + x_buff))
            xstart = x_data[i+1] - x_buff
    xlimits.append((xstart, x_data[-1] + x_buff))


    bax = brokenaxes.brokenaxes(
        xlims=xlimits,
        despine=False,
        tilt=60.,
        d=0.005,
        wspace=0.07,
        left=0.075, 
        right=0.99, 
        top=0.9, 
        bottom=0.25
        )

    idx1 = np.where(table["SOLUTIONS_TYPE"] == "B")[0]
    idx2 = np.where(table["SOLUTIONS_TYPE"] == "N")[0]
    idx3 = np.where(table["SOLUTIONS_TYPE"] == "S")[0]
    x_data = np.asarray(x_data)

    logger.info("{} B / {} S / {} N".format(len(idx1),len(idx2),len(idx3)))

    colors = ["black", "crimson", "dodgerblue"]
    markers = ["o", "s", "D"]
    labels = ["Calibrator (C)", "Nearest (N)", "Self (S)"]

    for i, idx in enumerate([idx1, idx2, idx3]):

        bax.plot(x_data[idx], rms[idx], 
            ls="",
            marker=markers[i],
            c=colors[i],
            ms=5,
            zorder=100,
            # mec="gray",
            mew=0.2,
            alpha=0.8,
            label=labels[i]
        )

    bax.fill_between(x_data, 
        rms16, rms84,
        ls="",
        color="black",
        alpha=0.2,
        lw=0,
        edgecolor=None
    )

    for ax in bax.axs:
        x_formatter = ScalarFormatter(useOffset=False)
        ax.yaxis.set_major_formatter(x_formatter)

    bax.tick_params(
        which="major", 
        axis="both",
        labelsize=fontlabels,
        length=7
    )
    bax.tick_params(
        which="minor",
        axis="both",
        length=3.5,
        labelsize=fontlabels
    )

    bax.axhline(1.0, ls="--", c="black")
    bax.axhline(1.2, ls=":", c="black")
    bax.axhline(0.83, ls=":", c="black")

    bax.set_yscale("log")
    bax.set_ylim([0.2, 5])
    # bax.set_yticks([0.67, 1.0, 1.5])
    # bax.set_yticklabels(["0.67", "1.0", "1.5"])
    bax.axs[0].yaxis.set_minor_locator(ticker.LogLocator(subs=[2, 5]))
    bax.axs[0].yaxis.set_major_formatter(ScalarFormatter())
    bax.axs[0].yaxis.set_minor_formatter(ScalarFormatter())
    # bax.axs[0].set_yticks([0.67, 1.0, 1.5])
    # bax.axs[0].set_yticklabels(["0.67", "1.0", "1.5"])
    

    legend = bax.axs[0].legend(loc="upper left",
        shadow=False,
        fancybox=False,
        frameon=False,
        fontsize=fontlabels-2,
        numpoints=1,
        handletextpad=-0.2,
        columnspacing=0.5,
        bbox_to_anchor=(0.001, 1.2),
        framealpha=0.,
        ncol=3,
        markerscale=1.5
    )

    ylabel = r"$S_\mathrm{\mathsf{300\,MHz}} / S_\mathrm{\mathsf{model}}$"
    if apply:
        ylabel = ylabel + ", corrected"

    bax.set_ylabel(ylabel, 
        fontsize=fontlabels,
        labelpad=100) 
    bax.set_xlabel(r"MJD", 
        fontsize=fontlabels,
        labelpad=0)

    bax.axs[0].text(0.05, 0.95, label, fontsize=fontlabels, va="top", ha="left", transform=bax.axs[0].transAxes)

    plt.savefig(outname, dpi=72)
    plt.savefig(outname.replace(".pdf", ".png"), dpi=72)


def beam_example(obsid, plotname=None, cmap="gnuplot_r", marker_color="springgreen"):
    from skymodel import get_beam, parsers

    if plotname is None:
        plotname = f"{obsid}_beamI.pdf"

    if not os.path.exists(f"{obsid}.metafits"):

        s = f"wget http://ws.mwatelescope.org/metadata/fits/?obs_id={obsid} -O {obsid}.metafits"
        Popen(s, shell=True).wait()

    t, delays, freq, pnt = parsers.parse_metafits(f"{obsid}.metafits")

    if not os.path.exists(f"{obsid}_beamI.fits"):
    
        get_beam.make_beam_image(t, delays, freq, 
            outname=f"{obsid}_beamI.fits",
            trim_beam_image=True
        )

    plt.close("all")

    image = f"{obsid}_beamI.fits"
    header = fits.getheader(image)
    data = fits.getdata(image)
    ra, dec = header["CRVAL1"], header["CRVAL2"]

    fontlabels = 15
    fontticks = 14

    wcs = WCS(header)

    fig = plt.figure(figsize=(6.8, 8))
    axes = [0.05, 0.01, 0.9, 0.85]
    caxes = [0.1, 0.86, 1-0.2, 0.02]

    ax =  fig.add_axes(axes, projection=wcs, frame_class=EllipticalFrame)

    # norm = LogNorm(vmin=0.01, vmax=1.5)
    norm = simple_norm(data, stretch="sqrt", min_cut=0.0, max_cut=1.)

    im = ax.imshow(data,
        cmap=cmap,
        norm=norm,
        origin="lower"
    )

    im.set_clip_path(ax.coords.frame.patch)
    ax.coords.grid(color="black")
    ax.coords[0].set_ticklabel(exclude_overlapping=True, size=fontlabels)
    ax.coords[1].set_ticklabel(size=fontlabels)

    cax = fig.add_axes(caxes)
    colorbar = mpl.colorbar.ColorbarBase(cax,
        cmap=cmap,
        norm=norm,
        orientation="horizontal",
    )

    colorbar.set_label(
        "Stokes I beam attenuation",
        fontsize=fontlabels,
        labelpad=10,
    )

    colorbar.ax.xaxis.set_ticks_position("top")
    colorbar.ax.xaxis.set_label_position("top")

    colorbar.ax.tick_params(which="major",
        labelsize=fontticks,
        length=4.,
        direction="out",
        labelcolor="black"
    )

    ax.contour(data, 
        transform=ax.get_transform(WCS(header)),
        levels=np.array([
            0.2, 0.5, 0.9
        ]), 
        colors="white"
    )

    ax.scatter(pnt.ra.value, pnt.dec.value, 
        transform=ax.get_transform("fk5"),
        s=100,
        marker="*",
        edgecolor=marker_color, 
        facecolor=marker_color
    )


    plt.savefig(plotname, dpi=72)



def flux_scale_comparison_gleam_vcss_gleamx(gleam_match, vcss_match, gleamx_match, 
    alpha=None, 
    overwrite=False,
    total_flux_key="int_flux",
    peak_flux_key="peak_flux",
    local_rms="local_rms",
    cmap1="gnuplot2",
    cmap2="cmr.neon",
    outname="GLEAM300_snr_flux.pdf"):
    """Plots showing flux scale comparison with GLEAM and VCSS."""

    gleam_table = Table.read(gleam_match)
    try:
        vcss_table = Table.read(vcss_match)
        gleamx_table = Table.read(gleamx_match)
    except Exception:
        vcss_table = None
        gleamx_table = None

    # print(np.min(gleam_table["alpha_2"]))
    idx1 = np.where(np.isfinite(gleam_table["alpha_2"]))
    # print(np.min(vcss_table["Ftot_2"]))

    median_alpha = np.nanmedian(gleam_table["alpha_2"][idx1])
    print("Median alpha from GLEAM: {}".format(median_alpha))
    
    if "flux_300" not in gleam_table.columns or overwrite:
        # Calculate 300 MHz flux density of GLEAM sources 
        # extrapolate from fitted alpha
        logger.info("Updating 300 MHz scaled flux for GLEAM")
        gleam_table["flux_300"] = utils.scale_flux(
            flux=gleam_table["Fintwide_2"],
            freq=200.,
            alpha=gleam_table["alpha_2"],
            out_freq=300.
        )
        gleam_table["flux_ratio"] = gleam_table["int_flux_1"]/gleam_table["flux_300"]
        gleam_table.write(gleam_match, overwrite=True)

    if vcss_table is None:
        raise RuntimeError("No VCSS and GLEAM-X DR2 tables.")

    if "flux_300" not in vcss_table.columns or overwrite:
        # Scale 340 MHz to 300 MHz for VCSS - use median alpha from GLEAM matches
        if alpha is None:
            alpha = median_alpha
        logger.info("Updating 300 MHz scaled flux for VCSS assuming median alpha")
        vcss_table["flux_300"] = utils.scale_flux(
            flux=vcss_table["Ftotc_2"]/1000.,
            freq=340,
            alpha=alpha,
            out_freq=300.
        )
        vcss_table["flux_ratio"] = vcss_table["int_flux_1"]/vcss_table["flux_300"]
        vcss_table.write(vcss_match, overwrite=True)

    if "flux_300" not in gleamx_table.columns or overwrite:
        logger.info("Updating 300 MHz scaled flux for GLEAMX")
        gleamx_table["flux_300"] = utils.scale_flux(
            flux=gleamx_table["int_flux_wide_2"],
            freq=200.,
            alpha=gleamx_table["sp_alpha_2"],
            out_freq=300.
        )
        gleamx_table["flux_ratio"] = gleamx_table["int_flux_1"]/gleamx_table["flux_300"]
        gleamx_table.write(gleamx_match, overwrite=True)

    ratio1 = gleam_table[total_flux_key+"_1"]/gleam_table["flux_300"]
    ratio2 = vcss_table[total_flux_key+"_1"]/vcss_table["flux_300"]
    ratio3 = gleamx_table[total_flux_key+"_1"]/gleamx_table["flux_300"]

    plt.close("all")
    cmap1 = parse_cmap(cmap1)
    cmap2 = parse_cmap(cmap2)

    colors = [cmap2(0.15), cmap2(0.3), cmap2(0.45)]
    fontsize = 14.
    figsize = (6, 9)
    fig = plt.figure(figsize=figsize)
    axes1 = fig.add_axes([0.15, 0.06, 0.65, 0.25])
    axes2 = fig.add_axes([0.15, 0.06+0.25+0.1/2, 0.65, 0.25])
    axes3 = fig.add_axes([0.15, 0.06+0.25+0.25+0.1, 0.65, 0.25])
    axesh1 = fig.add_axes([0.15+0.65+0.025, 0.06, 0.125, 0.25])
    axesh2 = fig.add_axes([0.15+0.65+0.025, 0.06+0.25+0.1/2, 0.125, 0.25])
    axesh3 = fig.add_axes([0.15+0.65+0.025, 0.06+0.25+0.25+0.1, 0.125, 0.25])
    
    

    snr1 = gleam_table[peak_flux_key+"_1"]/gleam_table[local_rms+"_1"]
    snr2 = vcss_table[peak_flux_key+"_1"]/vcss_table[local_rms+"_1"]
    snr3 = gleamx_table[peak_flux_key+"_1"]/gleamx_table[local_rms+"_1"]

    median1 = np.nanmedian(ratio1)
    median2 = np.nanmedian(ratio2)
    median3 = np.nanmedian(ratio3)
    r16_1 = np.nanpercentile(ratio1, [16.])[0]
    r84_1 = np.nanpercentile(ratio1, [84.])[0]
    r16_2 = np.nanpercentile(ratio2, [16.])[0]
    r84_2 = np.nanpercentile(ratio2, [84.])[0]
    r16_3 = np.nanpercentile(ratio3, [16.])[0]
    r84_3 = np.nanpercentile(ratio3, [84.])[0]

    logger.info("(GLEAM) Median: ${:.2f}".format(
        median1
    ) + r"_{-" + "{:.2f}".format(median1-r16_1) + r"}^{+" \
        + "{:.2f}".format(r84_1-median1) + r"}$")
    logger.info("(VCSS)  Median: ${:.2f}".format(
        median2
    ) + r"_{-" + "{:.2f}".format(median2-r16_2) + r"}^{+" \
        + "{:.2f}".format(r84_2-median2) + r"}$")
    logger.info("(GLEAMX) Median: ${:.2f}".format(
        median3
    ) + r"_{-" + "{:.2f}".format(median3-r16_3) + r"}^{+" \
        + "{:.2f}".format(r84_3-median3) + r"}$")
    
    flux_cuts = [10., 50., 100., 1000.][::-1]
    for flux_cut in flux_cuts:
        sigma10_cut1 = np.where(snr1>=flux_cut)[0]
        sigma10_median1 = np.nanmedian(ratio1[sigma10_cut1])
        sigma10_r16_1 = np.nanpercentile(ratio1[sigma10_cut1], [16.])[0]
        sigma10_r84_1 = np.nanpercentile(ratio1[sigma10_cut1], [84.])[0]

        logger.info("(GLEAM) Median ({} sigma): ${:.2f}".format(
            flux_cut, sigma10_median1
        ) + r"_{-" + "{:.2f}".format(sigma10_median1-sigma10_r16_1) + r"}^{+" \
            + "{:.2f}".format(sigma10_r84_1-sigma10_median1) + r"}$")
        
        sigma10_cut2 = np.where(snr2>=flux_cut)[0]
        sigma10_median2 = np.nanmedian(ratio2[sigma10_cut2])
        sigma10_r16_2 = np.nanpercentile(ratio2[sigma10_cut2], [16.])[0]
        sigma10_r84_2 = np.nanpercentile(ratio2[sigma10_cut2], [84.])[0]

        logger.info("(VCSS) Median ({} sigma): ${:.2f}".format(
            flux_cut, sigma10_median2
        ) + r"_{-" + "{:.2f}".format(sigma10_median2-sigma10_r16_2) + r"}^{+" \
            + "{:.2f}".format(sigma10_r84_2-sigma10_median2) + r"}$")
        
        sigma10_cut3 = np.where(snr3>=flux_cut)[0]
        sigma10_median3 = np.nanmedian(ratio3[sigma10_cut3])
        sigma10_r16_3 = np.nanpercentile(ratio3[sigma10_cut3], [16.])[0]
        sigma10_r84_3 = np.nanpercentile(ratio3[sigma10_cut3], [84.])[0]

        logger.info("(GLEAMX) Median ({} sigma): ${:.2f}".format(
            flux_cut, sigma10_median3
        ) + r"_{-" + "{:.2f}".format(sigma10_median3-sigma10_r16_3) + r"}^{+" \
            + "{:.2f}".format(sigma10_r84_3-sigma10_median3) + r"}$")
    
    # axes1.plot(snr1, ratio1,
    #     ms=5.,
    #     marker="o",
    #     mec=colors[1],
    #     ls="",
    #     zorder=100
    # )
    # axes2.plot(snr2, ratio2,
    #     ms=5.,
    #     marker="o",
    #     mec=colors[0],
    #     ls="",
    #     zorder=100
    # )

    norm = LogNorm(1, 1000)

    axes1.hexbin(snr1, ratio1,
        xscale="log",
        yscale="log",
        norm=norm,
        gridsize=75,
        cmap=cmap1,
        extent=(np.log10(5.), np.log10(2000.), np.log10(0.1), np.log10(1000))
    )
    axes3.hexbin(snr2, ratio2,
        xscale="log",
        yscale="log",
        gridsize=75,
        norm=norm,
        cmap=cmap1,
        extent=(np.log10(5.), np.log10(2000.), np.log10(0.1), np.log10(1000))
    )

    axes2.hexbin(snr3, ratio3,
        xscale="log",
        yscale="log",
        norm=norm,
        gridsize=75,
        cmap=cmap1,
        extent=(np.log10(5.), np.log10(2000.), np.log10(0.1), np.log10(1000))
    )

    bins = np.logspace(np.log10(0.1), np.log10(10), 50)
    axesh1.hist(
        ratio1, 
        bins=bins,
        orientation="horizontal",
        color=colors[0],
        fill=True,
        density=False,
        zorder=0,
        histtype="step",
        lw=2,
    )
    axesh3.hist(
        ratio2, 
        bins=bins,
        orientation="horizontal",
        color=colors[2],
        fill=True,
        density=False,
        zorder=0,
        histtype="step",
        lw=2,
    )
    axesh2.hist(
        ratio3, 
        bins=bins,
        orientation="horizontal",
        color=colors[1],
        fill=True,
        density=False,
        zorder=0,
        histtype="step",
        lw=2,
    )

    for axes_hist in [axesh1, axesh2, axesh3]:
        axes_hist.set_yscale("log")
        axes_hist.set_ylim([0.1, 10.])
        axes_hist.set_yticklabels([])
        axes_hist.set_ylabel("")

    for axes in [axes1, axes2, axes3, axesh1, axesh2, axesh3]:
        axes.axhline(1., ls="-", c="black", zorder=100, lw=1.)
        axes.axhline(0.91, ls="--", c="black", zorder=100, lw=1.)
        axes.axhline(1.1, ls="--", c="black", zorder=100, lw=1.)
        axes.axhline(1.2, ls=":", c="black", zorder=100, lw=1.)
        axes.axhline(0.83, ls=":", c="black", zorder=100, lw=1.)
        axes.tick_params(which="major", axis="both", labelsize=fontsize-2)
        axes.tick_params(which="major", axis="both", length=7)
        axes.tick_params(which="minor", axis="both", labelsize=fontsize-4, 
            length=3.5)
    for axes in [axes1, axes2, axes3]:
        axes.set_ylim([0.1, 10.])
        axes.set_xlim([5., 2000.])
        axes.set_xscale("log")
        axes.set_yscale("log")
        axes.set_yticks([0.1, 1., 10])
        axes.set_yticklabels(["0.1", "1", "10"])
        


    axes1.set_xlabel("SNR", fontsize=fontsize)
    axes1.set_ylabel(r"$S_{300} / S_\mathrm{\mathsf{GLEAM,scaled}}$", fontsize=fontsize)
    axes3.set_ylabel(r"$S_{300} / S_\mathrm{\mathsf{VCSS,scaled}}$", fontsize=fontsize)
    axes2.set_ylabel(r"$S_{300} / S_\mathrm{\mathsf{" + GLEAMX_DR2_LABEL + r",scaled}}$", fontsize=fontsize)
    axesh1.set_xlabel("$N$", fontsize=fontsize)

    pos = axes3.get_position()
    
    cbax = fig.add_axes([pos.x0, pos.y0+pos.height+0.01, pos.width, 0.035/2.])
    cbar = mpl.colorbar.ColorbarBase(cbax,
        cmap=cmap1,
        norm=norm,
        orientation="horizontal"
    )
    cbar.set_label(r"$N$",
        verticalalignment="bottom",
        horizontalalignment="center",
        labelpad=5,
        fontsize=fontsize-2
    )
    cbar.ax.tick_params(which="major", labelsize=fontsize-2, length=5,
        direction="out")
    cbar.ax.tick_params(which="minor", length=3, direction="out")
    cbar.ax.xaxis.set_label_position("top")
    cbar.ax.xaxis.set_ticks_position("top")

    plt.savefig(outname, dpi=300.)


def flux_scale_comparison(matched_catalogue,
    label,
    freq,
    flux,
    multiplier=1.,
    alpha=-0.8,
    overwrite=False,
    total_flux_key="int_flux",
    peak_flux_key="peak_flux",
    local_rms="local_rms",
    cmap1="gnuplot2",
    cmap2="cmr.neon",
    outname=None,
    absflux=False,
    min_snr_for_hist=0.,
    max_n=1000,
    ratio_range=[0.2, 5.],
    flux_scale_factor=1.,
    survey_peak_flux=None,
    survey_rms=None,
    q=None,
    running_median=False,
    do_fit=False,
    nbins=50,
    point_color="red"):
    """Single plot showing flux scale comparison with external catalogue."""

    gleam_table = Table.read(matched_catalogue)

    snr1 = gleam_table[peak_flux_key+"_1"]/gleam_table[local_rms+"_1"]
    if q is not None:
        gleam_table[f"{total_flux_key}_1_corr"] = stats.edd_bias(
            flux=gleam_table[f"{total_flux_key}_1"],
            snr=snr1,
            q=q
        )
        flux_key = f"{total_flux_key}_1_corr"
    else:
        flux_key = f"{total_flux_key}_1"

    if outname is None:
        outname = f"GLEAM300_snr_flux_{label}.pdf"

    if "flux_300" not in gleam_table.columns or overwrite:
        logger.info(f"Updating 300 MHz scaled flux for {label}")

        if q is not None and survey_peak_flux is not None and survey_rms is not None:
            snr = gleam_table[f"{survey_peak_flux}_2"] / gleam_table[f"{survey_rms}_2"]
            gleam_table[f"{flux}_2_corr"] = stats.edd_bias(
                flux=gleam_table[f"{flux}_2"],
                q=q,
                snr=snr
            )
            survey_flux_key = f"{flux}_2_corr"
        else:
            survey_flux_key = f"{flux}_2"

        if isinstance(alpha, str):
            alpha = gleam_table[alpha]
        else:
            alpha = np.full((len(gleam_table),), alpha)
        gleam_table["flux_300"] = utils.scale_flux(
            flux=gleam_table[survey_flux_key],
            freq=freq,
            alpha=alpha,
            out_freq=300.
        )*multiplier*flux_scale_factor

        
            
        gleam_table["flux_ratio"] = np.asarray(gleam_table[flux_key]) / np.asarray(gleam_table["flux_300"])
        gleam_table.write(matched_catalogue, overwrite=True)

    gleam_table = gleam_table[np.where(np.isfinite(gleam_table["flux_ratio"]))[0]]
    ratio1 = np.asarray(gleam_table[flux_key])/np.asarray(gleam_table["flux_300"])
    snr1 = gleam_table[peak_flux_key+"_1"]/gleam_table[local_rms+"_1"]

    plt.close("all")
    cmap1 = parse_cmap(cmap1)
    cmap2 = parse_cmap(cmap2)

    fontsize = 14.
    figsize = (6, 4)
    fig = plt.figure(figsize=figsize)
    axes1 = fig.add_axes([0.15, 0.15, 0.65, 0.65])
    axesh1 = fig.add_axes([0.15+0.65+0.025, 0.15, 0.125, 0.65])

    label, color = color_and_label(label)

    
    median1 = np.nanmedian(ratio1)
    r16_1 = np.nanpercentile(ratio1, [16.])[0]
    r84_1 = np.nanpercentile(ratio1, [84.])[0]

    logger.info("({}) Median: ${:.2f}".format(
        label, median1
    ) + r"_{-" + "{:.2f}".format(median1-r16_1) + r"}^{+" \
        + "{:.2f}".format(r84_1-median1) + r"}$")
    
    flux_cuts = [10., 50., 100., 1000.][::-1]
    for flux_cut in flux_cuts:
        try:
            sigma10_cut1 = np.where(snr1>=flux_cut)[0]
            sigma10_median1 = np.nanmedian(ratio1[sigma10_cut1])
            sigma10_r16_1 = np.nanpercentile(ratio1[sigma10_cut1], [16.])[0]
            sigma10_r84_1 = np.nanpercentile(ratio1[sigma10_cut1], [84.])[0]
            std_1 = np.nanstd(ratio1[sigma10_cut1])

            logger.info("({}) Median ({} sigma, std={:.2f}, {} sources): ${:.2f}".format(
                label, flux_cut, std_1, len(sigma10_cut1), sigma10_median1
            ) + r"_{-" + "{:.2f}".format(sigma10_median1-sigma10_r16_1) + r"}^{+" \
                + "{:.2f}".format(sigma10_r84_1-sigma10_median1) + r"}$")
        except Exception:
            pass    




    norm = LogNorm(1, max_n)

    axes1.hexbin(snr1, ratio1,
        xscale="log",
        yscale="log",
        norm=norm,
        gridsize=75,
        cmap=cmap1,
        extent=(np.log10(5.), np.log10(2000.), 
            np.log10(ratio_range[0]), np.log10(ratio_range[1]))
    )


    if running_median:
        bins = np.logspace(np.log10(5.), np.log10(1000.), nbins)
        bin_medians, bin_16, bin_84, bin_flux, bin_std = [], [], [], [], [] 
        for i in range(len(bins)-1):

            try:
                idx = np.where(
                    (snr1 > bins[i]) & \
                    (snr1 <= bins[i+1])
                )[0]
                params = np.nanpercentile(
                    ratio1[idx], [16., 84.]
                )
                logger.debug(params)
                bin_16.append(params[0])
                bin_84.append(params[1])
                bin_medians.append(np.nanmedian(ratio1[idx]))
                bin_flux.append(np.mean([bins[i], bins[i+1]]))
                bin_std.append(np.nanstd(np.log10(ratio1[idx])))
                
            except Exception:
                raise

        bin_flux = np.asarray(bin_flux)
        bin_medians = np.asarray(bin_medians)
        bin_16 = np.asarray(bin_16)
        bin_84 = np.asarray(bin_84)

        popt, perr, new_ys, snrs = stats.bootstrap_snr_ratio(
            snr=bin_flux, ratio=bin_medians, stds=bin_std
        )


        axes1.errorbar(bin_flux, bin_medians,
            ms=3.,
            yerr=(bin_medians-bin_16, bin_84-bin_medians),
            fmt="o",
            mec="red",
            c="red",
            ls="", 
            zorder=101
        )
        for i in range(len(new_ys)):
            axes1.errorbar(snrs, new_ys[i],
                ms=3.,
                fmt="x",
                mec="pink",
                c="pink",
                ls="", 
                zorder=99
            )
        # axes1.fill_between(bin_flux, bin_16, bin_84,
        #     lw=0,
        #     zorder=100,
        #     color="red",
        #     alpha=0.5
        # )

        x_model = np.logspace(np.log10(5.), np.log10(2000.), 1000)
        y_model = stats.snr_func(x_model, *popt)
        axes1.plot(x_model, y_model, ls="--", zorder=102, c="black")



    bins = np.logspace(np.log10(ratio_range[0]), np.log10(ratio_range[1]), 50)

    idx = np.where(snr1>min_snr_for_hist)[0]

    axesh1.hist(
        ratio1[idx], 
        bins=bins,
        orientation="horizontal",
        color=color,
        fill=True,
        density=False,
        zorder=0,
        histtype="step",
        lw=2,
    )

    for axes_hist in [axesh1]:
        axes_hist.set_yscale("log")
        axes_hist.set_ylim(ratio_range)
        axes_hist.set_yticklabels([])
        axes_hist.set_ylabel("")

    for axes in [axes1, axesh1]:
        axes.axhline(1., ls="-", c="black", zorder=100, lw=1.)
        # axes.axhline(0.91, ls="--", c="black", zorder=100, lw=1.)
        # axes.axhline(1.1, ls="--", c="black", zorder=100, lw=1.)
        axes.axhline(1.2, ls=":", c="black", zorder=100, lw=1.)
        axes.axhline(0.83, ls=":", c="black", zorder=100, lw=1.)
        axes.tick_params(which="major", axis="both", labelsize=fontsize-2)
        axes.tick_params(which="major", axis="both", length=7)
        axes.tick_params(which="minor", axis="both", labelsize=fontsize-4, 
            length=3.5)
    for axes in [axes1]:
        axes.set_ylim(ratio_range)
        axes.set_xlim([5., 2000.])
        axes.set_xscale("log")
        axes.set_yscale("log")
        axes.yaxis.set_minor_locator(ticker.LogLocator(subs=[2, 5]))
        # axes.yaxis.set_minor_formatter(FormatStrFormatter("%i"))
        axes.yaxis.set_minor_formatter(ScalarFormatter())
        axes.yaxis.set_major_formatter(ScalarFormatter())


        # axes.set_yticks([0.1, 1., 10])


        # axes.set_yticklabels(["0.1", "1", "10"])

    axes1.set_xlabel("SNR", fontsize=fontsize)
    axes1.set_ylabel(r"$S_{300} / S_\mathrm{\mathsf{" + f"{label}" + r",scaled}}$", fontsize=fontsize)
    axesh1.set_xlabel("$N$", fontsize=fontsize)

    pos = axes1.get_position()
    
    cbax = fig.add_axes([pos.x0, pos.y0+pos.height+0.01, pos.width, 0.03])
    cbar = mpl.colorbar.ColorbarBase(cbax,
        cmap=cmap1,
        norm=norm,
        orientation="horizontal"
    )
    cbar.set_label(r"$N$",
        verticalalignment="bottom",
        horizontalalignment="center",
        labelpad=5,
        fontsize=fontsize-2
    )
    cbar.ax.tick_params(which="major", labelsize=fontsize-2, length=5,
        direction="out")
    cbar.ax.tick_params(which="minor", length=3, direction="out")
    cbar.ax.xaxis.set_label_position("top")
    cbar.ax.xaxis.set_ticks_position("top")

    plt.savefig(outname, dpi=300.)


def clean_bias_fit(matched_catalogue,
    label="GLEAM",
    freq=200.0,
    flux="Fintfit200",
    multiplier=1.,
    alpha="alpha_2",
    overwrite=False,
    total_flux_key="int_flux",
    peak_flux_key="peak_flux",
    local_rms="local_rms",
    cmap1="gnuplot2",
    cmap2="cmr.neon",
    outname=None,
    absflux=False,
    min_snr_for_hist=0.,
    max_n=1000,
    ratio_range=[0.2, 5.],
    flux_scale_factor=1.,
    survey_peak_flux="Fpewide",
    survey_rms="lrmswide",
    q=1.54,
    running_median=True,
    do_fit=True,
    nbins=31,
    point_color="springgreen",
    popt=None):
    """Single plot showing fclean bias + fitting."""

    gleam_table = Table.read(matched_catalogue)

    snr1 = gleam_table[peak_flux_key+"_1"]/gleam_table[local_rms+"_1"]
    if q is not None:
        gleam_table[f"{total_flux_key}_1_corr"] = stats.edd_bias(
            flux=gleam_table[f"{total_flux_key}_1"],
            snr=snr1,
            q=q
        )
        flux_key = f"{total_flux_key}_1_corr"
    else:
        flux_key = f"{total_flux_key}_1"

    if outname is None:
        outname = f"GLEAM300_snr_flux_{label}.pdf"

    if "flux_300" not in gleam_table.columns or overwrite:
        logger.info(f"Updating 300 MHz scaled flux for {label}")

        if q is not None and survey_peak_flux is not None and survey_rms is not None:
            snr = gleam_table[f"{survey_peak_flux}_2"] / gleam_table[f"{survey_rms}_2"]
            gleam_table[f"{flux}_2_corr"] = stats.edd_bias(
                flux=gleam_table[f"{flux}_2"],
                q=q,
                snr=snr
            )
            survey_flux_key = f"{flux}_2_corr"
        else:
            survey_flux_key = f"{flux}_2"

        if isinstance(alpha, str):
            alpha = gleam_table[alpha]
        else:
            alpha = np.full((len(gleam_table),), alpha)
        gleam_table["flux_300"] = utils.scale_flux(
            flux=gleam_table[survey_flux_key],
            freq=freq,
            alpha=alpha,
            out_freq=300.
        )*multiplier*flux_scale_factor

        
            
        gleam_table["flux_ratio"] = np.asarray(gleam_table[flux_key]) / np.asarray(gleam_table["flux_300"])
        gleam_table.write(matched_catalogue, overwrite=True)

    gleam_table = gleam_table[np.where(np.isfinite(gleam_table["flux_ratio"]))[0]]
    ratio1 = np.asarray(gleam_table[flux_key])/np.asarray(gleam_table["flux_300"])
    snr1 = gleam_table[peak_flux_key+"_1"]/gleam_table[local_rms+"_1"]

    plt.close("all")
    cmap1 = parse_cmap(cmap1)
    cmap2 = parse_cmap(cmap2)

    fontsize = 14.
    figsize = (6, 5)
    fig = plt.figure(figsize=figsize)
    axes1 = fig.add_axes([0.15, 0.13, 0.8, 0.7])

    label, color = color_and_label(label)

    median1 = np.nanmedian(ratio1)
    r16_1 = np.nanpercentile(ratio1, [16.])[0]
    r84_1 = np.nanpercentile(ratio1, [84.])[0]

    norm = LogNorm(1, max_n)

    axes1.hexbin(snr1, ratio1,
        xscale="log",
        yscale="log",
        norm=norm,
        gridsize=75,
        cmap=cmap1,
        extent=(np.log10(5.), np.log10(2000.), 
            np.log10(ratio_range[0]), np.log10(ratio_range[1]))
    )

    if running_median:
        bins = np.logspace(np.log10(5.), np.log10(1000.), nbins)
        bin_medians, bin_16, bin_84, bin_flux, bin_std = [], [], [], [], [] 
        for i in range(len(bins)-1):

            try:
                idx = np.where(
                    (snr1 > bins[i]) & \
                    (snr1 <= bins[i+1])
                )[0]
                params = np.nanpercentile(
                    ratio1[idx], [16., 84.]
                )
                logger.debug(params)
                bin_16.append(params[0])
                bin_84.append(params[1])
                bin_medians.append(np.nanmedian(ratio1[idx]))
                bin_flux.append(np.mean([bins[i], bins[i+1]]))
                bin_std.append(np.nanstd(np.log10(ratio1[idx])))
                
            except Exception:
                pass

        bin_flux = np.asarray(bin_flux)
        bin_medians = np.asarray(bin_medians)
        bin_16 = np.asarray(bin_16)
        bin_84 = np.asarray(bin_84)

        if popt is None:
            popt, perr, new_ys, snrs = stats.bootstrap_snr_ratio(
                snr=bin_flux, ratio=bin_medians, stds=bin_std
            )

        ymodel1 = stats.snr_func(snrs, *popt)
        table1 = Table(
            [snrs, bin_medians, ymodel1],
            names=["snr", "ratio", "model"]
        ) 
        table1.write(outname.replace(".pdf", "") + ".fits", overwrite=True)

        axes1.errorbar(bin_flux, bin_medians,
            ms=5.,
            yerr=(bin_medians-bin_16, bin_84-bin_medians),
            fmt="o",
            mec="black",
            c=point_color,
            ecolor=point_color,
            ls="", 
            zorder=101,
            label="Bin median"
        )
        # for i in range(len(new_ys)):
        #     axes1.errorbar(snrs, new_ys[i],
        #         ms=3.,
        #         fmt="x",
        #         mec="pink",
        #         c="pink",
        #         ls="", 
        #         zorder=99
        #     )
        # axes1.fill_between(bin_flux, bin_16, bin_84,
        #     lw=0,
        #     zorder=100,
        #     color="red",
        #     alpha=0.5
        # )

        logger.info(popt)
        x_model = np.logspace(np.log10(5.), np.log10(2000.), 1000)
        y_model = stats.snr_func(x_model, *popt)
        axes1.set_xscale("log")
        axes1.set_yscale("log")
        axes1.plot(x_model, y_model, ls="--", zorder=102, c="red", label="Model")

        idx = np.argmin(abs(np.log10(y_model)))
        logger.info("Model at 1: {} sigma".format(x_model[idx]))


    for axes in [axes1]:
        axes.axhline(1., ls="-", c="black", zorder=100, lw=1.)
        # axes.axhline(0.91, ls="--", c="black", zorder=100, lw=1.)
        # axes.axhline(1.1, ls="--", c="black", zorder=100, lw=1.)
        # axes.axhline(1.2, ls=":", c="black", zorder=100, lw=1.)
        # axes.axhline(0.83, ls=":", c="black", zorder=100, lw=1.)
        axes.tick_params(which="major", axis="both", labelsize=fontsize-2)
        axes.tick_params(which="major", axis="both", length=7)
        axes.tick_params(which="minor", axis="both", labelsize=fontsize-4, 
            length=3.5)
    for axes in [axes1]:
        axes.set_ylim(ratio_range)
        axes.set_xlim([5., 2000.])
        axes.set_xscale("log")
        axes.set_yscale("log")
        axes.yaxis.set_minor_locator(ticker.LogLocator(subs=[2, 5]))
        # axes.yaxis.set_minor_formatter(FormatStrFormatter("%i"))
        axes.yaxis.set_minor_formatter(ScalarFormatter())


        # axes.set_yticks([0.1, 1., 10])


        # axes.set_yticklabels(["0.1", "1", "10"])

    axes1.set_xlabel("SNR", fontsize=fontsize)
    axes1.set_ylabel(r"$S_{300} / S_\mathrm{\mathsf{" + f"{label}" + r",scaled}}$", fontsize=fontsize)


    pos = axes1.get_position()
    
    cbax = fig.add_axes([pos.x0, pos.y0+pos.height+0.01, pos.width, 0.02])
    cbar = mpl.colorbar.ColorbarBase(cbax,
        cmap=cmap1,
        norm=norm,
        orientation="horizontal"
    )
    cbar.set_label(r"$N$",
        verticalalignment="bottom",
        horizontalalignment="center",
        labelpad=5,
        fontsize=fontsize-2
    )
    cbar.ax.tick_params(which="major", labelsize=fontsize-2, length=5,
        direction="out")
    cbar.ax.tick_params(which="minor", length=3, direction="out")
    cbar.ax.xaxis.set_label_position("top")
    cbar.ax.xaxis.set_ticks_position("top")

    legend = axes.legend(loc="upper right",
        shadow=False,
        fancybox=False,
        frameon=True,
        fontsize=fontsize-2,
        numpoints=1,
        handletextpad=0.5,
        columnspacing=0.5,
        framealpha=1.,
        ncol=1
    )


    plt.savefig(outname, dpi=300.)


def flux_scale_comparison_peak(matched_catalogue,
    label,
    freq,
    flux,
    multiplier=1.,
    alpha=-0.8,
    overwrite=False,
    total_flux_key="int_flux",
    peak_flux_key="peak_flux",
    local_rms="local_rms",
    cmap1="gnuplot2",
    cmap2="cmr.neon",
    outname=None,
    absflux=False,
    min_snr_for_hist=0.05,
    max_n=1000,
    ratio_range=[0.2, 5.],
    flux_scale_factor=1.,
    survey_peak_flux=None,
    survey_rms=None,
    q=None,
    frequency=300.,
    running_median=False,
    peak_range=[0.05, 50]):
    """Single plot showing flux scale comparison with external catalogue."""

    gleam_table = Table.read(matched_catalogue)

    snr1 = gleam_table[peak_flux_key+"_1"]/gleam_table[local_rms+"_1"]
    if q is not None:
        gleam_table[f"{total_flux_key}_1_corr"] = stats.edd_bias(
            flux=gleam_table[f"{total_flux_key}_1"],
            snr=snr1,
            q=q
        )
        flux_key = f"{total_flux_key}_1_corr"
    else:
        flux_key = f"{total_flux_key}_1"

    if outname is None:
        outname = f"GLEAM300_snr_flux_{label}.pdf"

    if "flux_300" not in gleam_table.columns or overwrite:
        logger.info(f"Updating 300 MHz scaled flux for {label}")

        # if q is not None:
        #     snr = gleam_table[f"{survey_peak_flux}_2"] / gleam_table[f"{survey_rms}_2"]
        #     gleam_table[f"{flux}_2_corr"] = stats.edd_bias(
        #         flux=gleam_table[f"{flux}_2"],
        #         q=q,
        #         snr=snr
        #     )
        #     survey_flux_key = f"{flux}_2_corr"
        # else:
        survey_flux_key = f"{flux}_2"

        if isinstance(alpha, str):
            alpha = gleam_table[alpha]
        else:
            alpha = np.full((len(gleam_table),), alpha)
        gleam_table["flux_300"] = utils.scale_flux(
            flux=gleam_table[survey_flux_key],
            freq=freq,
            alpha=alpha,
            out_freq=frequency
        )*multiplier*flux_scale_factor
            
        gleam_table["flux_ratio"] = gleam_table[flux_key] / gleam_table["flux_300"]
        gleam_table.write(matched_catalogue, overwrite=True)

    gleam_table = gleam_table[np.where(np.isfinite(gleam_table["flux_ratio"]))[0]]
    
    snr1 = gleam_table[f"{peak_flux_key}_1"]

    ratio1 = gleam_table[total_flux_key+"_1"]/gleam_table["flux_300"]

    plt.close("all")
    cmap1 = parse_cmap(cmap1)
    cmap2 = parse_cmap(cmap2)

    fontsize = 14.
    figsize = (6, 4)
    fig = plt.figure(figsize=figsize)
    axes1 = fig.add_axes([0.15, 0.15, 0.65, 0.65])
    axesh1 = fig.add_axes([0.15+0.65+0.025, 0.15, 0.125, 0.65])

    label, color = color_and_label(label)

    
    median1 = np.nanmedian(ratio1)
    r16_1 = np.nanpercentile(ratio1, [16.])[0]
    r84_1 = np.nanpercentile(ratio1, [84.])[0]

    logger.info("({}) Median: ${:.2f}".format(
        label, median1
    ) + r"_{-" + "{:.2f}".format(median1-r16_1) + r"}^{+" \
        + "{:.2f}".format(r84_1-median1) + r"}$")
    
    norm = LogNorm(1, max_n)

    axes1.hexbin(snr1, ratio1,
        xscale="log",
        yscale="log",
        norm=norm,
        gridsize=75,
        cmap=cmap1,
        extent=(np.log10(peak_range[0]), np.log10(peak_range[1]), 
            np.log10(ratio_range[0]), np.log10(ratio_range[1]))
    )

    if running_median:
        bins = np.logspace(np.log10(peak_range[0]), np.log10(peak_range[1]), 20)
        bin_medians, bin_16, bin_84, bin_flux = [], [], [], [] 
        for i in range(len(bins)-1):

            try:
                idx = np.where(
                    (snr1 > bins[i]) & \
                    (snr1 <= bins[i+1])
                )[0]
                params = np.nanpercentile(
                    ratio1[idx], [16., 84.]
                )
                bin_16.append(params[0])
                bin_84.append(params[1])
                bin_medians.append(np.nanmedian(ratio1[idx]))
                bin_flux.append(np.mean([bins[i], bins[i+1]]))
                
            except Exception:
                pass

        axes1.plot(bin_flux, bin_medians,
            ms=5.,
            marker="o",
            mec="red",
            c="red",
            ls="", 
            zorder=101
        )
        axes1.fill_between(bin_flux, bin_16, bin_84,
            lw=0,
            zorder=100,
            color="red",
            alpha=0.5
        )

    bins = np.logspace(np.log10(ratio_range[0]), np.log10(ratio_range[1]), 50)

    idx = np.where(snr1>min_snr_for_hist)[0]

    axesh1.hist(
        ratio1[idx], 
        bins=bins,
        orientation="horizontal",
        color=color,
        fill=True,
        density=False,
        zorder=0,
        histtype="step",
        lw=2,
    )

    for axes_hist in [axesh1]:
        axes_hist.set_yscale("log")
        axes_hist.set_ylim(ratio_range)
        axes_hist.set_yticklabels([])
        axes_hist.set_ylabel("")

    for axes in [axes1, axesh1]:
        axes.axhline(1., ls="-", c="black", zorder=100, lw=1.)
        # axes.axhline(0.91, ls="--", c="black", zorder=100, lw=1.)
        # axes.axhline(1.1, ls="--", c="black", zorder=100, lw=1.)
        axes.axhline(1.2, ls=":", c="black", zorder=100, lw=1.)
        axes.axhline(0.83, ls=":", c="black", zorder=100, lw=1.)
        axes.tick_params(which="major", axis="both", labelsize=fontsize-2)
        axes.tick_params(which="major", axis="both", length=7)
        axes.tick_params(which="minor", axis="both", labelsize=fontsize-4, 
            length=3.5)
    for axes in [axes1]:
        axes.set_ylim(ratio_range)
        axes.set_xlim(peak_range)
        axes.set_xscale("log")
        axes.set_yscale("log")
        axes.yaxis.set_minor_locator(ticker.LogLocator(subs=[2, 5]))
        # axes.yaxis.set_minor_formatter(FormatStrFormatter("%i"))
        axes.yaxis.set_minor_formatter(ScalarFormatter())


        # axes.set_yticks([0.1, 1., 10])


        # axes.set_yticklabels(["0.1", "1", "10"])

    axes1.set_xlabel(r"Peak flux density / Jy beam$^{-1}$", fontsize=fontsize)
    axes1.set_ylabel(r"$S_{300} / S_\mathrm{\mathsf{" + f"{label}" + r",scaled}}$", fontsize=fontsize)
    axesh1.set_xlabel("$N$", fontsize=fontsize)

    pos = axes1.get_position()
    
    cbax = fig.add_axes([pos.x0, pos.y0+pos.height+0.01, pos.width, 0.03])
    cbar = mpl.colorbar.ColorbarBase(cbax,
        cmap=cmap1,
        norm=norm,
        orientation="horizontal"
    )
    cbar.set_label(r"$N$",
        verticalalignment="bottom",
        horizontalalignment="center",
        labelpad=5,
        fontsize=fontsize-2
    )
    cbar.ax.tick_params(which="major", labelsize=fontsize-2, length=5,
        direction="out")
    cbar.ax.tick_params(which="minor", length=3, direction="out")
    cbar.ax.xaxis.set_label_position("top")
    cbar.ax.xaxis.set_ticks_position("top")

    plt.savefig(outname, dpi=300.)


def astrometry_square(matched_catalogue, 
    cmap1="gnuplot2",
    cmap2="cmr.neon",
    overwrite=False,
    outname=None,
    ra_key="ra",
    dec_key="dec",
    match_ra_key="_RAJ2000",
    match_dec_key="_DEJ2000",
    snr_limit=50,
    peak_flux_key="peak_flux",
    local_rms_key="local_rms",
    survey=None,
    offset_range=(-14, 14),
    find_keys=True,
    hexbin=False):
    """Square RA/DEC astrometry plots."""

    table = Table.read(matched_catalogue)
    if outname is None:
        outname = matched_catalogue.replace(".fits", "") + "_astrometry.pdf"

    if find_keys:
        if "RAJ2000_2" in table.columns:
            match_ra_key = "RAJ2000"
            match_dec_key = "DEJ2000"
        elif "_RAJ2000_2" in table.columns:
            match_ra_key = "_RAJ2000"
            match_dec_key = "_DEJ2000"

    if "ra_offset" not in table.columns or overwrite:
        table["ra_offset"] = 3600.*(table[ra_key+"_1"]-table[match_ra_key+"_2"]) * \
            np.cos(np.radians(table[dec_key+"_1"]))
        table["dec_offset"] = 3600.*(table[dec_key+"_1"]-table[match_dec_key+"_2"])

        table.write(matched_catalogue, overwrite=True)

    original_len = len(table)
    table = table[np.where(table[peak_flux_key+"_1"]/table[local_rms_key+"_1"]>snr_limit)[0]]
    logger.info("{} > {} sigma: {} / {}".format(
        matched_catalogue, snr_limit, len(table), original_len
    ))

    mean_ra = np.nanmean(table["ra_offset"])
    std_ra = np.nanstd(table["ra_offset"])
    mean_dec = np.nanmean(table["dec_offset"])
    std_dec = np.nanstd(table["dec_offset"])

    logger.info("{}: RA:  ${:.2f} \\pm {:.2f}$".format(matched_catalogue, mean_ra, std_ra))
    logger.info("{}: DEC: ${:.2f} \\pm {:.2f}$".format(matched_catalogue, mean_dec, std_dec))

    plt.close("all")
    cmap1 = parse_cmap(cmap1)
    cmap2 = parse_cmap(cmap2)

    if survey is None:
        colors = [cmap2[0.2], cmap2[0.8]]
    else:
        colors = [COLOR_DICT[survey], cmap2(0.8)]
    
    
    if hexbin:
        fontsize = 14
        figsize = (5, 4.5)
        f = 4.5/5
        xpad = 0.15
        ypad = 0.1
        asize = 0.65
        fig = plt.figure(figsize=figsize)
        axes1 = fig.add_axes([xpad, ypad, asize, asize/f])
        axesh1 = fig.add_axes([xpad+asize+0.02, ypad, 0.125, asize/f])
        axesh2 = fig.add_axes([xpad, ypad+asize/f+0.02, asize, 0.125])
        axesc = fig.add_axes([xpad+asize+0.02, ypad+asize/f+0.02, 0.125, 0.03])
    else:
        fontsize = 12.
        figsize = (3, 3)
        fig = plt.figure(figsize=figsize)
        axes1 = fig.add_axes([0.2, 0.2, 0.6, 0.6])
        axesh1 = fig.add_axes([0.2+0.6+0.02, 0.2, 0.125, 0.6])
        axesh2 = fig.add_axes([0.2, 0.2+0.6+0.02, 0.6, 0.125])



    if hexbin:
        norm = LogNorm(1, 100)
        axes1.hexbin(table["ra_offset"], table["dec_offset"],
            norm=norm,
            gridsize=75,
            cmap=cmap1,
            extent=( 
                offset_range[0], offset_range[1], offset_range[0], offset_range[1]
            )
         )
        

    else:
        axes1.plot(table["ra_offset"], table["dec_offset"],
            ms=2.,
            marker="o",
            mec=colors[0],
            c=colors[0],
            ls="",
            alpha=0.8,
            rasterized=True,
            zorder=0
        )

    axesh1.hist(
        table["dec_offset"], 
        bins=100,
        orientation="horizontal",
        color=colors[0],
        fill=True,
        density=False,
        zorder=0,
        histtype="step",
        lw=2,
    )

    axesh2.hist(
        table["ra_offset"], 
        bins=100,
        orientation="vertical",
        color=colors[0],
        fill=True,
        density=False,
        zorder=0,
        histtype="step",
        lw=2,
    )


    for axes in [axes1, axesh2]:
        axes.axvline(0., ls="-", c="black")
        axes.axvline(mean_ra, ls="--", c="black")
        axes.axvline(mean_ra-std_ra, ls=":", c="black")
        axes.axvline(mean_ra+std_ra, ls=":", c="black")

    for axes in [axes1, axesh1]:
        axes.axhline(0., ls="-", c="black")
        axes.axhline(mean_dec, ls="--", c="black")
        axes.axhline(mean_dec-std_dec, ls=":", c="black")
        axes.axhline(mean_dec+std_dec, ls=":", c="black")

    if hexbin:
        cbar = mpl.colorbar.ColorbarBase(axesc,
            cmap=cmap1,
            norm=norm,
            orientation="horizontal"
        )
        cbar.set_label(r"$N$",
            verticalalignment="bottom",
            horizontalalignment="center",
            labelpad=5,
            fontsize=fontsize-2
        )
        cbar.ax.tick_params(which="major", labelsize=fontsize-2, length=5,
            direction="out")
        cbar.ax.tick_params(which="minor", length=0, direction="out")
        cbar.ax.xaxis.set_label_position("top")
        cbar.ax.xaxis.set_ticks_position("top")

        cbar.ax.xaxis.set_ticks([1, 100])
        cbar.ax.set_xticklabels(["1", "100"])


    axesh1.set_yticklabels([])
    axesh1.set_ylabel("")
    axesh2.set_xticklabels([])
    axesh2.set_xlabel("")

    for axes in [axes1, axesh1, axesh2]:
        axes.tick_params(which="major", axis="both", labelsize=fontsize-2)
        axes.tick_params(which="major", axis="both", length=7)
        axes.tick_params(which="minor", axis="both", labelsize=fontsize-4, 
            length=3.5)
        axes.tick_params(which="both", axis="both", direction="in")
    axesh1.set_ylim(offset_range)
    axesh2.set_xlim(offset_range)
    for axes in [axes1]:
        axes.set_ylim(offset_range)
        axes.set_xlim(offset_range)

    ylabels = [r"$\Delta\alpha$cos($\delta$) / arcsec", r"$\Delta\delta$ / arcsec"]
    axes1.set_ylabel(ylabels[1], fontsize=fontsize)
    axes1.set_xlabel(ylabels[0], fontsize=fontsize)
    axesh1.set_xlabel("$N$", fontsize=fontsize)
    axesh2.set_ylabel("$N$", fontsize=fontsize, labelpad=0)

    plt.savefig(outname, dpi=72.)


def astrometry_dec(matched_catalogues=[
    "GLEAM300_sources_GLEAM_int_flux.fits",
    "GLEAM300_sources_GLEAMXDR2_int_flux.fits",
    "GLEAM300_sources_TGSS_int_flux.fits",
],
    catalogue_names=["GLEAM", "GLEAM-X DR2", "TGSS"],
    outname=None,
    dec_key="dec_1",
    dec_offset_key="dec_offset",
    cmap="trans",
    peak_flux_key="peak_flux_1",
    local_rms_key="local_rms_1",
    snr=50,
    nbins=21):
    """
    """


    if outname is None:
        outname = "GLEAM300_dec_astrometry.pdf"

    plt.close("all")
    fontsize = 12.
    figsize = (6, 4)
    cmap = parse_cmap(cmap)
    colors = [cmap(0.15), cmap(0.3), cmap(0.85)]
    fig = plt.figure(figsize=figsize)
    axes = fig.add_axes([0.15, 0.15, 0.84, 0.84])

    markers = ["o", "s", "D"]
    lses = ["-", "--", ":"]
    colors = [cmap(0.2), cmap(0.8), "black"]

    bins = np.linspace(-90, 24, nbins)
    
    for i in range(len(matched_catalogues)):
        # l, c = color_and_label(catalogue_names[i])
        c = colors[i]
        l = catalogue_names[i]
        t = Table.read(matched_catalogues[i])

        idx = np.where(t[peak_flux_key] / t[local_rms_key] > snr)[0]
        t = t[idx]


        bins_medians, bins_sigma, bins_snr = [], [], []
        for j in range(len(bins)-1):
            idx = np.where(
                (t[dec_key] > bins[j]) & (t[dec_key] <= bins[j+1])
            )[0]

            if len(idx) > 1:
                bins_medians.append(np.nanmedian(t[dec_offset_key][idx]))
                bins_sigma.append(np.nanstd(t[dec_offset_key][idx]))
                bins_snr.append(np.mean([bins[j], bins[j+1]]))
        
        bins_medians = np.asarray(bins_medians)
        bins_sigma = np.asarray(bins_sigma)

        axes.errorbar(
            bins_snr, bins_medians,
            yerr=bins_sigma,
            ms=6.,
            marker=markers[i],
            mec="black",
            c=c,
            ls=lses[i],
            label=l
        )

    axes.tick_params(which="major", axis="both", labelsize=fontsize)
    axes.tick_params(which="major", axis="both", length=7)
    axes.tick_params(which="minor", axis="both", labelsize=fontsize, 
        length=3.5)
    
    axes.axhline(0., ls="--", color="gray")

    axes.set_xlabel(r"$\delta_\mathrm{\mathsf{J2000}}$ / deg", fontsize=fontsize)
    axes.set_ylabel(r"$\Delta\delta$ / arcsec", fontsize=fontsize)

    legend = axes.legend(loc="upper right",
        shadow=False,
        fancybox=False,
        frameon=True,
        fontsize=fontsize,
        numpoints=1,
        handletextpad=0.5,
        columnspacing=0.5,
        framealpha=0.,
        ncol=1
    )

    plt.savefig(outname, dpi=72.)



def astrometry_4square(matched_catalogue, 
    cmap1="gnuplot2",
    cmap2="trans",
    overwrite=False,
    outname=None,
    ra_key="ra",
    dec_key="dec",
    match_ra_key="_RAJ2000",
    match_dec_key="_DEJ2000",
    snr_limit=50,
    peak_flux_key="peak_flux",
    local_rms_key="local_rms",
    offset_range=(-10, 10)):
    """Square RA/DEC astrometry plots."""

    table = Table.read(matched_catalogue)
    if outname is None:
        outname = matched_catalogue.replace(".fits", "") + "_astrometry.pdf"

    if "ra_offset" in table.columns or overwrite:
        table["ra_offset"] = 3600.*(table[ra_key+"_1"]-table[match_ra_key+"_2"]) * \
            np.cos(np.radians(table[dec_key+"_1"]))
        table["dec_offset"] = 3600.*(table[dec_key+"_1"]-table[match_dec_key+"_2"])

        table.write(matched_catalogue, overwrite=True)

    original_len = len(table)
    table = table[np.where(table[peak_flux_key+"_1"]/table[local_rms_key+"_1"]>snr_limit)[0]]
    logger.info("{} > {} sigma: {} / {}".format(
        matched_catalogue, snr_limit, len(table), original_len
    ))

    plt.close("all")
    cmap1 = parse_cmap(cmap1)
    cmap2 = parse_cmap(cmap2)

    colors = [cmap2(0.2), cmap2(0.8)]
    fontsize = 12.
    figsize = (3, 3)
    fig = plt.figure(figsize=figsize)
    axes1 = fig.add_axes([0.15, 0.15, 0.7, 0.8])
    axesh1 = fig.add_axes([0.15+0.7+0.02, 0.15, 0.125, 0.8])
    axesh2 = fig.add_axes([0.15, 0.15+0.8+0.02, 0.7, 0.125])

    axes1.plot(table["ra_offset"], table["dec_offset"],
        ms=5.,
        marker="o",
        mec=colors[0],
        ls="",
        zorder=100
    )

    axesh1.hist(
        table["dec_offset"], 
        bins=50,
        orientation="horizontal",
        color="lightgray",
        fill=True,
        density=False,
        zorder=0,
        histtype="step",
        lw=2,
    )

    axesh2.hist(
        table["ra_offset"], 
        bins=50,
        orientation="vertical",
        color="lightgray",
        fill=True,
        density=False,
        zorder=0,
        histtype="step",
        lw=2,
    )

    axesh1.set_yticklabels([])
    axesh1.set_ylabel("")
    axesh2.set_xticklabels([])
    axesh2.set_xlabel("")

    for axes in [axes1, axesh1, axesh2]:
        axes.tick_params(which="major", axis="both", labelsize=fontsize-2)
        axes.tick_params(which="major", axis="both", length=7)
        axes.tick_params(which="minor", axis="both", labelsize=fontsize-4, 
            length=3.5)
    axesh1.set_ylim(offset_range)
    axesh2.set_xlim(offset_range)
    for axes in [axes1]:
        axes.set_ylim(offset_range)
        axes.set_xlim(offset_range)

    axes1.set_ylabel(r"$\Delta\mathrm{\mathsf{DEC}}$", fontsize=fontsize)
    axes1.set_xlabel(r"$\Delta\mathrm{\mathsf{RA}}\cos \mathrm{\mathsf{DEC}}$", fontsize=fontsize)
    axesh1.set_xlabel("$N$", fontsize=fontsize)
    axesh2.set_ylabel("$N$", fontsize=fontsize)

    plt.savefig(outname, dpi=300.)


def reliability(sources, negative_sources,
    outname="GLEAM300_reliability.pdf",
    peak_flux_key="peak_flux",
    local_rms_key="local_rms",
    cmap="trans",
    snr_bins=50,
    max_snr=None):
    """Plot reliability as a function of SNR.
    
    Reliability defined as 1-N_pos/N_negative."""

    plt.close("all")
    fontsize = 14.
    figsize = (6, 4)
    cmap = parse_cmap(cmap)
    colors = [cmap(0.15), cmap(0.3), cmap(0.45)]

    positive_table = Table.read(sources)
    negative_table = Table.read(negative_sources)

    frac = len(negative_table)/len(positive_table)
    rel = (1 - (frac))*100.

    logger.info("N positive: {}".format(len(positive_table)))
    logger.info("N negative: {}".format(len(negative_table)))
    logger.info("Overall reliability: {:.3f} % ({:.3f} % artefacts)".format(rel, frac*100.))

    fig = plt.figure(figsize=figsize)
    axes = fig.add_axes([0.15, 0.15, 0.84, 0.84])

    psnr = positive_table[peak_flux_key]/positive_table[local_rms_key]
    nsnr = abs(negative_table[peak_flux_key]/negative_table[local_rms_key])

    if max_snr is None:
        max_snr = np.log10(np.nanmax(psnr))
    else:
        max_snr = np.log10(max_snr)

    bins = np.logspace(
        np.log10(np.nanmin(psnr)), 
        max_snr,
        snr_bins
    )

    print(bins)

    bin_rel = []
    bin_mid = []
    bin_size = []
    
    for i in range(len(bins)-1):
    
        try:
            pcond1 = psnr >= bins[i]
            pcond2 = psnr < bins[i+1]
            pidx = np.where(pcond1 & pcond2)[0]
            ncond1 = nsnr >= bins[i]
            ncond2 = nsnr < bins[i+1]
            nidx = np.where(ncond1 & ncond2)[0]

            frac = len(nidx) / len(pidx)
            rel = (1 - frac)*100.

            b = 10**(
                    np.mean(
                        [np.log10(bins[i]), np.log10(bins[i+1])]
                    )
                )

            s = bins[i+1]-bins[i]

            bin_rel.append(rel)
            bin_mid.append(b)
            bin_size.append(s)

            logger.debug("{}, {}".format(rel, b))
        
        except ZeroDivisionError:
            pass

    axes.errorbar(bin_mid, bin_rel,
        xerr=np.asarray(bin_size)/2,
        ms=6.,
        marker="o",
        mec="black",
        c="black",
        ls="",
    )

    axes.axhline(100., ls="--", color="gray")
    axes.set_xscale("log")

    axes.tick_params(which="major", axis="both", labelsize=fontsize)
    axes.tick_params(which="major", axis="both", length=7)
    axes.tick_params(which="minor", axis="both", labelsize=fontsize, 
        length=3.5)
    
   

    axes.xaxis.set_minor_locator(ticker.LogLocator(subs=[2, 3, 4, 5, 6, 7, 8, 9]))
    axes.xaxis.set_major_formatter(FormatStrFormatter("%i"))
    axes.xaxis.set_minor_formatter(ScalarFormatter())
    # axes.xaxis = log_minor_fix(axes.xaxis)

    
    axes.set_xlabel("SNR", fontsize=fontsize)
    axes.set_ylabel("Reliability, %", fontsize=fontsize)

    plt.savefig(outname, dpi=72., bbox_inches="tight")


def peak_int_catalogue(catalogue,
    outname=None,
    peak_flux_key="peak_flux",
    int_flux_key="int_flux",
    local_rms_key="local_rms",
    cmap="trans",
    snr_range=[5, 2000],
    ratio_range=[0.5, 3],
    cmap_hexbin="gnuplot2"
    ):

    if outname is None:
        outname = "GLEAM300_peak_int_ratio.pdf"

    plt.close("all")
    fontsize = 14.
    figsize = (6, 4)
    cmap = parse_cmap(cmap)
    fig = plt.figure(figsize=figsize)
    axes = fig.add_axes([0.15, 0.15, 0.84, 0.84])
    colors = [cmap(0.15), cmap(0.3), cmap(0.45)]

    table = Table.read(catalogue)

    ratio = table[int_flux_key] / table[peak_flux_key]
    snr = table[peak_flux_key] / table[local_rms_key]

    norm = LogNorm(1, 1000)

    med_ratio = np.nanmedian(ratio)
    ratio16 = np.nanpercentile(ratio, [16.])[0]
    ratio84 = np.nanpercentile(ratio, [84.])[0]
    logger.info("Median int/peak: {:.2f} (+{:.2f}) (-{:.2f})".format(
        med_ratio,
        ratio84-med_ratio,
        med_ratio-ratio16
    ))


    ratio100 = ratio[np.where(snr>100)[0]]
    med_ratio100 = np.nanmedian(ratio100)
    ratio16100 = np.nanpercentile(ratio100, [16.])[0]
    ratio84100 = np.nanpercentile(ratio100, [84.])[0]
    logger.info("Median int/peak: {:.2f} (+{:.2f}) (-{:.2f})".format(
        med_ratio100,
        ratio84100-med_ratio100,
        med_ratio100-ratio16100
    ))

    axes.hexbin(snr, ratio,
        xscale="log",
        yscale="log",
        norm=norm,
        gridsize=75,
        cmap=cmap_hexbin,
        extent=(np.log10(snr_range[0]), 
                np.log10(snr_range[1]), 
                np.log10(ratio_range[0]), 
                np.log10(ratio_range[1])),

        )
    # axes.plot(
    #     snr, ratio,
    #     ms=2., marker=".",
    #     color="black",
    #     alpha=0.5,
    #     ls="",
    #     rasterized=True
    # )


    axes.axhline(1., ls="-", c="black", zorder=100, lw=1.)
    # axes.axhline(med_ratio, ls="--", c="black", zorder=101, lw=1.)
    axes.tick_params(which="major", axis="both", labelsize=fontsize-2)
    axes.tick_params(which="major", axis="both", length=7)
    axes.tick_params(which="minor", axis="both", labelsize=fontsize-4, 
        length=3.5)
    axes.set_ylim(ratio_range)
    axes.set_xlim(snr_range)
    
    axes.set_xscale("log")
    axes.set_yscale("log")
    # axes.set_yticks([0.1, 1., 10])
    # axes.set_yticklabels(["0.1", "1", "10"])
    axes.yaxis.set_minor_locator(ticker.LogLocator(subs=[2, 3, 4, 5, 6, 7, 8, 9]))
    axes.yaxis.set_major_formatter(FormatStrFormatter("%i"))
    axes.yaxis.set_minor_formatter(ScalarFormatter())
    axes.set_xlabel("SNR", fontsize=fontsize)
    axes.set_ylabel(r"$S_\mathrm{\mathsf{int}} / S_\mathrm{\mathsf{peak}}$", fontsize=fontsize)

    pos = axes.get_position()
    
    cbax = fig.add_axes([pos.x0, pos.y0+pos.height+0.01, pos.width, 0.03])
    cbar = mpl.colorbar.ColorbarBase(cbax,
        cmap=cmap_hexbin,
        norm=norm,
        orientation="horizontal"
    )
    cbar.set_label(r"$N$",
        verticalalignment="bottom",
        horizontalalignment="center",
        labelpad=5,
        fontsize=fontsize-2
    )
    cbar.ax.tick_params(which="major", labelsize=fontsize-2, length=5,
        direction="out")
    cbar.ax.tick_params(which="minor", length=3, direction="out")
    cbar.ax.xaxis.set_label_position("top")
    cbar.ax.xaxis.set_ticks_position("top")

    plt.savefig(outname, dpi=300., bbox_inches="tight")


def make_all_sed_plots(catalogue, 
    min_snr=10, 
    cmap="cmr.neon", 
    output_directory="./"):
    
    table = Table.read(catalogue)
    pbar = trange(len(table))
    for i in pbar:
        if table["peak_flux"][i]/table["local_rms"][i] > min_snr:
            sed_plot(table, i, cmap=cmap, output_directory=output_directory)


def sed_plot(table, index, cmap="cmr.neon", output_directory="./"):
    """SED plotting."""

    from gleam300.sed_fitting import flux_keys, freqs, e_flux_keys2, powerlaw, cpowerlaw, cpowerlaw_from_ref

    plt.close("all")
    fontsize = 14.
    figsize = (5, 5)
    cmap = parse_cmap(cmap)
    if white_labels:
        # colors = ["cyan", "magenta", "white"]
        colors = ["#00b1e1", "#ee2ba0", "white", "lightgray"]
    else:
        colors = ["crimson", "gray", "black", cmap(0.15)]
    markers = ["*", "o", "x"]
    mses = [10, 6., 9.,]
    labels = ["GLEAM-300", "GLEAM-X", "RACS"]

    flux = np.array(
        [table[key][index] for key in flux_keys]
    )
    eflux = np.array(
        [table[key][index] for key in e_flux_keys2]
    )

    fig = plt.figure(figsize=figsize)
    axes = fig.add_axes([0.2, 0.2, 0.75, 0.75])

    axes.errorbar(freqs[0], flux[0],
        yerr=eflux[0],
        ms=mses[0],
        marker=markers[0],
        ls="",
        c=colors[0],
        label=labels[0]
    )

    axes.errorbar(freqs[1:6], flux[1:6],
        yerr=eflux[1:6],
        ms=mses[1],
        marker=markers[1],
        ls="",
        c=colors[1],
        label=labels[1]
    )

    for i in range(1, 4):
        axes.errorbar(freqs[-i], flux[-i],
            yerr=eflux[-i],
            ms=mses[2],
            marker=markers[2],
            ls="",
            c=colors[2],
            label=labels[2]
        )

    x_range = np.linspace(min(freqs), max(freqs), 1000)
    y1 = powerlaw(x_range, table["pl_a"][index], table["pl_b"][index])
    y2 = cpowerlaw(x_range, table["cpl_a"][index], table["cpl_b"][index], table["cpl_c"][index])
    y3 = cpowerlaw_from_ref(
        x=x_range,
        x0=200.,
        y0=table["sp_int_flux_fit_200_GLEAMX"][index],
        b=table["sp_alpha_GLEAMX"][index],
        c=0.
    )

    axes.set_xscale("log")
    axes.set_yscale("log")

    axes.margins(0.1, 0.1)
    ylims = axes.get_ylim()

    axes.plot(x_range, y1, ls="-", c="gray", label="Powerlaw")
    axes.plot(x_range, y2, ls="--", c="gray", label="Curved powerlaw")
    # axes.plot(x_range, y3, ls=":", c=colors[1], label="GLEAM power law")
    
    axes.set_ylim(ylims)

    

    axes.tick_params(which="major", axis="both", labelsize=fontsize)
    axes.tick_params(which="major", axis="both", length=7)
    axes.tick_params(which="minor", axis="both", labelsize=fontsize, 
        length=3.5)
    axes.tick_params(which="both", axis="both", direction="in")
    
    axes.xaxis.set_minor_locator(ticker.LogLocator(subs=[5]))
    # axes.xaxis.set_major_formatter(FormatStrFormatter("%i"))
    axes.xaxis.set_minor_formatter(ScalarFormatter())
    axes.xaxis.set_major_formatter(ScalarFormatter())
    axes.yaxis.set_minor_locator(
        ticker.LogLocator(
            subs=[
                2, 4, 6, 8, 
                # 2, 4, 6, 8,
                # 20, 40, 60, 80,
                # 200, 400, 600, 800,
                # 2000, 4000, 6000, 8000,
                # 0.02, 0.04, 0.06, 0.08,
                # 0.002, 0.004, 0.006, 0.008,
                # 0.0002, 0.0004, 0.0006, 0.0008
            ]
        )
    )
    # axes.yaxis.set_minor_formatter(FormatStrFormatter("%s"))
    axes.yaxis.set_minor_formatter(ScalarFormatter())
    axes.yaxis.set_major_formatter(ScalarFormatter())

    axes.set_xlabel("Frequency / MHz", fontsize=fontsize)
    axes.set_ylabel("Flux density / Jy", fontsize=fontsize)

    c = SkyCoord(ra=table["ra"][index]*u.deg, dec=table["dec"][index]*u.deg)
    source_name = "J{r}{d}".format(
        r=c.ra.to_string(unit=u.hourangle, sep="", precision=2,pad=True)[0:6],
        d=c.dec.to_string(sep="",precision=2, alwayssign=True, pad=True)[0:7]
    )

    hands1 = [
        Line2D([0], [0],
               linestyle="",
               color=colors[i],
               marker=markers[i],
               label=labels[i],
               markersize=8.,
               )
        for i in range(3)
    ]
    hands1 = [hands1[1], hands1[0], hands1[2]]
    hands2 = [
        Line2D([0], [0], linestyle="-", color="gray", label="PL"),
        Line2D([0], [0], linestyle="--", color="gray", label="CPL"),
        # Line2D([0], [0], linestyle=":", color=colors[1], label="GLEAM power law")
    ]

    if table["cpl_c"][index] > 0.5 or table["pl_b"][index] > 0:
        loc1 = "lower right"
        loc2 = "upper left"
    else:
        loc1 = "upper right"
        loc2 = "lower left"

    legend1 = axes.legend(loc=loc1,
        handles=hands1,
        shadow=False,
        fancybox=False,
        frameon=True,
        fontsize=fontsize-2,
        numpoints=1
    )
    legend1.get_frame().set_edgecolor("dimgrey")
    legend2 = axes.legend(loc=loc2,
        handles=hands2,
        shadow=False,
        fancybox=False,
        frameon=True,
        fontsize=fontsize-2,
        numpoints=1
    )
    legend2.get_frame().set_edgecolor("dimgrey")
    plt.gca().add_artist(legend1)

    outname = f"{output_directory}/{source_name}_SED.png"

    plt.savefig(outname, dpi=72., bbox_inches="tight")
    plt.savefig(outname.replace(".png", ".pdf"), dpi=72)



# def stacked_sed_plot()
# 

def poster_examples(image, outname, vsetting, 
    cmap="cmr.swamp_r", 
    log=False,
    bad_color="white"):

    plt.close("all")

    cmap = parse_cmap(cmap)
    cmap.set_bad(bad_color)

    figsize = (20, 20)

    fig = plt.figure(figsize=figsize)
    
    wcs = WCS(fits.getheader(image)).celestial    
    data = fits.getdata(image)
    
    if log:
        norm = mpl.colors.LogNorm(vmin=vsetting[0], vmax=vsetting[1])
    else:
        norm = mpl.colors.Normalize(vmin=vsetting[0], vmax=vsetting[1])

    axes = fig.add_axes([0., 0., 1.0, 1.0], projection=wcs)

    im = axes.imshow(data,
        norm=norm,
        cmap=cmap,
        origin="lower",
        aspect="equal"
    )

    plt.savefig(outname, dpi=300, bbox_inches="tight")



def blanksky_example(cmap="gnuplot2", tc="white", vsetting=(-10e-3, 300e-3)):

    image = "GLEAM300HiPS_blank.fits"
    norm = mpl.colors.Normalize(vmin=vsetting[0], vmax=vsetting[1])

    coords = (357.5256615, -21.6570847)
    fov = (40*0.5, 17*0.5)
    font_labels = 14
    font_ticks = 14

    figsize = (15, 6)
    cpad = 0.015
    cthick = 0.025

    fig = plt.figure(figsize=figsize)
    axes = [0.1, 0.1, 0.8, 0.89]

    ax, fig, cbax = basic_axes.make_axes(
        ax=axes,
        fig=fig,
        header=fits.getheader(image),
        data=fits.getdata(image),
        scale=1.,
        centre=coords,
        fov=fov,
        aspect="equal",
        do_colorbar=True,
        cmap=cmap,
        norm=norm,
        fontlabels=font_labels,
        do_beam=False,
        fontticks=font_ticks,
        do_axis_labels=True,
        tick_colour=tc,
        colorbar_thickness=cthick,
        colorbar_label_pad=0.2,
        colorbar_pad=cpad,
        colorbar_orientation="vertical",
        colorbar_label= r"Jy beam$^{-1}$",
    )

    ax.grid(color=tc)

    plt.savefig("GLEAM300_blanksky_example.pdf", dpi=300, bbox_inches="tight")


def smc_example(cmap="cmr.flamingo_r", tc="white", vsetting=(-10e-3, 300e-3)):

    image = "GLEAM300HiPS_SMC.fits"
    norm = mpl.colors.Normalize(vmin=vsetting[0], vmax=vsetting[1])

    f = 40/17
    coords = (13.9014054, -72.7949461)
    fov = (12, 12/f)

    font_labels = 14
    font_ticks = 14

    figsize = (15, 6)
    cpad = 0.015
    cthick = 0.025

    fig = plt.figure(figsize=figsize)
    axes = [0.1, 0.1, 0.8, 0.89]

    ax, fig, cbax = basic_axes.make_axes(
        ax=axes,
        fig=fig,
        header=fits.getheader(image),
        data=fits.getdata(image),
        scale=1.,
        centre=coords,
        fov=fov,
        aspect="equal",
        do_colorbar=True,
        cmap=cmap,
        norm=norm,
        fontlabels=font_labels,
        do_beam=False,
        fontticks=font_ticks,
        do_axis_labels=True,
        tick_colour=tc,
        colorbar_thickness=cthick,
        colorbar_label_pad=0.2,
        colorbar_pad=cpad,
        colorbar_orientation="vertical",
        colorbar_label= r"Jy beam$^{-1}$",
    )

    ax.grid(color=tc)

    plt.savefig("GLEAM300_smc_example.pdf", dpi=300, bbox_inches="tight")


def gp_example(cmap="cmr.flamingo_r", tc="white", vsetting=(-10e-3, 300e-3)):

    image = "GLEAM300HiPS_GP.fits"
    norm = mpl.colors.Normalize(vmin=vsetting[0], vmax=vsetting[1])

    f = 40./17.
    coords = (351.3488704, 0)
    fov = (20., 20./f)

    font_labels = 14
    font_ticks = 14

    figsize = (15, 6)
    cpad = 0.015
    cthick = 0.025

    fig = plt.figure(figsize=figsize)
    axes = [0.1, 0.1, 0.8, 0.89]

    ax, fig, cbax = basic_axes.make_axes(
        ax=axes,
        fig=fig,
        header=fits.getheader(image),
        data=fits.getdata(image),
        scale=1.,
        centre=coords,
        fov=fov,
        aspect="equal",
        do_colorbar=True,
        cmap=cmap,
        norm=norm,
        fontlabels=font_labels,
        do_beam=False,
        fontticks=font_ticks,
        do_axis_labels=True,
        tick_colour=tc,
        colorbar_thickness=cthick,
        colorbar_label_pad=0.2,
        colorbar_pad=cpad,
        colorbar_orientation="vertical",
        colorbar_label= r"Jy beam$^{-1}$",
    )

    ax.coords[0].set_axislabel(r"$l$")
    ax.coords[1].set_axislabel(r"$b$")

    ax.grid(color=tc)

    plt.savefig("GLEAM300_gp_example.pdf", dpi=300, bbox_inches="tight")


def vela_example(cmap="cmr.flamingo_r", tc="white", vsetting=(-10e-3, 300e-3)):

    image = "GLEAM300HiPS_Vela.fits"
    norm = mpl.colors.Normalize(vmin=vsetting[0], vmax=vsetting[1])

    f = 40./17.
    coords = (117.0489943, -45.9965624)
    fov = (28., 28./f)

    font_labels = 14
    font_ticks = 14

    figsize = (15, 6)
    cpad = 0.015
    cthick = 0.025

    fig = plt.figure(figsize=figsize)
    axes = [0.1, 0.1, 0.8, 0.89]

    ax, fig, cbax = basic_axes.make_axes(
        ax=axes,
        fig=fig,
        header=fits.getheader(image),
        data=fits.getdata(image),
        scale=1.,
        centre=coords,
        fov=fov,
        aspect="equal",
        do_colorbar=True,
        cmap=cmap,
        norm=norm,
        fontlabels=font_labels,
        do_beam=False,
        fontticks=font_ticks,
        do_axis_labels=True,
        tick_colour=tc,
        colorbar_thickness=cthick,
        colorbar_label_pad=0.2,
        colorbar_pad=cpad,
        colorbar_orientation="vertical",
        colorbar_label= r"Jy beam$^{-1}$",
    )

    ax.grid(color=tc)

    plt.savefig("GLEAM300_vela_example.pdf", dpi=300, bbox_inches="tight")


def a3667_example(cmap="cmr.flamingo_r", tc="white", vsetting=(-10e-3, 300e-3)):

    image = "GLEAM300_imagesHiPS_a3667_small.fits"
    norm = mpl.colors.Normalize(vmin=vsetting[0], vmax=vsetting[1])

    coords = (304.0877041, -56.4215855)
    fov = (3.4, 3.4)

    font_labels = 14
    font_ticks = 14

    figsize = (7.5, 6)
    cpad = 0.015
    cthick = 0.025

    fig = plt.figure(figsize=figsize)
    axes = [0.2, 0.1, 0.7, 0.89]

    ax, fig, cbax = basic_axes.make_axes(
        ax=axes,
        fig=fig,
        header=fits.getheader(image),
        data=fits.getdata(image),
        scale=1.,
        centre=coords,
        fov=fov,
        aspect="equal",
        do_colorbar=True,
        cmap=cmap,
        norm=norm,
        fontlabels=font_labels,
        do_beam=False,
        fontticks=font_ticks,
        do_axis_labels=True,
        tick_colour=tc,
        colorbar_thickness=cthick,
        colorbar_label_pad=0.2,
        colorbar_pad=cpad,
        colorbar_orientation="vertical",
        colorbar_label= r"Jy beam$^{-1}$",
    )

    # ax.grid(color=tc)

    plt.savefig("GLEAM300_a3667_example.pdf", dpi=300, bbox_inches="tight")


def fornax_example(cmap="cmr.flamingo_r", tc="white", vsetting=(-10e-3, 300e-3)):

    image = "GLEAM300HiPS_FornaxA.fits"
    norm = mpl.colors.Normalize(vmin=vsetting[0], vmax=vsetting[1])

    coords = (50.6834920, -37.2394263)
    fov = (7.4, 7.4)

    font_labels = 14
    font_ticks = 14

    figsize = (7.5, 6)
    cpad = 0.015
    cthick = 0.025

    fig = plt.figure(figsize=figsize)
    axes = [0.2, 0.1, 0.7, 0.89]

    ax, fig, cbax = basic_axes.make_axes(
        ax=axes,
        fig=fig,
        header=fits.getheader(image),
        data=fits.getdata(image),
        scale=1.,
        centre=coords,
        fov=fov,
        aspect="equal",
        do_colorbar=True,
        cmap=cmap,
        norm=norm,
        fontlabels=font_labels,
        do_beam=False,
        fontticks=font_ticks,
        do_axis_labels=True,
        tick_colour=tc,
        colorbar_thickness=cthick,
        colorbar_label_pad=0.2,
        colorbar_pad=cpad,
        colorbar_orientation="vertical",
        colorbar_label= r"Jy beam$^{-1}$",
    )

    # ax.grid(color=tc)

    plt.savefig("GLEAM300_fornax_example.pdf", dpi=300, bbox_inches="tight")


def scp_example(cmap="cmr.flamingo_r", tc="black", vsetting=(-10e-3, 300e-3),
    rms=False):

    image = "GLEAM300_image_fix_unresolved/GLEAM300_J0000-90.fits"
    if rms:
        image = image.replace(".fits", "_rms.fits")
        vsetting = (10e-3, 30e-3)

    norm = mpl.colors.Normalize(vmin=vsetting[0], vmax=vsetting[1])

    f = 1.
    coords = (0., -90)
    fov = (30, 30)

    font_labels = 14
    font_ticks = 14

    figsize = (6, 6)
    cpad = 0.01
    cthick = 0.02

    fig = plt.figure(figsize=figsize)
    axes = [0.1, 0.1, 0.8, 0.8]

    ax, fig, cbax = basic_axes.make_axes(
        ax=axes,
        fig=fig,
        header=fits.getheader(image),
        data=fits.getdata(image),
        scale=1.,
        centre=coords,
        fov=fov,
        aspect="equal",
        do_colorbar=True,
        cmap=cmap,
        norm=norm,
        fontlabels=font_labels,
        do_beam=False,
        fontticks=font_ticks,
        do_axis_labels=True,
        tick_colour=tc,
        colorbar_thickness=cthick,
        colorbar_label_pad=0.2,
        colorbar_pad=cpad,
        colorbar_orientation="vertical",
        colorbar_label= r"Jy beam$^{-1}$",
    )

    ax.grid(color=tc)

    if not rms:
        plt.savefig("GLEAM300_scp_example.pdf", dpi=300, bbox_inches="tight")
    else:
        plt.savefig("GLEAM300_scp_rms_example.pdf", dpi=300, bbox_inches="tight")


def scp_example_double(cmap="cmr.flamingo_r", tc="black", vsetting=(-10e-3, 299e-3),
    outname="GLEAM300_SCP_example_plot.pdf"):

    image = "GLEAM300_image_fix_unresolved/GLEAM300_J0000-90.fits"
    rms_image = image.replace(".fits", "_rms.fits")
    vsetting2 = (8e-3, 29e-3)  

    norm = mpl.colors.Normalize(vmin=vsetting[0]*1000., vmax=vsetting[1]*1000.)
    norm2 = mpl.colors.Normalize(vmin=vsetting2[0]*1000., vmax=vsetting2[1]*1000.)

    f = 1.
    coords = (0., -90)
    fov = (30, 30)

    font_labels = 12
    font_ticks = 12

    figsize = (12, 6)
    cpad = 0.01
    cthick = 0.01

    fig = plt.figure(figsize=figsize)
    axes = [0.1, 0.1, 0.4, 0.8]
    axes2 = [0.4+0.05+0.1, 0.1, 0.4, 0.8]
    
    ax, fig, cbax = basic_axes.make_axes(
        ax=axes,
        fig=fig,
        header=fits.getheader(image),
        data=fits.getdata(image)*1000.,
        scale=1.,
        centre=coords,
        fov=fov,
        aspect="equal",
        do_colorbar=True,
        cmap=cmap,
        norm=norm,
        fontlabels=font_labels,
        do_beam=False,
        fontticks=font_ticks,
        do_axis_labels=True,
        tick_colour=tc,
        colorbar_thickness=cthick,
        colorbar_label_pad=0.2,
        colorbar_pad=cpad,
        colorbar_orientation="horizontal",
        colorbar_label= r"mJy beam$^{-1}$",
    )

    ax2, fig, cbax2 = basic_axes.make_axes(
        ax=axes2,
        fig=fig,
        header=fits.getheader(rms_image),
        data=fits.getdata(rms_image)*1000.,
        scale=1.,
        centre=coords,
        fov=fov,
        aspect="equal",
        do_colorbar=True,
        cmap=cmap,
        norm=norm2,
        fontlabels=font_labels,
        do_beam=False,
        fontticks=font_ticks,
        do_axis_labels=True,
        tick_colour=tc,
        colorbar_thickness=cthick,
        colorbar_label_pad=0.2,
        colorbar_pad=cpad,
        colorbar_orientation="horizontal",
        colorbar_label= r"mJy beam$^{-1}$",
    )

    ax2.coords[1].set_axislabel("")
    ax2.coords[1].set_ticklabel_visible(False)



    ax.grid(color=tc, linestyle="--")
    ax2.grid(color=tc, linestyle="--")

    plt.savefig(outname, dpi=300, bbox_inches="tight")


def scp_zoom_example(cmap="cmr.flamingo_r", tc="black", vsetting=(-10e-3, 300e-3)):

    image = "GLEAM300_images_rescaled/GLEAM300_J0000-90.fits"

    norm = mpl.colors.Normalize(vmin=vsetting[0], vmax=vsetting[1])

    f = 1.
    coords = (307.0065814, -88.0860803)
    fov = (6, 6)

    font_labels = 14
    font_ticks = 14

    figsize = (10, 10)
    cpad = 0.01
    cthick = 0.02

    fig = plt.figure(figsize=figsize)
    axes = [0.1, 0.1, 0.8, 0.8]

    ax, fig, cbax = basic_axes.make_axes(
        ax=axes,
        fig=fig,
        header=fits.getheader(image),
        data=fits.getdata(image),
        scale=1.,
        centre=coords,
        fov=fov,
        aspect="equal",
        do_colorbar=True,
        cmap=cmap,
        norm=norm,
        fontlabels=font_labels,
        do_beam=False,
        fontticks=font_ticks,
        do_axis_labels=True,
        tick_colour=tc,
        colorbar_thickness=cthick,
        colorbar_label_pad=0.2,
        colorbar_pad=cpad,
        colorbar_orientation="vertical",
        colorbar_label= r"Jy beam$^{-1}$",
    )

    ax.grid(color=tc)

    plt.savefig("GLEAM300_scp_zoom_example.pdf", dpi=300, bbox_inches="tight")

def tile_example(tile, cmap="cmr.flamingo_r", tc="black", vsetting=(-10e-3, 300e-3),
    rms=False, fontsize=10, outname=None, grid=False):

    image =  tile
    if rms:
        image = image.replace(".fits", "_rms.fits")
        vsetting = (10e-3, 30e-3)

    norm = mpl.colors.Normalize(vmin=vsetting[0], vmax=vsetting[1])

    font_labels = fontsize
    font_ticks = fontsize

    figsize = (10, 6)
    cpad = 0.01
    cthick = 0.02

    fig = plt.figure(figsize=figsize)
    axes = [0.1, 0.1, 0.8, 0.8]

    ax, fig, cbax = basic_axes.make_axes(
        ax=axes,
        fig=fig,
        header=fits.getheader(image),
        data=fits.getdata(image),
        scale=1.,
        aspect="equal",
        do_colorbar=True,
        cmap=cmap,
        norm=norm,
        fontlabels=font_labels,
        do_beam=False,
        fontticks=font_ticks,
        do_axis_labels=True,
        tick_colour=tc,
        colorbar_thickness=cthick,
        colorbar_label_pad=0.2,
        colorbar_pad=cpad,
        colorbar_orientation="vertical",
        colorbar_label= r"Jy beam$^{-1}$",
    )

    if grid:
        ax.grid(color=tc)

    if outname is None:
        tile = tile.replace(".fits", "")
        plt.savefig("{}.pdf".format(tile), dpi=300, bbox_inches="tight")
        plt.savefig("{}.png".format(tile), dpi=300, bbox_inches="tight")
    else:
        plt.savefig(outname, dpi=300, bbox_inches="tight")




def flux_ratio_with_model_by_night(table, outname=None, cmap="cmr.neon"):
    """Plot flux density ratios with models as a function of OBSID per night."""

    if outname is None:
        outname = table.replace(".fits", "") + "_ratios.pdf"

    fontlabels = 14

    
    plt.close("all")
    figsize=(15./2, 6/1.5)
    fig, axes = plt.subplots(2, 1, figsize=figsize,
        gridspec_kw={"hspace": 0.1,
                     "wspace": 0.01, 
                     "left": 0.15, 
                     "bottom": 0.3, 
                     "right": 0.92,
                     "top": 0.95
                     },
                     height_ratios=[1.5, 0.75])

    table = Table.read(table)
    table = table[table.argsort(keys=["OBSID"])]

    n_groups = list(set(table["group_id"]))
    norm = mpl.colors.Normalize(vmin=-1, vmax=max(n_groups)+1)
    cmap = parse_cmap(cmap)
    cmap.set_bad("black")
    cmap.set_under("black")

    axes[0].plot(table["OBSID"], table["ratio"],
        # yerr=table["std"],
        ls="",
        marker="o",
        c="black",
        ms=2.,
        mec="black"
    )
    # axes[0].errorbar(table["OBSID"], table["ratio"],
    #     yerr=table["std"],
    #     ls="",
    #     marker="o",
    #     c="black",
    #     ms=2.,
    #     mec="black",
    #     ecolor="darkgray",
    #     zorder=0
    # )
    axes[0].fill_between(
        table["OBSID"], table["ratio"]+table["std"], table["ratio"]-table["std"],
        color="darkgray",
        zorder=-1,
        alpha=0.5,
        lw=0
    )
    axes[1].plot(table["OBSID"], table["res"],
        ls="",
        marker="o",
        c="darkgray",
        ms=2.,
        mec="darkgray"
    )

    for i in n_groups:

        idx = np.where(table["group_id"] == i)[0]
        if len(idx) > 0:

            if i >= 0:

                axes[0].plot(table["OBSID"][idx], table["model"][idx],
                    ls="-",
                    marker="",
                    c=cmap(norm(i))
                )
                # axes[1].plot(table["OBSID"][idx], table["res"][idx],
                #     ls="-",
                #     marker="",
                #     c=cmap(norm(i))
                # )

            else:

                axes[0].plot(table["OBSID"][idx], table["model"][idx],
                    ls="",
                    marker="_",
                    mew=2.,
                    c="black"
                )
                # axes[1].plot(table["OBSID"][idx], table["res"][idx],
                #     ls="",
                #     marker="_",
                #     mew=2.,
                #     c="black"
                # )


    axes[0].set_yscale("log")
    axes[0].set_xticklabels("")


    for ax in axes:
        ax.tick_params(which="major", axis="both", labelsize=fontlabels, length=7.)
        ax.tick_params(which="minor", axis="both", length=3.5)
        ax.tick_params(which="major", axis="x", rotation=30.)
    axes[0].axhline(1., ls="--", c="black", zorder=-1)
    axes[1].axhline(1., ls="--", c="black", zorder=-1)
    axes[1].ticklabel_format(style="plain")
    
    axes[0].yaxis.set_minor_locator(ticker.LogLocator(subs=[2, 4, 6, 8]))
    axes[0].yaxis.set_minor_formatter(ScalarFormatter())

    axes[1].set_xlabel("GPS time / s", fontsize=fontlabels)
    axes[0].set_ylabel("$S_{300} / S_\mathrm{\mathsf{model}}$", fontsize=fontlabels)
    axes[1].set_ylabel("Residual", fontsize=fontlabels)

    if outname.endswith(".png"):
        plt.savefig(outname, dpi=72., bbox_inches="tight")
    else:
        plt.savefig(outname, dpi=72.)


def flux_ratio_with_model_by_all_nights(tablename, outdir="./", cmap="cmr.neon",
    yrange=(0.2, 5.0),
    gal_lat_limit=5.):
    """Plot flux density ratios with models as a function of OBSID per night."""


    fontlabels = 14

    big_table = Table.read(tablename)

    cmap = parse_cmap(cmap)
    cmap.set_bad("black")
    cmap.set_under("black")

    strips = np.unique(big_table["STRIP"])
    for strip in strips:
    
        idx1 = np.where(big_table["STRIP"] == strip)[0]
        nights = np.unique(big_table[idx1]["NIGHT"])

        for night in nights:

            outname = "{}/{}_night{}_ratios.pdf".format(outdir, strip, night)

            idx2 = np.where(big_table[idx1]["NIGHT"] == night)[0]

            table = big_table[idx1][idx2]
            coords = SkyCoord(ra=table["RA"]*u.deg, dec=table["DEC"]*u.deg)
            galactic = np.where(abs(coords.galactic.b.value) <= gal_lat_limit)[0]

            plt.close("all")
            figsize=(15./2, 6/1.5)
            fig, axes = plt.subplots(2, 1, figsize=figsize,
                gridspec_kw={"hspace": 0.1,
                            "wspace": 0.01, 
                            "left": 0.15, 
                            "bottom": 0.3, 
                            "right": 0.92,
                            "top": 0.95
                            },
                            height_ratios=[1.5, 0.75])

            table = table[table.argsort(keys=["OBSID"])]

            print(table)

            n_groups = np.asarray(list(set(table["FLUX_RATIO_GROUP"])))
            max_groups = len(n_groups[np.where(n_groups != -1)[0]])

            norm = mpl.colors.Normalize(vmin=-1, vmax=max(n_groups)+1)

            ratio = table["FLUX_RATIO_WMEAN"] * table["INT_PEAK_MODEL"]

            axes[0].plot(table["OBSID"], ratio,
                # yerr=table["std"],
                ls="",
                marker="o",
                c="black",
                ms=2.,
                mec="black"
            )
            axes[0].plot(table["OBSID"][galactic], ratio[galactic],
                # yerr=table["std"],
                ls="",
                marker="*",
                c="gray",
                ms=3.,
                mec="gray"
            )

            axes[0].fill_between(
                table["OBSID"], ratio+table["FLUX_RATIO_WSTD"], ratio-table["FLUX_RATIO_WSTD"],
                color="darkgray",
                zorder=-1,
                alpha=0.5,
                lw=0
            )
            axes[1].plot(table["OBSID"], table["FLUX_RATIO_RESIDUALS"],
                ls="",
                marker="o",
                c="darkgray",
                ms=2.,
                mec="darkgray"
            )

            for i in n_groups:

                idx = np.where(table["FLUX_RATIO_GROUP"] == i)[0]
                if len(idx) > 0:

                    if i >= 0:

                        axes[0].plot(table["OBSID"][idx], table["FLUX_RATIO_MODEL"][idx],
                            ls="-",
                            marker="",
                            c=cmap(norm(i))
                        )
   
                    else:

                        axes[0].plot(table["OBSID"][idx], table["FLUX_RATIO_MODEL"][idx],
                            ls="",
                            marker="_",
                            mew=2.,
                            c="black"
                        )


            axes[0].set_yscale("log")
            axes[1].set_yscale("log")
            axes[1].set_ylim([0.5, 2.0])
            axes[0].set_ylim(yrange)
            axes[0].set_xticklabels("")


            for ax in axes:
                ax.tick_params(which="major", axis="both", labelsize=fontlabels, length=7.)
                ax.tick_params(which="minor", axis="both", length=3.5)
                ax.tick_params(which="major", axis="x", rotation=30.)
            axes[0].axhline(1., ls="--", c="black", zorder=-1)
            axes[1].axhline(1., ls="--", c="black", zorder=-1)
            
            
            for i in [0 , 1]:
                axes[i].yaxis.set_minor_locator(ticker.LogLocator(subs=[2, 5]))
                axes[i].yaxis.set_minor_formatter(ScalarFormatter())
                axes[i].yaxis.set_major_formatter(ScalarFormatter())

            axes[1].ticklabel_format(axis="x", style="plain")

            axes[1].set_xlabel("GPS time / s", fontsize=fontlabels)
            axes[0].set_ylabel("$S_{300} / S_\mathrm{\mathsf{model}}$", fontsize=fontlabels)
            axes[1].set_ylabel("Residual", fontsize=fontlabels)

            if max_groups == 1:
                axes[0].text(0.01, 0.05, "1 group",
                    fontsize=fontlabels, 
                    transform=axes[0].transAxes,
                    ha="left",
                    va="bottom"
                )
            elif max_groups == 0:
                axes[0].text(0.01, 0.05, "No groups",
                    fontsize=fontlabels, 
                    transform=axes[0].transAxes,
                    ha="left",
                    va="bottom"
                )
            else:
                axes[0].text(0.01, 0.05, "{} groups".format(max_groups),
                    fontsize=fontlabels, 
                    transform=axes[0].transAxes,
                    ha="left",
                    va="bottom"
                )

            plt.savefig(outname, dpi=72.)   
            plt.savefig(outname.replace(".pdf", ".png"), dpi=72., bbox_inches="tight")


def intpeak_ratio_with_model_by_all_nights(tablename, outdir="./", cmap="cmr.neon",
    yrange=(0.67, 1.5),
    gal_lat_limit=5.):
    """Plot flux density ratios with models as a function of OBSID per night."""


    fontlabels = 14

    big_table = Table.read(tablename)

    cmap = parse_cmap(cmap)
    cmap.set_bad("black")
    cmap.set_under("black")


    strips = np.unique(big_table["STRIP"])
    for strip in strips:
    
        idx1 = np.where(big_table["STRIP"] == strip)[0]
        nights = np.unique(big_table[idx1]["NIGHT"])

        for night in nights:

            outname = "{}/{}_night{}_ip.pdf".format(outdir, strip, night)

            idx2 = np.where(big_table[idx1]["NIGHT"] == night)[0]

            table = big_table[idx1][idx2]
            coords = SkyCoord(ra=table["RA"]*u.deg, dec=table["DEC"]*u.deg)
            galactic = np.where(abs(coords.galactic.b.value) <= gal_lat_limit)[0]

            plt.close("all")
            figsize=(15./2, 6/1.5)
            fig, axes = plt.subplots(2, 1, figsize=figsize,
                gridspec_kw={"hspace": 0.1,
                            "wspace": 0.01, 
                            "left": 0.15, 
                            "bottom": 0.3, 
                            "right": 0.92,
                            "top": 0.95
                            },
                            height_ratios=[1.5, 0.75])

            table = table[table.argsort(keys=["OBSID"])]

            print(table)

            n_groups = np.asarray(list(set(table["INT_PEAK_GROUP"])))
            max_groups = len(n_groups[np.where(n_groups != -1)[0]])

            norm = mpl.colors.Normalize(vmin=-1, vmax=max(n_groups)+1)

            

            axes[0].plot(table["OBSID"], table["INT_PEAK"],
                # yerr=table["std"],
                ls="",
                marker="o",
                c="black",
                ms=2.,
                mec="black"
            )
            axes[0].plot(table["OBSID"][galactic], table["INT_PEAK"][galactic],
                # yerr=table["std"],
                ls="",
                marker="*",
                c="gray",
                ms=3.,
                mec="gray"
            )

            axes[0].fill_between(
                table["OBSID"], table["INT_PEAK"]+table["INT_PEAK_WSTD"], table["INT_PEAK"]-table["INT_PEAK_WSTD"],
                color="darkgray",
                zorder=-1,
                alpha=0.5,
                lw=0
            )
            axes[1].plot(table["OBSID"], table["INT_PEAK_RESIDUALS"],
                ls="",
                marker="o",
                c="darkgray",
                ms=2.,
                mec="darkgray"
            )

            for i in n_groups:

                idx = np.where(table["INT_PEAK_GROUP"] == i)[0]
                if len(idx) > 0:

                    if i >= 0:

                        axes[0].plot(table["OBSID"][idx], table["INT_PEAK_MODEL"][idx],
                            ls="-",
                            marker="",
                            c=cmap(norm(i))
                        )
   
                    else:

                        axes[0].plot(table["OBSID"][idx], table["INT_PEAK_MODEL"][idx],
                            ls="",
                            marker="_",
                            mew=2.,
                            c="black"
                        )


            axes[0].set_yscale("log")
            axes[1].set_yscale("log")
            axes[1].set_ylim([0.67, 1.5])
            axes[0].set_ylim(yrange)
            axes[0].set_xticklabels("")


            for ax in axes:
                ax.tick_params(which="major", axis="both", labelsize=fontlabels, length=7.)
                ax.tick_params(which="minor", axis="both", length=3.5)
                ax.tick_params(which="major", axis="x", rotation=30.)
            axes[0].axhline(1., ls="--", c="black", zorder=-1)
            axes[1].axhline(1., ls="--", c="black", zorder=-1)
            
            
            for i in [0 , 1]:
                axes[i].yaxis.set_minor_locator(ticker.LogLocator(subs=[]))
                axes[i].yaxis.set_minor_formatter(ScalarFormatter())
                axes[i].set_yticks([0.7, 1.0, 1.5])
                axes[i].set_yticklabels([0.7, 1.0, 1.5])

            axes[1].ticklabel_format(axis="x", style="plain")

            axes[1].set_xlabel("GPS time / s", fontsize=fontlabels)
            axes[0].set_ylabel("$S_\mathrm{\mathsf{int}} / S_\mathrm{\mathsf{peak}}$", fontsize=fontlabels)
            axes[1].set_ylabel("Residual", fontsize=fontlabels)

            if max_groups == 1:
                axes[0].text(0.01, 0.05, "1 group",
                    fontsize=fontlabels, 
                    transform=axes[0].transAxes,
                    ha="left",
                    va="bottom"
                )
            elif max_groups == 0:
                axes[0].text(0.01, 0.05, "No groups",
                    fontsize=fontlabels, 
                    transform=axes[0].transAxes,
                    ha="left",
                    va="bottom"
                )
            else:
                axes[0].text(0.01, 0.05, "{} groups".format(max_groups),
                    fontsize=fontlabels, 
                    transform=axes[0].transAxes,
                    ha="left",
                    va="bottom"
                )

            plt.savefig(outname, dpi=72.)   
            plt.savefig(outname.replace(".pdf", ".png"), dpi=72., bbox_inches="tight")


def clean_threshold_plot(table, outname, cmap="trans"):
    """Plot showing flux differences for different CLEAN thresholds."""

    plt.close("all")
    fontsize = 12.
    figsize = (6, 4)
    cmap = parse_cmap(cmap)
    colors = [cmap(0.15), cmap(0.3), cmap(0.85)]
    fig = plt.figure(figsize=figsize)
    axes = fig.add_axes([0.15, 0.15, 0.84, 0.84])

    table = Table.read(table)

    axes.errorbar(
        table["threshold"], table["int_flux"],
        yerr=table["err_int_flux"],
        ms=6.,
        marker="o",
        mec="black",
        c=colors[0],
        ls="",
        label=r"$S_\mathrm{\mathsf{int}}$"
    )
    axes.errorbar(
        table["threshold"], table["peak_flux"],
        yerr=table["err_peak_flux"],
        ms=6.,
        marker="s",
        mec="black",
        c=colors[-1],
        ls="",
        label=r"$S_\mathrm{\mathsf{peak}}$"
    )

    axes.tick_params(which="major", axis="both", labelsize=fontsize)
    axes.tick_params(which="major", axis="both", length=7)
    axes.tick_params(which="minor", axis="both", labelsize=fontsize, 
        length=3.5)
    
    axes.axhline(1., ls="--", color="gray")

    axes.set_xlabel(r"Residual fraction", fontsize=fontsize)
    axes.set_ylabel(r"Recovered fraction", fontsize=fontsize)

    legend = axes.legend(loc="lower right",
        shadow=False,
        fancybox=False,
        frameon=False,
        fontsize=fontsize,
        numpoints=1,
        handletextpad=-0.2,
        columnspacing=0.5,
        framealpha=0.,
        ncol=1
    )

    plt.savefig(outname, dpi=72.)


def artefacts_example(cmap="cmr.flamingo_r", tc="black",
    vsetting=(-0.01, 1)):

    plt.close("all")

    cmap = parse_cmap(cmap)

    image1 = "GLEAM300_images_16-01-2025/GLEAM300_J1231+10.fits"
    catalogue1 = "source_lists_psffix/GLEAM300_J1231+10_aegean_comp.fits"
    natalogue1 = "source_lists_psffix/GLEAM300_J1231+10_aegean_negative_comp.fits"
    coords1 = (187.6908555, 12.3707651)
    fov1 = (2.*2, 2.*2)
    image2 = "GLEAM300_images_16-01-2025/GLEAM300_J1930-48.fits"
    catalogue2 = "source_lists_psffix/GLEAM300_J1930-48_aegean_comp.fits"
    natalogue2 = "source_lists_psffix/GLEAM300_J1930-48_aegean_negative_comp.fits"
    coords2 = (293.9542356, -46.3363755)
    fov2 = (1.3*2, 1.3*2)

    font_labels = 14
    font_ticks = 14

    figsize = (7.5, 6)
    cpad = 0.01
    cthick = 0.015


    fig = plt.figure(figsize=figsize)
    axes1 = [0.2, 0.1, 0.35, 0.7]
    axes2 = [0.2+0.35+0.125, 0.1, 0.35, 0.7]

    rms1 = 0.036
    rms2 = 0.008

    max1 = 264.284
    art1 = 2.49506
    max2 = 50.6144
    art2 = 0.369129

    dr1 = max1 / art1
    dr2 = max2 / art2

    norm1 = mpl.colors.Normalize(vmin=-3*rms1, vmax=20*rms1)
    norm2 = mpl.colors.Normalize(vmin=-3*rms2, vmax=20*rms2)
    # norm1 = mpl.colors.TwoSlopeNorm(vmin=-3*rms1, vmax=20*rms1, vcenter=0.)
    # norm2 = mpl.colors.TwoSlopeNorm(vmin=-3*rms2, vmax=20*rms2, vcenter=0.)

    ax1, fig, cbax1 = basic_axes.make_axes(
        ax=axes1,
        fig=fig,
        header=fits.getheader(image1),
        data=fits.getdata(image1),
        scale=1.,
        centre=coords1,
        fov=fov1,
        aspect="equal",
        do_colorbar=True,
        cmap=cmap,
        norm=norm1,
        fontlabels=font_labels,
        do_beam=False,
        fontticks=font_ticks,
        do_axis_labels=True,
        tick_colour=tc,
        colorbar_thickness=cthick,
        colorbar_label_pad=0.2,
        colorbar_pad=cpad,
        colorbar_orientation="horizontal",
        colorbar_label= r"Jy beam$^{-1}$",
    )
    ax2, fig, cbax2 = basic_axes.make_axes(
        ax=axes2,
        fig=fig,
        header=fits.getheader(image2),
        data=fits.getdata(image2),
        scale=1.,
        centre=coords2,
        fov=fov2,
        aspect="equal",
        do_colorbar=True,
        cmap=cmap,
        norm=norm2,
        fontlabels=font_labels,
        do_beam=False,
        fontticks=font_ticks,
        do_axis_labels=True,
        tick_colour=tc,
        colorbar_thickness=cthick,
        colorbar_label_pad=0.2,
        colorbar_pad=cpad,
        colorbar_orientation="horizontal",
        colorbar_label= r"Jy beam$^{-1}$",
    )

    ax1.text(0.5, 0.05, "DR: {:.0f}".format(dr1), va="bottom", ha="center", fontsize=font_labels,
        transform=ax1.transAxes,
        fontweight="bold",
        color=tc)
    ax2.text(0.5, 0.05, "DR: {:.0f}".format(dr2), va="bottom", ha="center", fontsize=font_labels,
        transform=ax2.transAxes,
        fontweight="bold",
        color=tc)

    # table1 = Table.read(catalogue1)
    # nable1 = Table.read(natalogue1)

    # ax1.scatter(
    #     table1["ra"], table1["dec"], marker="x", s=20, c="springgreen", 
    #     transform=ax1.get_transform("fk5")
    # )

    ax2.coords[1].set_axislabel("")

    plt.savefig("GLEAM300_artefact_example.pdf", dpi=300, bbox_inches="tight")


def jupiter_example(cmap="cmr.flamingo_r", tc="black",
    vsetting=(-0.01, 1)):

    plt.close("all")

    cmap = parse_cmap(cmap)

    table = Table.read("selection_text_files/GLEAM300_jupiter_tiles.fits")

    image1 = "GLEAM300_images_16-01-2025/GLEAM300_J1129+10.fits"
    coords1 = (171.454, 5.261)
    fov1 = (12*2/60., 12*2/60.)
    image2 = "GLEAM300_images_16-01-2025/GLEAM300_J1129+10.fits"
    coords2 = (165.147, 7.865)
    fov2 = (12*2/60., 12*2/60.)

    font_labels = 14
    font_ticks = 14

    figsize = (7.5, 6)
    cpad = 0.01
    cthick = 0.015


    fig = plt.figure(figsize=figsize)
    axes1 = [0.2, 0.1, 0.35, 0.7]
    axes2 = [0.2+0.35+0.125, 0.1, 0.35, 0.7]

    # rms1 = 0.036
    rms2 = 0.008
    rms1 = rms2

    max1 = 264.284
    art1 = 2.49506
    max2 = 50.6144
    art2 = 0.369129

    dr1 = max1 / art1
    dr2 = max2 / art2

    norm1 = mpl.colors.Normalize(vmin=-3*rms1, vmax=100*rms1)
    norm2 = mpl.colors.Normalize(vmin=-3*rms2, vmax=100*rms2)
    # norm1 = mpl.colors.TwoSlopeNorm(vmin=-3*rms1, vmax=20*rms1, vcenter=0.)
    # norm2 = mpl.colors.TwoSlopeNorm(vmin=-3*rms2, vmax=20*rms2, vcenter=0.)

    ax1, fig, cbax1 = basic_axes.make_axes(
        ax=axes1,
        fig=fig,
        header=fits.getheader(image1),
        data=fits.getdata(image1),
        scale=1.,
        centre=coords1,
        fov=fov1,
        aspect="equal",
        do_colorbar=True,
        cmap=cmap,
        norm=norm1,
        fontlabels=font_labels,
        do_beam=False,
        fontticks=font_ticks,
        do_axis_labels=True,
        tick_colour=tc,
        colorbar_thickness=cthick,
        colorbar_label_pad=0.2,
        colorbar_pad=cpad,
        colorbar_orientation="horizontal",
        colorbar_label= r"Jy beam$^{-1}$",
    )
    ax2, fig, cbax2 = basic_axes.make_axes(
        ax=axes2,
        fig=fig,
        header=fits.getheader(image2),
        data=fits.getdata(image2),
        scale=1.,
        centre=coords2,
        fov=fov2,
        aspect="equal",
        do_colorbar=True,
        cmap=cmap,
        norm=norm2,
        fontlabels=font_labels,
        do_beam=False,
        fontticks=font_ticks,
        do_axis_labels=True,
        tick_colour=tc,
        colorbar_thickness=cthick,
        colorbar_label_pad=0.2,
        colorbar_pad=cpad,
        colorbar_orientation="horizontal",
        colorbar_label= r"Jy beam$^{-1}$",
    )

    # ax1.text(0.5, 0.05, "DR: {:.0f}".format(dr1), va="bottom", ha="center", fontsize=font_labels,
    #     transform=ax1.transAxes,
    #     fontweight="bold",
    #     color=tc)
    # ax2.text(0.5, 0.05, "DR: {:.0f}".format(dr2), va="bottom", ha="center", fontsize=font_labels,
    #     transform=ax2.transAxes,
    #     fontweight="bold",
    #     color=tc)

    # table1 = Table.read(catalogue1)
    # nable1 = Table.read(natalogue1)

    ax1.scatter(
        table["RA"], table["DEC"], marker="x", s=50, c="mediumspringgreen", 
        transform=ax1.get_transform("fk5")
    )

    ax2.scatter(
        table["RA"], table["DEC"], marker="x", s=50, c="mediumspringgreen", 
        transform=ax2.get_transform("fk5")
    )

    ax2.coords[1].set_axislabel("")

    plt.savefig("GLEAM300_jupiter_example.pdf", dpi=300, bbox_inches="tight")


def ateam_sed(source="Fornax A", outname="FornaxA_SED.png", cmap="gnuplot2"):
    """
    """

    # units: Jy / MHz

    if source == "Fornax A":
        table, model_x, model_y = sed_fitting.fornax() 
    else:
        raise NotImplementedError

    plt.close("all")
    cmap1 = parse_cmap(cmap)

    fontsize = 14.
    figsize = (5, 5)
    fig = plt.figure(figsize=figsize)
    axes1 = fig.add_axes([0.2, 0.2, 0.75, 0.75])

    markers = ["*", "o", "x", "D"]
    colors = ["crimson", "gray", "black", "dodgerblue"]
    labels = ["GLEAM-300", "GLEAM SGP", "VLA", ]

    logger.debug(table["label"])

    idx1 = np.where(table["label"] == 0)[0]
    idx2 = np.where(table["label"] == 1)[0]
    idx3 = np.where(table["label"] == 3)[0]
    idx4 = np.where(table["label"] == 2)[0]

    # axes1.errorbar(
    #     table["freq"][idx1],
    #     table["flux"][idx1],
    #     yerr=table["err_flux"][idx1],
    #     marker="o",
    #     ms=6.,
    #     ls="",
    #     c="gray",
    #     label="GLEAM",
    #     zorder=100
    # )

    axes1.errorbar(
        table["freq"][idx2],
        table["flux"][idx2],
        yerr=table["err_flux"][idx2],
        marker="*",
        ms=10.,
        ls="",
        c="crimson",
        label="GLEAM-300",
        zorder=99
    )

    axes1.errorbar(
        table["freq"][idx3],
        table["flux"][idx3],
        marker="x",
        ms=9.,
        ls="",
        c="black",
        label="VLA",
        zorder=98
    )
    
    axes1.errorbar(
        table["freq"][idx4],
        table["flux"][idx4],
        yerr=table["err_flux"][idx4],
        marker="o",
        ms=6.,
        ls="",
        c="gray",
        label="GLEAM SGP",
        zorder=97
    )
    
    axes1.set_xscale("log")
    axes1.set_yscale("log")

    axes1.margins(0.1, 0.1)
    ylims = axes1.get_ylim()
    logger.debug(ylims)

    axes1.plot(model_x, model_y, ls="-", c="pink", label="PB2017", zorder=101)

    axes1.set_ylim(ylims)

    axes1.tick_params(which="major", axis="both", labelsize=fontsize)
    axes1.tick_params(which="major", axis="both", length=7)
    axes1.tick_params(which="minor", axis="both", labelsize=fontsize, 
        length=3.5)
    axes1.tick_params(which="both", axis="both", direction="in")
    
    axes1.xaxis.set_minor_locator(ticker.LogLocator(subs=[0.5]))
    axes1.xaxis.set_minor_formatter(ScalarFormatter())
    axes1.yaxis.set_minor_locator(
        ticker.LogLocator(
            subs=[
                0.2, 0.4, 0.6, 0.8, 
            ]
        )
    )

    axes1.yaxis.set_minor_formatter(FormatStrFormatter("%s"))
    # axes.yaxis.set_minor_formatter(ScalarFormatter())

    axes1.set_xlabel("Frequency / MHz", fontsize=fontsize)
    axes1.set_ylabel("Flux density / Jy", fontsize=fontsize)

    axes = axes1

    hands1 = [
        Line2D([0], [0],
               linestyle="",
               color=colors[i],
               marker=markers[i],
               label=labels[i],
               markersize=8.,
               )
        for i in range(3)
    ]
    hands1 = [hands1[1], hands1[0], hands1[2]]
    hands2 = [
        Line2D([0], [0], linestyle="-", color="pink", label="PB2017"),
    ]

    loc1 = "upper right"
    loc2 = "lower left"

    legend1 = axes.legend(loc=loc1,
        handles=hands1,
        shadow=False,
        fancybox=False,
        frameon=True,
        fontsize=fontsize-2,
        numpoints=1
    )
    legend1.get_frame().set_edgecolor("dimgrey")
    legend2 = axes.legend(loc=loc2,
        handles=hands2,
        shadow=False,
        fancybox=False,
        frameon=True,
        fontsize=fontsize-2,
        numpoints=1
    )
    legend2.get_frame().set_edgecolor("dimgrey")
    plt.gca().add_artist(legend1)

    plt.savefig(outname, dpi=72., bbox_inches="tight")
    plt.savefig(outname.replace(".png", ".pdf"), dpi=72)


def plot_artefact_removal(image,
    positive, negative,
    threshold, radius, coords, rms,
    cmap : str = "cmr.flamingo_r",
    positive_artefacts=None,
    negative_artefacts=None,
    tc : str = "black",
    outname : str = None,
    negative_contour : bool = False):
    """
    """

    # logger.debug(positive)
    # logger.debug(negative)
    # logger.debug(positive_artefacts)
    # logger.debug(negative_artefacts)

    if outname is None:
        outname = "{}_{}d_{}d_{}Jy.png".format(
            image.replace(".fits", ""), coords[0], coords[1], threshold
        )

    plt.close("all")
    cmap = parse_cmap(cmap)

    font_labels = 14
    font_ticks = 14

    figsize = (7.5, 6)
    cpad = 0.01
    cthick = 0.015

    fig = plt.figure(figsize=figsize)
    axes = [0.2, 0.1, 0.8, 0.85]

    norm1 = mpl.colors.Normalize(vmin=-1*rms, vmax=10*rms)

    ax1, fig, cbax1 = basic_axes.make_axes(
        ax=axes,
        fig=fig,
        header=fits.getheader(image),
        data=fits.getdata(image),
        scale=1.,
        centre=coords,
        fov=(2*radius*1.5, 2*radius*1.5),
        aspect="equal",
        do_colorbar=True,
        cmap=cmap,
        norm=norm1,
        fontlabels=font_labels,
        do_beam=False,
        fontticks=font_ticks,
        do_axis_labels=True,
        tick_colour=tc,
        colorbar_thickness=cthick,
        colorbar_label_pad=0.2,
        colorbar_pad=cpad,
        colorbar_orientation="vertical",
        colorbar_label= r"Jy beam$^{-1}$",
    )

    if positive is not None:
        if len(positive) > 0:
            ax1.scatter(
                positive["ra"], 
                positive["dec"], 
                marker="x", s=30, 
                c="mediumspringgreen", 
                transform=ax1.get_transform("fk5")
            )

    if negative is not None:
        if len(negative) > 0:
            ax1.scatter(
                negative["ra"], 
                negative["dec"], 
                marker="+", s=30, 
                c="dodgerblue", 
                transform=ax1.get_transform("fk5")
            )

    if positive_artefacts is not None:
        if len(positive_artefacts) > 0: 
            ax1.scatter(
                positive_artefacts["ra"], 
                positive_artefacts["dec"], 
                marker="o", s=100, 
                c="mediumspringgreen", 
                transform=ax1.get_transform("fk5")
            )

    if negative_artefacts is not None:
        if len(negative_artefacts) > 0:
            ax1.scatter(
                negative_artefacts["ra"], 
                negative_artefacts["dec"], 
                marker="s", s=100, 
                c="dodgerblue", 
                transform=ax1.get_transform("fk5")
            )

    r = SphericalCircle((coords[0]*u.deg, coords[1]*u.deg),
        radius*u.deg,
        edgecolor="black",
        linestyle="--",
        linewidth=2.,
        facecolor="none",
        transform=ax1.get_transform("fk5")
    )
    ax1.add_patch(r)

    plt.savefig(outname, dpi=72, bbox_inches="tight")


