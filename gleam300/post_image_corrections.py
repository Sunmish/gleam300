#! /usr/bin/env python

from typing import List, Any, Optional, Tuple

import os
import numpy as np
from glob import glob
import math

from subprocess import Popen

from astropy.table import Table, vstack
from astropy.io import fits
from astropy.time import Time
from astropy.stats import akaike_info_criterion_lsq, bayesian_info_criterion_lsq

from astropy.coordinates import SkyCoord
from astropy import units as u

from scipy.signal import savgol_filter
from scipy.interpolate import UnivariateSpline

from skymodel import get_beam, parsers

from tqdm import trange

from racs_sf import utils
from gleam300.plotting import flux_ratio_with_model_by_night 
from gleam300.stats import window_filter


import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def ignore_sources(coords, values, sources=["CenA", "LMC", "SMC"]):
    source_coords = {
        "CenA": SkyCoord(ra="13h25m28s", dec="-43d01m09s", units=(u.hourangle, u.deg)),
        "LMC": SkyCoord(ra="05h23m35s", dec="-69d45m22s", units=(u.hourangle, u.deg)),
        "SMC": SkyCoord(ra="00h52m38s", dec="-72d48m01s", units=(u.hourangle, u.deg))
    }

    print(source_coords)

    source_radius = {
        "CenA": 9.,
        "LMC": 5.5,
        "SMC": 2.5
    }

    for source in sources:

        seps = source_coords[source].separation(coords)
        print(seps)
        idx = np.where(seps.value < source_radius[source])[0]
        values[idx] = np.nan
        logger.debug("{} obsids near {}".format(len(idx), source))

    return values


def fit_value(table,
        ratio_key,
        weight_key,
        polyorder=3,
        ratio_range=(0.1, 10),
        polydegrees=[i for i in range(20)],
        selector="bic",
        max_jump=0.1,
        max_gap=20*60.,
        min_group_fit=5,
        filter=True,
        default_value=np.nan,
        gal_lat_limit=None,
        apply_ip_correction=False
    ):

    if apply_ip_correction:
        ratio = table[ratio_key] * table["INT_PEAK_MODEL"]
    else:
        ratio = table[ratio_key]

    if filter:
        filtered_ratios = window_filter(ratio, 5)
    else:
        filtered_ratios = ratio


    coords = SkyCoord(ra=table["RA"]*u.deg, dec=table["DEC"]*u.deg)
    if gal_lat_limit is not None:
        idx = np.where(abs(coords.galactic.b.value) <= gal_lat_limit)[0]
        filtered_ratios[idx] = np.nan

    # filtered_ratios = ignore_sources(coords, filtered_ratios)


    obsids = np.asarray(table["OBSID"])
    group_id = np.full((len(obsids),), 0, dtype=int)
    fit_obsids = obsids[np.where(np.isfinite(filtered_ratios))[0]]

    original_ratios = np.asarray(table[ratio_key])
    weights = table[weight_key][np.where(np.isfinite(filtered_ratios))[0]]
    ratios = filtered_ratios[np.where(np.isfinite(filtered_ratios))[0]] 
    logger.debug(ratios)
    logger.debug(fit_obsids)

    if len(ratios) < 3:
        raise RuntimeError("Not enough ratios outside of {}".format(ratio_range))

    groups = {}
    n = 0
    i = 1
    init_group = fit_obsids[0]
    while i < len(fit_obsids):
        diff = np.diff(ratios)
        if abs(np.log10(ratios[i]) - np.log10(ratios[i-1])) > np.std(diff)+np.mean(abs(diff)) or \
            abs(np.log10(ratios[i]) - np.log10(ratios[i-1])) > max_jump:
            groups[n] = (init_group, fit_obsids[i-1])
            init_group = fit_obsids[i]
            n += 1
        elif abs(fit_obsids[i] - fit_obsids[i-1]) > max_gap:
            groups[n] = (init_group, fit_obsids[i-1])
            init_group = fit_obsids[i]
            n += 1
        elif i == len(fit_obsids)-1:
            groups[n] = (init_group, fit_obsids[i])

        i += 1


    logger.debug(groups)
    group_fits = []
    groups_with_fits = []
    group_median_obsid = []

    for i in groups.keys():

        logger.debug("Group {}".format(i))

        aics = []
        fits = []

        cond1 = fit_obsids >= groups[i][0]
        cond2 = fit_obsids <= groups[i][1]
        idx = np.where(cond1 & cond2)[0]
        if len(idx) < min_group_fit:
            continue

        group_ratios = ratios[idx]
        group_obsids = fit_obsids[idx]
        group_weights = weights[idx]

        if polydegrees is None:
            degrees_to_try = [i for i in range(math.ceil(len(group_obsids)/polyorder))]
        else:
            degrees_to_try = polydegrees
        for deg in degrees_to_try:
            if len(group_ratios) < deg-1:
                break
            try:
                fit, params = np.polynomial.Polynomial.fit(
                    group_obsids, group_ratios,
                    w=group_weights,
                    deg=deg,
                    full=True 
                )
                ssr1 = np.nansum((fit(group_obsids) - group_ratios)**2)
                if selector == "aic":
                    aic = akaike_info_criterion_lsq(ssr1, len(fit.convert().coef), len(group_ratios))
                else:
                    aic = bayesian_info_criterion_lsq(ssr1, len(fit.convert().coef), len(group_ratios))
                aics.append(aic)
                fits.append(fit)
            except ZeroDivisionError:
                break
        min_aic = np.argmin(np.asarray(aics))
        deg1 = degrees_to_try[min_aic]
        logger.info("Group {} - min AIC: {}".format(i, deg1))
        final_deg = deg1
        fit = fits[min_aic]
        group_fits.append(fit)
        group_median_obsid.append(np.nanmedian(group_obsids))
        groups_with_fits.append(i)

    group_median_obsid = np.asarray(group_median_obsid)
    logger.debug(group_median_obsid)
    logger.debug(groups_with_fits)

    filtered = np.full((len(obsids),), default_value)
    for i in range(len(filtered)):
        # idx = (np.abs(group_median_obsid - obsids[i])).argmin()

        use_fit = False
        for g in groups_with_fits:
            if groups[g][0] <= obsids[i] <= groups[g][1]:
                use_fit = True
                break

        if use_fit:
            diffs1 = [abs(obsids[i]-groups[n][0]) for n in groups_with_fits]
            diffs2 = [abs(obsids[i]-groups[n][1]) for n in groups_with_fits]
            min_sep = [min([diffs1[n], diffs2[n]]) for n in range(len(diffs1))]
            idx = np.argmin(min_sep) 
            fit = group_fits[idx]
            # logger.debug(group_median_obsid[idx])
            filtered[i] = fit(obsids[i])
            group_id[i] = groups_with_fits[idx]

    good_obsids = obsids[np.where(np.isfinite(filtered))[0]]
    good_filtered = filtered[np.where(np.isfinite(filtered))[0]]

    for i in range(len(filtered)):
        if np.isnan(filtered[i]):
            filtered[i] = good_filtered[np.argmin(abs(obsids[i]-good_obsids))]
            group_id[i] = -1 
        elif filtered[i] == default_value:
            group_id[i] = -1

    residuals = ratio / filtered

    table_out = Table(
        [obsids, original_ratios, filtered, residuals, group_id],
        names=["OBSID", "RATIO", "MODEL", "RES", "GROUP"]
    )

    return table_out



def fit_flux_density_ratio(database,
    polyorder=3,
    ratio_range=(0.2, 5),
    polydegrees=None,
    selector="bic",
    max_jump=0.1,
    min_group_fit=5,
    filter=True,
    plot_name_stub=".png",
    strips=None,
    nights=None,
    apply_ip_correction=True,
    gal_lat_limit=5.,
    match_scp=False
    ):

    table = Table.read(database)

    # if apply_ip_correction:
        # ratio = table["FLUX_RATIO_WMEAN"] * table["INT_PEAK_MODEL"]
         

    obsids = list(set(list(table["OBSID"])))

    if strips is None:
        strips = np.unique(table["STRIP"])

    model_ratios = np.full((len(table),), 1.)
    residuals = np.full((len(table),), 0.)
    group_ids = np.full((len(table),), -1, dtype=int)
    filtered_ratios = np.full((len(table), ), 1.)

    for strip in strips:
        
        # if strip == "g235m86.0" and match_scp:
        #     continue
    
        idx = np.where(table["STRIP"] == strip)[0]


        if nights is None:
            nights = np.unique(table[idx]["NIGHT"])


        model_ratios_n = np.full((len(table[idx]),), 1.)
        residuals_n = np.full((len(table[idx]),), 0.)
        group_ids_n = np.full((len(table[idx]),), -1, dtype=int)
        filtered_ratios_n = np.full((len(table[idx]), ), 1.)

        for night in nights:
            
            ndx = np.where(table[idx]["NIGHT"] == night)[0]

            try:
                params = fit_value(
                    table=table[idx][ndx],
                    ratio_key="FLUX_RATIO_WMEAN",
                    weight_key="FLUX_RATIO_WSUM",
                    polyorder=polyorder,
                    polydegrees=polydegrees,
                    max_jump=max_jump,
                    selector=selector,
                    min_group_fit=min_group_fit,
                    filter=filter,
                    gal_lat_limit=gal_lat_limit,
                    apply_ip_correction=True
                )

                model_ratios_n[ndx] = np.asarray(params["MODEL"])
                residuals_n[ndx] = np.asarray(params["RES"])
                filtered_ratios_n[ndx] = np.asarray(params["RATIO"])
                group_ids_n[ndx] = np.asarray(params["GROUP"])
            except (ValueError, RuntimeError) as e:
                pass


        model_ratios[idx] = model_ratios_n
        residuals[idx] = residuals_n
        filtered_ratios[idx] = filtered_ratios_n
        group_ids[idx] = group_ids_n

    try:
        table.add_column(model_ratios, name="FLUX_RATIO_MODEL")
    except ValueError:
        table["FLUX_RATIO_MODEL"] = model_ratios
    
    try:
        table.add_column(group_ids, name="FLUX_RATIO_GROUP")
    except ValueError:
        table["FLUX_RATIO_GROUP"] = group_ids

    try:
        table.add_column(residuals, name="FLUX_RATIO_RESIDUALS")
    except ValueError:
        table["FLUX_RATIO_RESIDUALS"] = residuals

    try:
        table.add_column(filtered_ratios, name="FLUX_RATIO_FILTERED")
    except ValueError:
        table["FLUX_RATIO_FILTERED"] = filtered_ratios

    if match_scp:

        scp_idx = np.where(table["STRIP"] == "g235m86.0")[0]
        zenith_idx = np.where(table["STRIP"] == "g235m26.7")[0]

        keys = ["FLUX_RATIO_MODEL", "FLUX_RATIO_GROUP", "FLUX_RATIO_RESIDUALS", "FLUX_RATIO_FILTERED"]

        for i in scp_idx:
            idx = np.where(table["OBSID"][i] == table[zenith_idx]["OBSID"])[0]
            for key in keys:
                table[key][i] = table[key][zenith_idx[idx]]
    
    table.write(database, overwrite=True)


def fit_intpeak_ratio(database,
    polyorder=3,
    ratio_range=(0.5, 2.0),
    polydegrees=None,
    selector="bic",
    max_jump=0.05,
    min_group_fit=10,
    filter=True,
    plot_name_stub=".png",
    strips=None,
    nights=None,
    gal_lat_limit=5.
):

    table = Table.read(database)
    obsids = list(set(list(table["OBSID"])))

    if strips is None:
        strips = np.unique(table["STRIP"])

    model_ratios = np.full((len(table),), 1.)
    residuals = np.full((len(table),), 0.)
    group_ids = np.full((len(table),), -1, dtype=int)
    filtered_ratios = np.full((len(table), ), 1.)

    for strip in strips:
        
        idx = np.where(table["STRIP"] == strip)[0]

        if nights is None:
            nights = np.unique(table[idx]["NIGHT"])


        model_ratios_n = np.full((len(table[idx]),), 1.)
        residuals_n = np.full((len(table[idx]),), 0.)
        group_ids_n = np.full((len(table[idx]),), -1, dtype=int)
        filtered_ratios_n = np.full((len(table[idx]), ), 1.)

        for night in nights:
            
            ndx = np.where(table[idx]["NIGHT"] == night)[0]

            try:
                params = fit_value(
                    table=table[idx][ndx],
                    ratio_key="INT_PEAK",
                    weight_key="INT_PEAK_STD",
                    polyorder=polyorder,
                    polydegrees=polydegrees,
                    max_jump=max_jump,
                    selector=selector,
                    min_group_fit=min_group_fit,
                    filter=filter,
                    default_value=1.,
                    gal_lat_limit=gal_lat_limit
                )

                model_ratios_n[ndx] = np.asarray(params["MODEL"])
                residuals_n[ndx] = np.asarray(params["RES"])
                filtered_ratios_n[ndx] = np.asarray(params["RATIO"])
                group_ids_n[ndx] = np.asarray(params["GROUP"])
            except (ValueError, RuntimeError) as e:
                pass


        model_ratios[idx] = model_ratios_n
        residuals[idx] = residuals_n
        filtered_ratios[idx] = filtered_ratios_n
        group_ids[idx] = group_ids_n

    try:
        table.add_column(model_ratios, name="INT_PEAK_MODEL")
    except ValueError:
        table["INT_PEAK_MODEL"] = model_ratios
    
    try:
        table.add_column(group_ids, name="INT_PEAK_GROUP")
    except ValueError:
        table["INT_PEAK_GROUP"] = group_ids

    try:
        table.add_column(residuals, name="INT_PEAK_RESIDUALS")
    except ValueError:
        table["INT_PEAK_RESIDUALS"] = residuals

    try:
        table.add_column(filtered_ratios, name="INT_PEAK_FILTERED")
    except ValueError:
        table["INT_PEAK_FILTERED"] = filtered_ratios

    table.write(database, overwrite=True)



def apply_corrections(
    database,
    strip=None,
    image_stub="_deep-MFS-image-pb_warp.fits",
    unapply_flux=False,
    unapply_psf=False,
    outbase="_deep-MFS-image-pb_warp_corr.fits"
):

    table = Table.read(database)
    flux_model = np.asarray(table["FLUX_RATIO_MODEL"])
    ip_model = np.asarray(table["INT_PEAK_MODEL"])

    # if images is None:
    images_all = [
        "{}_{}{}".format(table["OBSID"][i], table["STRIP"][i], image_stub) for i in range(len(table))
    ]

    if strip is not None:
        images = []
        for image in images_all:
            if strip in image:
                images.append(image)
    else:
        images = images_all

    if outbase is not None:
        mode = "readonly"
    else:
        mode = "update"

    pbar = trange(len(images))
    for i in pbar:
        
        if os.path.exists(images[i]):

            try:
                with fits.open(images[i], mode=mode) as f:

                    bits = images[i].split("_")
                    obsid = bits[0]
                    strip = bits[1]
                    cond1 = table["OBSID"] == int(obsid)
                    cond2 = table["STRIP"] == strip
                
                    idx = np.where(cond1 & cond2)

                    fscale = flux_model[idx]
                    pscale = ip_model[idx]
                    pbar.set_description("{}: {:.3f} {:.3f}".format(images[i], fscale, pscale))

                    if unapply_flux and "FSCALE" in f[0].header.keys():

                        f[0].data *= f[0].header["FSCALE"]
                    
                    if unapply_psf and "PSCALE" in f[0].header.keys():

                        f[0].header["BMAJ"] /= np.sqrt(f[0].header["PSCALE"])
                        f[0].header["BMIN"] /= np.sqrt(f[0].header["PSCALE"])

                    f[0].data /= fscale
                    f[0].header["BMAJ"] *= np.sqrt(pscale)
                    f[0].header["BMIN"] *= np.sqrt(pscale)

                    f[0].header["FSCALE"] = fscale
                    f[0].header["PSCALE"] = pscale

                    if outbase is not None:
                        if strip is None:
                            strip_i = table["STRIP"][i]
                        else:
                            strip_i = strip
                        f.writeto("{}_{}{}".format(obsid, strip_i, outbase), overwrite=True)
                    else:
                        f.flush()

            except TypeError:
                os.remove(images[i])

def apply_corrections_catalogues(
    database,
    catalogue_stub="_deep-MFS-image-pb_comp.vot",
    unapply_flux=False,
    unapply_psf=False,
    outbase="_deep-MFS-image-pb_comp_corr.fits",
    int_flux_key="int_flux",
    peak_flux_key="peak_flux",
    local_rms_key="local_rms",
    overwrite=False
):

    table = Table.read(database)
    flux_model = np.asarray(table["FLUX_RATIO_MODEL"])
    ip_model = np.asarray(table["INT_PEAK_MODEL"])

    pbar = trange(len(table))
    for i in pbar:

        catalogue = "GLEAM{}/{}{}".format(
            table[i]["STRIP"],
            table[i]["OBSID"],
            catalogue_stub
        )

        pbar.set_description(catalogue)

        if os.path.exists(catalogue):

            table_i = Table.read(catalogue)

            table_i[int_flux_key] *= ip_model[i]

            for key in [int_flux_key, local_rms_key, peak_flux_key]:

                table_i[key] /= flux_model[i]

            outname = "GLEAM{}/{}{}".format(
                table[i]["STRIP"],
                table[i]["OBSID"],
                outbase
            )

            try:
                table_i.write(outname, overwrite=overwrite)
            except OSError:
                pass

        

